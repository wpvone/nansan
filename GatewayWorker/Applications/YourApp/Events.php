<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;

/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        var_dump($client_id);
        // 向当前client_id发送数据 
        Gateway::sendToClient($client_id, json_encode(['type'=>'client_id','content'=>$client_id]));
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $message)
   {
//       $message 为json字符串
//       var_dump($message);
        $message = json_decode(stripslashes($message),true);
//       var_dump($message);
        if($message['type'] == 'heartbeat'){
            //心跳
//             var_dump($client_id.'心跳监控');
            if (Gateway::getUidByClientId($client_id)){
                $user_init = true;
            }else{
                $user_init = false;
            }
            Gateway::sendToClient($client_id, json_encode(['type'=>'heartbeat','content'=>$client_id,'user_init'=>$user_init],JSON_UNESCAPED_UNICODE));
        }elseif ($message['type'] == 'login'){
           //首次登录绑定
//           var_dump($client_id.'绑定'.$message['user_id']);
           Gateway::bindUid( $client_id,$message['user_id']);//将用户id与getwayWorker分配的链接id绑定
       }elseif($message['type'] == 'sendToUid'){
//           var_dump($message);
           //单聊
           if (isset($message['content'])){
               //没传内容我也不推了
                $send_id = Gateway::getUidByClientId($client_id);//获取发送人的id
                if($send_id){
                   //接收方正常登陆在线
                   $message['from_id'] = $send_id;
                   if(isset($message['to_id'])){
                       $message['friend_id'] = $message['from_id'];
                       Gateway::sendToUid($message['to_id'],json_encode($message,JSON_UNESCAPED_UNICODE));
                   }
                   $message['friend_id'] = $message['to_id'];
                   Gateway::sendToUid($send_id,json_encode($message,JSON_UNESCAPED_UNICODE));
                }
           }
       }elseif ($message['type'] == 'sendToGroup'){
//           var_dump($message);
           //群聊
           if (isset($message['group_id']) && isset($message['content'])){
               $send_id = Gateway::getUidByClientId($client_id);//获取发送人的id
               if($send_id){
                   //为获取到发送人的id就不发送啦
                   $message['from_id'] = $send_id;
                   Gateway::sendToGroup($message['group_id'],  json_encode($message,JSON_UNESCAPED_UNICODE), [] ,false);
               }
           }
       }elseif($message['type'] == 'joinGroup'){
           //加入群聊
           if (isset($message['group_ids'])){
               var_dump($client_id.'进入群'.$message['group_ids']);
               $group_ids = explode(',',trim($message['group_ids'],','));
               foreach ($group_ids as $group_id){
                   Gateway::joinGroup($client_id,$group_id);
               }
           }
       }elseif ($message['type'] == 'leaveGroup'){
           //离开群聊
           if (isset($message['group_id'])){
               Gateway::leaveGroup($client_id,$message['group_id']);
           }
       }elseif ($message['type'] == 'ungroup'){
           //解散群聊
           if (isset($message['group_id'])){
               Gateway::ungroup($message['group_id']);
           }
       }elseif ($message['type'] == 'qunzhutiren'){
           //群主踢人
           if (isset($message['group_id']) && isset($message['user_id'])){
               //参数不全不处理
               if (Gateway::isUidOnline( $message['user_id'])){
                   //不在线不处理
                   if ($arr = Gateway::getClientIdByUid($message['user_id'])){
                       //获取对应uid的所有有效client_id
                       foreach ($arr as $value){
                           //把所有的client_id都踢出去
                           Gateway::leaveGroup($value,$message['group_id']);
                       }
                   }
               }
           }
       }
   }
   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id)
   {
       // 向所有人发送 
       var_dump("$client_id logout\r\n");
    //   GateWay::sendToAll("$client_id logout\r\n");
   }
}
