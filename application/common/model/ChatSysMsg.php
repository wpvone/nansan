<?php


namespace app\common\model;


class ChatSysMsg extends \think\Model
{
    public function noread(){
        return $this->belongsTo('chat_msg_noread','msg_id','id');
    }
}