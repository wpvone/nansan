<?php


namespace app\common\model;


class ChatGroupuser extends \think\Model
{
    public function groupinfo(){
        return $this->belongsTo('chat_group','chat_group_id','id')->field('id,name,avatar');
    }
    public function user(){
        return $this->belongsTo('user','user_id','id')->field('id,nickname,avatar');
    }
    public function friend(){
        return $this->belongsTo('chat_friend','user_id','friend_id');
    }
}