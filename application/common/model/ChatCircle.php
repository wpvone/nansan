<?php

namespace app\common\model;

use think\Model;

class ChatCircle extends Model
{
    protected $name = 'chat_circle';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->field('id,nickname,avatar');
    }

    public function getCreatetimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }
}