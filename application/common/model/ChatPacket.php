<?php


namespace app\common\model;


class ChatPacket extends \think\Model
{
    public function parketmaster()
    {
        return $this->belongsTo('user')->field('id,nickname,avatar');
    }
}