<?php

namespace app\common\model;

use think\Model;
use traits\model\SoftDelete;

class OrderProduct extends Model
{
    use SoftDelete;
    // 表名,不含前缀
    protected $name = 'order_product';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
    ];

    public function order()
    {
        return $this->belongsTo('Order', 'order_id');
    }

    public function product()
    {
        return $this->belongsTo('Product', 'product_id');
    }

    public function AttrSku()
    {
        return $this->belongsTo('Product', 'attr_sku_id');
    }
}
