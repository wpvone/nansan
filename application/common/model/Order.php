<?php

namespace app\common\model;

use think\Model;
use traits\model\SoftDelete;

/**
 * Class Order
 * @package app\common\model
 *
 * 订单model
 */
class Order extends Model
{
    use SoftDelete;
    // 表名,不含前缀
    protected $name = 'order';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
        'pay_type_text'
    ];

    //关联product
    public function product()
    {
        return $this->belongsTo('Product', 'product_id')->field('id, categories_id,title,head_image,sell_num');
    }

    // order_product
    public function orderProduct()
    {
        return $this->hasMany('OrderProduct', 'order_id')
            ->field('id, order_id, product_id, product_title, attr_sku_id, one_price, description, image, attr_sku_name,buy_num, product_price, delivery_price, order_price');
    }

    protected function getAddressJsonAttr($v)
    {
        return json_decode($v, true);
    }

    public function business()
    {
        return $this->belongsTo('Business', 'business_id')
            ->field('id, name, createtime, image, brand');
    }

    /**
     *
     * @param $where  '订单状态:1=待付款,2=退款中,3=待发货,4=待收货,5=待评价,6=订单完成,7=取消订单审核中,8=订单已取消'
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function orderList($where, $page, $pageSize)
    {
        $data = self::with(['orderProduct', 'business'])
            ->where($where)
            ->field('id, business_id, order_num,user_id,actual_price,createtime, status, express_name, express_sn, pay_type, pay_status')
            ->order('id desc')
            ->paginate($pageSize);
        if($data){
            $data = $data->toArray()['data'];
            foreach ($data as &$item){
                $item['all_num'] = count($item['order_product']);
            }
        }
        return $data;
    }


    /**
     *
     * @param $where  '订单状态:1=待付款,2=退款中,3=待发货,4=待收货,5=待评价,6=订单完成,7=取消订单审核中,8=订单已取消'
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function orderList1($where)
    {
        $data = self::with(['product'=>function($q){
                $q->with(['categories'=>function($query){

                }]);
            }])
            ->where($where)
            ->field('id, order_num,user_id,product_id,attr_sku_id,attr_sku_name,
            one_price,buy_num,total_price,actual_price,actual_price,createtime, status, express_name, express_sn')
            ->order('id desc')
            ->select();
        return $data;
    }
    public function getPayTypeTextAttr($value,$data){
        switch ($data['pay_type']){
            case '1':
                $value = '微信支付';
                break;
            case '2':
                $value = '支付宝支付';
                break;
            case '3':
                $value = '银行卡支付';
                break;
            case '4':
                $value = '余额支付';
                break;
            default:
                $value = '其他';
                break;
        }
        return $value;
    }

    // 时间转化
    protected function getCreatetimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    // 时间转化
    protected function getPayTimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    // 时间转化
    protected function getReceivedTimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    // 时间转化 delivery_time
    protected function getDeliveryTimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    // 时间转化
    protected function getUpdatetimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }



    /**
     * 订单详细
     * $where
     */
    public static function orderDetail($where)
    {
        $data = self::with([ 'orderProduct', 'business'
            ])
            ->where($where)
//            ->field('id, order_num,user_id,product_id,attr_sku_id,attr_sku_name,
//            one_price,buy_num,total_price,actual_price,actual_price,createtime, status, express_name,
//            express_sn, address_json, pay_type, delivery_time,pay_time, received_time')
            ->find();
        if (!$data) return [];
        $express_price = 0;
        $all_price = 0;
        if ($data && $data['orderProduct']) {
            foreach ($data['orderProduct'] as $k => $v) {
                $express_price += $v['delivery_price'];
                $all_price += $v['product_price'];
            }
        }

        $data['total_delivery_price'] = $express_price;
        $data['all_price'] = $all_price;
        return $data;
    }



    /**
     * 订单详细
     * $where
     */
    public static function orderDetail2($where)
    {
        $data = self::with(['product'=>function($q){
            $q->with(['categories'=>function($query){

            }]);
        }])
            ->where($where)
//            ->field('id, order_num,user_id,product_id,attr_sku_id,attr_sku_name,
//            one_price,buy_num,total_price,actual_price,actual_price,createtime, status, express_name,
//            express_sn, address_json, pay_type, delivery_time,pay_time, received_time')
            ->find();

        $data['address_json'] = json_decode($data['address_json'], true);
        return $data;
    }



}



