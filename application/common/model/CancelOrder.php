<?php

namespace app\common\model;

use think\Model;

/**
 * Class CancelOrder
 * @package app\common\model
 *
 * 取消订单申请表
 */
class CancelOrder extends Model
{
    // 表名,不含前缀
    protected $name = 'cancel_order';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    // 追加属性
    protected $append = [
    ];

    public function getCreatetimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    public function getUpdatetimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    public function order()
    {
        return $this->belongsTo('Order', 'order_id')
            ->field('id, order_num, user_id, actual_price, pay_type, pay_time');
    }

    public function orderCancel()
    {
        return $this->belongsTo('OrderCancel', 'order_cancel_id');
    }

    public static function detail($where)
    {
        $data = self::with([
            'order', 'orderCancel'
        ])
            ->where($where)
            ->field('id, user_id, order_id, createtime, type, updatetime, status, order_cancel_id, money, msg, Progress')
            ->find();
        if ($data['msg']) {
            $msg = json_decode($data['msg'], true);
            foreach ($msg as $k => &$v) {
                $msg[$k]['time'] = date('Y-m-d H:i', $v['time']);
            }
            $data['msg'] = $msg;
        }

        return $data;
    }



}
