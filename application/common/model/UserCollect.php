<?php

namespace app\common\model;

use think\Model;

class UserCollect extends Model
{
    protected $name = 'user_collect';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}