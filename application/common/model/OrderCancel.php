<?php

namespace app\common\model;

use think\Model;

/**
 * Class OrderCancel
 * @package app\common\model
 *
 * 取消订单申请原因选项表
 */
class OrderCancel extends Model
{
    // 表名,不含前缀
    protected $name = 'order_cancel';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    // 追加属性
    protected $append = [
    ];

}
