<?php

namespace app\common\model;

use think\Exception;
use think\Model;
use think\Request;
use traits\model\SoftDelete;

/**
 * Class Product
 * @package app\common\model
 * 商品model
 */
class Product extends Model
{
    use SoftDelete;
    // 表名,不含前缀
    protected $name = 'product';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
    ];

    /**
     * 下架不显示
     */
    protected function base($query)
    {
        $query->where([
            'siteswitch' => ['<>', 0],
            'deletetime' => null,
        ]);
    }
    /**
     * @param $query
     * 为您推荐查询条件
     */
    protected function scopeHot($query)
    {
        $query->where('hotswitch',1);
    }

    /**
     * @param $query
     * @param $cid 分类id
     *
     * 根据分类查
     */
    protected function scopeCate($query, $where)
    {
        $query->where($where);
    }

    protected function scopeSearch($query, $str)
    {
        $query->where('title', 'LIKE', '%'. $str .'%');
    }

    protected function getMaincontentAttr($v)
    {
        if (!$v) return '';
        $data = str_replace('<img src="', '<img style="width:100%" src="'. Request::instance()->domain(), $v);
        //$data = preg_replace('<img src="', '<img src="'. Request::instance()->domain(), $v);
        // return preg_replace('/style=".*?"/', 'style="width: 100%;"', $data);
        return $data;
    }

    protected function getDescriptionAttr($v)
    {
        if (!$v) return '';
        return mb_substr($v, 0, 60);
    }

    protected function getTitleAttr($v)
    {
        if (!$v) return '';
        return mb_substr($v, 0, 60);
    }

    /**
     * 关联unit单位表
     */
    public function unit()
    {
        return $this->hasOne('Unit', 'id', 'unit_id')->field('id, key, value');
    }

    /**
     * 关联delivery_tpl运费模块
     */
    public function deliTpl()
    {
        return $this->hasOne('DeliveryTpl', 'id', 'delivery_tpl_id')->field('id, title, type');
    }

    protected function getDeployAttr($v)
    {
        return json_decode($v);
    }

    /**
     * 关联attr表
     */
    public function attrSku()
    {
        return $this->hasMany('AttrSku', 'product_id', 'id');
    }

    /**
     * 关联attr表1次
     */
    public function attrSkuOne()
    {
        return $this->hasOne('AttrSku', 'product_id', 'id');
    }

    /**
     * 关联business表
     */
    public function business()
    {
        return $this->belongsTo('Business', 'business_id')
            ->field('id,name,image,brand');
    }



    /**
     * @param $v
     * @return array
     *
     * 转化评论图片
     *
     */
    protected function getImagesAttr($v)
    {
        return explode(',', $v);
    }

    /**
     * 关联favo收藏表
     */
    public function favo()
    {
        return $this->hasOne('UserFavo', 'product_id', 'id')->field('id, user_id, product_id');
    }



    /**
     * 商品列表(为您推荐)
     *
     * @param int $page
     * @param int $limit
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function hotList($page = 1, $limit = 4)
    {
        $data = self::scope('hot')
            ->with(['categories'])
            ->field('id, categories_id, title, description, head_image, sell_num, low_price')
            ->page($page , $limit)
            ->select();
        return $data;
    }

    /**
     * @param $type
     * @return string
     *
     * 根据类型得到排序条件
     */
    protected static function getType($type)
    {
        $order = '';
        switch ($type) {
            case 1: // 销量
                $order = 'sell_num desc';
                break;
            case 2: // 好评率
                $order = 'favo_coms desc';
                break;
            case 3:
                $order = 'low_price asc';
                break;
            case 4:
                $order = 'low_price desc';
                break;
            default :
                break;
        }

        return $order;
    }


    /**
     * @param array $where 分类
     * @param int $order
     * @param int $page
     * @param int $limit
     *
     * 根据分类查询
     */
    public static function cateProList(array $where, $order, $page = 1, $pageSize = 4)
    {
        if (!is_array($where)) {
            throw new Exception('参数错误');
        }
        $field = 'id, unit_id, delivery_tpl_id, categories_id, read_num, 
        title, description, maincontent, head_image, images, sell_num,low_price, 
        from_address, deploy,default_exp_price,business_id';
        $data = self::where($where)
            ->with(['attrSku','deliTpl','unit','business'])
            ->field($field)
            ->order($order)
            ->paginate($pageSize);
        if($data){
            $data = $data->toArray()['data'];
        }
        return $data;
    }

    /**
     * @param string $str
     * @param int $type
     * @param int $page
     * @param int $limit
     * @return false|\PDOStatement|string|\think\Collection
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     *
     * 搜索显示信息
     */
    public static function searchList($str = '', $type = 1, $page = 1, $limit = 4)
    {
        if (!$str) {
            throw new Exception('参数错误');
        }

        $order = self::getType($type);

        $data = self::scope('search', $str)
            ->with('categories')
            ->field('id, categories_id, title, description, head_image, sell_num,low_price')
            ->order($order)
            ->page($page , $limit)
            ->select();

        return $data;
    }

    /**
     * @param $id 商品id
     * @param  $uid 用户id
     * @return array|bool|false|\PDOStatement|string|Model
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     *
     * 商品详细(用户登录)
     */
    public static function proDetail($id)
    {
        $field = 'id, unit_id, delivery_tpl_id, categories_id, read_num, 
        title, description, maincontent, head_image, images, sell_num,low_price, 
        from_address, deploy,default_exp_price,business_id';
        $data = self::with(['attrSku', 'deliTpl', 'unit', 'business'])
            ->field($field)
            ->find($id);
        $data->read_num++;
        $data->save();
        $data = $data->toArray();
        $sku = 0;
        if ($data['attr_sku']) {
            $sku = array_sum(array_column($data['attr_sku'],'stock'));
        }
        $data['stock'] = $sku;
        $data['attrGroup'] = []; // 规格名集合
        $data['attrItems'] = []; // 规格值集合
        if (isset($data['attr_sku']) && $data['attr_sku']) {
            $data['attrGroup'] = explode(',', $data['attr_sku'][0]['keys']);
            $attr = array_column($data['attr_sku'],'value');
            $count = count($data['attrGroup']);
            $arr = [];

            /**
             * 可以优化下先这么用
             */
            foreach ($attr as $k => $value) {
                $attr[$k] = explode(',', $value);
                for ($i=0; $i < $count; $i++) {
                    if (!isset($arr[$i]))  $arr[$i] = [];
                    if (!in_array($attr[$k][$i], $arr[$i]))
                        array_push($arr[$i], $attr[$k][$i]);
                }
            }
            $data['attrItems'] = $arr;
        }
        return $data;
    }

    /**
     * 商品详细==>>确认订单
     */
    public static function goodsDetail($product_id, $attr_id)
    {
        /**
         * 获取下单信息
         */
        $data = self::with([
            'deliTpl' => function($q) {
                $q->with([
                    'deliveryRule' => function($qu){
                        $qu->order('id desc');
                    }]);
            },
            'attrSkuOne' => function($q)use($attr_id) {
                $q->where('id', $attr_id);
            },
            'unit',
            'business'
        ])->field('id, title, description, head_image, read_num, sell_num, unit_id, delivery_tpl_id, default_exp_price, business_id')
//            ->group('business_id')
            ->find($product_id);
        return $data;
    }

}
