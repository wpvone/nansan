<?php


namespace app\common\model;


class ChatFriend extends \think\Model
{
    public function friend(){
        return $this->belongsTo('user','friend_id','id')->field('id,nickname,avatar,idnum,gender');
    }
}