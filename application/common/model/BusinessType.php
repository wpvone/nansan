<?php

namespace app\common\model;

use think\Model;

class BusinessType extends Model
{
    protected $name = 'business_type';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
}