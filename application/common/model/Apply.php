<?php

namespace app\common\model;

use think\Model;

class Apply extends Model
{
    protected $name = 'apply';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
}