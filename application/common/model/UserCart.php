<?php

namespace app\common\model;

use think\Model;

class UserCart extends Model
{
    // 表名
    protected $name = 'user_cart';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    // 追加属性
    protected $append = [
    ];

//    protected  function base($query)
//    {
//        $query->where('is_show', 1);
//    }

    /**
     * @param $q
     * @param $uid
     *
     * 指定用户id
     */
    protected function scopeUser($q, $uid)
    {
        $q->where('user_id', $uid);
    }

    // 关联product表
    public function product()
    {
        return $this->belongsTo('Product', 'product_id')
            ->field('id, categories_id, title, head_image, sell_num, low_price, delivery_tpl_id, default_exp_price');
    }

    //关联business
    public function business()
    {
        return $this->belongsTo('Business', 'business_id');
    }


    // attr_sku
    public function attrSku()
    {
        return $this->belongsTo('AttrSku', 'attr_sku_id');
    }



    /**
     * @param $uid 用户ID
     * @param int $page
     * @param int $limit
     *
     * 获取购物车列表
     */
    public static function getList($uid, $page=1, $limit=4)
    {
        if (!$uid)  return false;

        $data = self::scope('user', $uid)
            ->with([
                'product' => function($q) {
                    $q->with(['categories'])->field('id, categories_id, title, description, head_image, sell_num, low_price');
                },
                'attrSku',
                'business'
            ])
            ->where('is_show', 1)
            ->order('id desc')
            ->page($page, $limit)
//            ->group('business_id')
            ->select();
        $count = self::scope('user', $uid)
            ->with([
                'product' => function($q) {
                    $q->with(['categories'])->field('id, categories_id, title, description, head_image, sell_num, low_price');
                },
                'attrSku',
            ])
            ->where('is_show', 1)
            ->count();


        return ['data' => $data, 'count' => $count];
    }

}
