<?php

namespace app\common\model;

use think\Model;

class PostType extends Model
{
    protected $name = 'post_type';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

}