<?php

namespace app\common\model;

use think\Model;

class Business extends Model
{
    protected $name = 'business';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    // 关联usercart
    public function userCart()
    {
        return $this->hasMany('UserCart', 'business_id', 'id');
    }
}
