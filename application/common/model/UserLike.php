<?php

namespace app\common\model;

use think\Model;

class UserLike extends Model
{
    protected $name = 'user_like';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
}