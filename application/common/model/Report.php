<?php

namespace app\common\model;

use think\Model;

class Report extends Model
{
    protected $name = 'report';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
}