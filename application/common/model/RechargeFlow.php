<?php

namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;
use think\exception\ValidateException;
use think\Validate;

/**
 * Class RechargeFlow
 * @package app\common\model
 *
 * 充值表
 */
class RechargeFlow extends Model
{
    protected $name = 'recharge_flow';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    // 追加属性

    // 时间转化
    protected function getCreatetimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    /**
     * 新增数据
     * @param $uid  用户id
     * @param $type 充值方式:1=微信,2=支付宝,3=银行卡,4=其他
     * @param $money 金额
     * @param int $status  状态:1=处理中,2=成功,3=失败
     * @param string $remark 备注
     *
     * 充值计入流水
     */
    public static function addInfo($uid, $type, $money, $status=2, $remark='充值')
    {
        $param = [
            'user_id' => $uid,
            'type' => $type,
            'money' => $money,
            'status' => $status,
            'remark' => $remark,
            'createtime' => time(),
            'flow_sn' => makeOrder()
        ];
        $rule = [
            'user_id|用户ID' => 'require|number',
            'type|充值' => 'require|in:1,2,3,4',
            'money|金额' => 'require|gt:0',
            'status|状态' => 'in:1,2,3',
            'remark' => 'chsDash',
        ];

        $validate = new Validate($rule);
        if (!$validate->check($param)) return ['code' => 0, 'msg' => $validate->getError()];
        Db::startTrans();
        try {
            self::insert($param);
            Db::name('user')->where('id', $uid)->setInc('money', $money);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
           return ['code' => 0, 'msg' => $e->getMessage()];
        }
        return ['code' => 1, 'msg' => '充值成功'];
    }

    /**
     * 获取流水
     *
     * $uid
     */
    public static function getInfo($uid, $param)
    {
        extract($param);

        $data = self::where('user_id', $uid)
            ->order('id desc')
            ->page($page, $limit)
            ->select();

        return $data;
    }

}
