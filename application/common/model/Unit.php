<?php

namespace app\common\model;

use think\Model;
use traits\model\SoftDelete;

/**
 * Class Unit
 * @package app\common\model
 * 商品单位模型
 */
class Unit extends Model
{
    use SoftDelete;
    // 表名,不含前缀
    protected $name = 'unit';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
    ];
}
