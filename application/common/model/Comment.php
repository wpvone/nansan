<?php

namespace app\common\model;

use think\Model;

class Comment extends Model
{
    protected $name = 'comment';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getCreatetimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }

    public function getUpdatetimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }
}