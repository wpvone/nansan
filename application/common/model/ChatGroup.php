<?php


namespace app\common\model;


class ChatGroup extends \think\Model
{
    public function groupuser(){
        return $this->hasMany('chat_groupuser','chat_group_id','id');
    }
}