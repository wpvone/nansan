<?php

namespace app\common\model;

use think\Model;
use traits\model\SoftDelete;

class DeliveryTpl extends Model
{
    use SoftDelete;
    // 表名,不含前缀
    protected $name = 'delivery_tpl';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
    ];

    // 关联rule表
    public function deliveryRule()
    {
        return $this->hasMany('DeliveryRule', 'tpl_id', 'id');
    }

    /**
     *
     *获取列表
     */
    public static function getInfo($id)
    {
        $data = self::with(['deliveryRule' => function($q){
            $q->order('id desc');
        }])
            ->find($id);

        return $data;
    }
}
