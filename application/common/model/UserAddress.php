<?php

namespace app\common\model;

use think\Db;
use think\Exception;
use think\exception\ValidateException;
use think\Model;
use think\Validate;
use traits\model\SoftDelete;

class UserAddress extends Model
{
    use SoftDelete;
    // 表名
    protected $name = 'user_address';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    // 追加属性
    protected $append = [
    ];

    // 关联
    public function area()
    {
        return $this->belongsTo('Area', 'area_id')->field('id, pid, mergename, zip');
    }

    /**
     * 列表
     */
    public static function getList($uid)
    {
        $list = self::where('user_id', $uid)
            ->with('area')->order('id desc')->field('id, area_id, address, name, mobile, is_default, detail_address')
            ->select();

        return $list;

    }

    /**
     * 新增
     */
    public static function addInfo($uid, $param)
    {
        $rule = [
            'name|收件人名' => 'require|chsAlpha',
            'mobile|号码' => 'require|max:11|/^1[3456789]{1}\d{9}$/',
            'detail_address|详细地址' => 'require|chsAlphaNum',
            'address|地址' => 'require'
        ];
        $validate = new Validate($rule);

        if (!$validate->check($param)) {
            throw new ValidateException($validate->getError());
        }

        extract($param);
        $address = str_replace('/', ',', $address);
        $area_id = Db::name('area')->where('mergename', '=', '中国,'.$address)
            ->find();
        $first = Db::name('area')->where('id', '=', $area_id['pid'])
            ->find();
        if ($first) {
            $tree_ids = $first['pid'] . ','. $first['id'] . ',' . $area_id['id'];
        } else {
            $tree_ids = $area_id['pid'] .  ',' . $area_id['id'];
        }

        $res = self::where([
            'user_id' => $uid,
            'area_id' => $tree_ids,
            'name' => $name,
            'mobile' => $mobile,
            'detail_address' => $detail_address
        ])->find();
        if ($res) return false;
        if ($is_default == 2) {
            $addressHand = self::where(['is_default' => 2,  'user_id' => $uid])->update([
                'is_default' => '1',
                'updatetime' => time()
            ]);

        }

         $ret =  self::insert([
            'user_id' => $uid,
            'address' => $address,
            'area_id' => $tree_ids,
            'name' => $name,
            'mobile' => $mobile,
            'detail_address' => $detail_address,
             'is_default' => $is_default,
             'createtime' => time(),
//             'code' => $code
        ]);

        return $ret;
    }

    /**
     *
     * 更新
     */
    public static function updateInfo($uid, $param)
    {
        $rule = [
            'name|收件人名' => 'require|chsAlpha',
            'mobile|号码' => 'require|length:11',
            'detail_address|详细地址' => 'require|chsAlphaNum',
            'address|地址' => 'require',
            'id|ID' => 'require',
            'is_default' => 'require|number|length:1'
        ];
        $validate = new Validate($rule);

        if (!$validate->check($param)) {
            throw new ValidateException($validate->getError());
        }
        extract($param);
        $hander = self::get($id);
        if (!$hander) throw new Exception('没有此收货地址信息');


        $address = str_replace('/', ',', $address);
        $area_id = Db::name('area')->where('mergename', '=', '中国,'.$address)
            ->find();
        $first = Db::name('area')->where('id', '=', $area_id['pid'])
            ->find();
        if ($first) {
            $tree_ids = $first['pid'] . ','. $first['id'] . ',' . $area_id['id'];
        } else {
            $tree_ids = $area_id['pid'] .  ',' . $area_id['id'];
        }



        if ($is_default == 2) {

            $addressHand = self::where(['is_default' => 2,  'user_id' => $uid])->update([
                'is_default' => '1',
                'updatetime' => time()
            ]);
        }

        Db::name('user_address')->where('id', $hander['id'])->update([
            'name' => $name,
            'mobile' => $mobile,
            'detail_address' => $detail_address,
            'address' => $address,
            'is_default' => $is_default,
            'area_id' => $tree_ids,
//            'code' => $code
        ]);

        return true;
    }

}
