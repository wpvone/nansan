<?php

namespace app\common\model;

use think\Model;

class Question extends Model
{
    protected $name = 'question';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
}