<?php


namespace app\common\model;

/**
 * Class 好友申请
 * @package app\common\model
 */
class ChatFriendApply extends \think\Model
{
    public function sendUserInfos(){
        return $this->belongsTo('user','send_user_id','id')
            ->field('id,avatar,nickname,gender');
    }
    public function getUserInfos(){
        return $this->belongsTo('user','get_user_id','id')
            ->field('id,avatar,nickname,gender');
    }
}