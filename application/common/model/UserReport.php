<?php

namespace app\common\model;

use think\Model;

class UserReport extends Model
{
    protected $name = 'user_report';
    protected $autoWriteTimestamp = 'int';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
}