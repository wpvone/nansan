<?php

namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;
use think\exception\ValidateException;
use think\Validate;

/**
 * Class CashFlow
 * @package app\common\model
 *
 * 提现表
 */

class CashFlow extends Model
{
    protected $name = 'cash_flow';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    // 追加属性

    // 时间转化
    protected function getCreatetimeAttr($v)
    {
        if (!$v) return '';
        return date('Y-m-d H:i', $v);
    }

    //管连bank
    public function userBank()
    {
        return $this->belongsTo('UserBank', 'user_bank_id', 'id')->field('id, user_id, bank_id, bank_name, account_name, bank_account');
    }



    /**
     *
     * @param $uid  用户id
     * @param $type 提现类型 1=银行卡,2=其他
     * @param $money 金额
     * @param int $status  状态:1=处理中,2=成功,3=失败
     * @param string $user_bank_id银行ID
     * @param string $remark 备注
     *
     * 提现计入流水
     */
    public static function addInfo($uid,  $money, $bankInfo, $remark='提现', $type=1, $status = 1)
    {
        $param = [
            'user_id' => $uid,
            'type' => $type,
            'money' => $money,
            'status' => $status,
            'remark' => $remark,
            'createtime' => time(),
            'flow_sn' => makeOrder(),
            'bankName' => $bankInfo['bank_name'],
            'bank_num' => $bankInfo['bank_num'],
            'bank_name_zhi' => $bankInfo['bank_name_zhi'],
            'card_name' => $bankInfo['card_name']
        ];
        $rule = [
            'user_id|用户ID' => 'require|number',
            'type|提现类型' => 'require|in:1,2',
            'money|金额' => 'require|gt:0',
            'status|状态' => 'in:1,2,3',
            'remark' => 'chsDash',
        ];

        $validate = new Validate($rule);
        if (!$validate->check($param)) throw new ValidateException($validate->getError());
        $res = self::insert($param);
        if (!$res) throw new Exception();

        return true;
    }

    /**
     * 获取流水
     *
     * $uid
     */
    public static function getInfo($uid, $param)
    {
        extract($param);
        $data = self::where('user_id', $uid)
            ->order('id desc')
            ->page($page, $limit)
            ->select();

        return $data;
    }

}
