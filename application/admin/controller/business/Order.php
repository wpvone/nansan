<?php

namespace app\admin\controller\business;

use app\common\controller\Backend;

/**
 * 商品订单管理
 *
 * @icon fa fa-circle-o
 */
class Order extends Backend
{
    
    /**
     * Order模型对象
     * @var \app\admin\model\business\Order
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\business\Order;
        $this->view->assign("isEvaluateList", $this->model->getIsEvaluateList());
        $this->view->assign("payTypeList", $this->model->getPayTypeList());
        $this->view->assign("payStatusList", $this->model->getPayStatusList());
        $this->view->assign("isDeliveryList", $this->model->getIsDeliveryList());
        $this->view->assign("isReceivedList", $this->model->getIsReceivedList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['user'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['user'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('user')->visible(['username','nickname']);
            }
            $list = collection($list)->toArray();
            foreach ($list as $k => $value) {
                $list[$k]['address_json'] = json_decode($value['address_json'], true);
                $list[$k]['pay_time'] = date('Y-m-d H:i', $value['pay_time']);
                $list[$k]['delivery_time'] = date('Y-m-d H:i', $value['delivery_time']);
                $list[$k]['received_time'] = date('Y-m-d H:i', $value['received_time']);
            }
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 发货
     */
    public function send($ids)
    {
       if ($this->request->isPost()) {

           $row = $this->model->get($ids);
           if (!$row) {
               $this->error(__('No Results were found'));
           }

//           $param = $this->request->post('row/a');

//           $row->express_sn = $param['express_sn'];
//           $row->express_name = $param['express_name'];
           $row->is_delivery = '1';
           $row->status = '4';
           $row->delivery_time = time();
           $row->save();

           $this->success('操作成功!');
       }

       $this->assign('id', $ids);
       return $this->view->fetch();
    }


    /**
     * 编辑
     */
    public function sedit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $row->express_name = $params['express_name'];
                $row->express_sn = $params['express_sn'];
                $row->save();
                $this->success('修改成功!');
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    /**
     * 确认付款
     */
    public function pay($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            // 确认付款
            $row->pay_type = '5';
            $row->pay_status = '2';
            $row->pay_time = time();
            $row->status = '3';
            $row->updatetime = time();
            $row->save();

            $this->success('操作成功!');
        }
    }


}


