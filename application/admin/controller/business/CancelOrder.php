<?php

namespace app\admin\controller\business;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use app\common\model\Order;

/**
 * 订单取消退款审核
 *
 * @icon fa fa-circle-o
 */
class CancelOrder extends Backend
{
    
    /**
     * CancelOrder模型对象
     * @var \app\admin\model\business\CancelOrder
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\business\CancelOrder;
        $this->view->assign("typeList", $this->model->getTypeList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['user','order','ordercancel'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['user','order','ordercancel'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('user')->visible(['username']);
				$row->getRelation('order')->visible(['order_num','product_id','attr_name','sku_name','one_price','buy_num','total_price']);
				$row->getRelation('ordercancel')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 通过()
     */
    public function pass($ids)
    {
        if ($this->request->isAjax()) {
            $coModel = new \app\common\model\CancelOrder();
            $hander = $coModel::get($ids);
            if (!$hander) $this->error('暂无数据!');
            Db::startTrans();
            try {
                $orderHand = Order::get($hander->order_id);
                $hander->status = '2'; // 状态:1=待审核,2=成功,3=驳回
                $orderHand->status = '8'; //订单已取消

                $hander->Progress = '审核通过';
                $oldMsg = json_decode($hander->msg, true);
                $newMsg = [
                    'title' => '审核通过,申请成功!',
                    'time' => time()
                ];
                array_unshift($oldMsg, $newMsg);
                $hander->msg = json_encode($oldMsg);

                $opModel = new \app\common\model\OrderProduct();
                $attrIds = $opModel->where('order_id', $hander->order_id)->select();

                foreach ($attrIds as $k => $v) {
                    Db::name('attr_sku')->where('id', $v['attr_sku_id'])->setInc('stock', $v['buy_num']); // 回库存
                }

                // 退钱到用户账户

                if ($hander->type == 2) { // 1=取消订单,2=申请退款
                    Db::name('user')->where('id', $orderHand->user_id)->setInc('money', $hander->money);
                }

                $orderHand->save();
                $hander->save();
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error('操作失败!');
            }

        }
        $this->success('操作成功!');
    }

    /**
     * 驳回
     */
    public function reject($ids)
    {
        if ($this->request->isAjax()) {
            $coModel = new \app\common\model\CancelOrder();
            $hander = $coModel::get($ids);
            if (!$hander) $this->error('暂无数据!');
            Db::startTrans();
            try {


                $hander->Progress = '审核驳回';
                $oldMsg = json_decode($hander->msg, true);
                $newMsg = [
                    'title' => '审核完成,您的申请被驳回!',
                    'time' => time()
                ];
                array_unshift($oldMsg, $newMsg);
                $hander->msg = json_encode($oldMsg);

                $orderHand = Order::get($hander->order_id);
                $hander->status = '3'; // 状态:1=待审核,2=成功,3=驳回

                if ($hander->type == 1) { // 1=取消订单,2=申请退款
                    $orderHand->status = '1'; //回滚待付款
                } else {
                    $orderHand->status = '3'; //回滚待发货
                }
                $orderHand->save();
                $hander->save();

                 Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error('操作失败!');
            }

        }
       $this->success('操作成功!');
    }


    /**
     * 进度更新
     */
    public function update($ids='')
    {
        if ($this->request->isAjax()) {
            $param =$this->request->post('row/a');

            $row = $this->model->get($ids);
            if (!$row) {
                $this->error('暂无信息!');
            }

            $row->Progress = $param['Progress'];
            $oldMsg = json_decode($row->msg, true);

            $newMsg = json_decode($param['msg'], true);

            foreach ($newMsg as $k => &$value) {
                $newMsg[$k]['time'] = strtotime($value['time']);
                array_unshift($oldMsg, $newMsg[$k]);
            }

            $row->msg = json_encode($oldMsg);
            $row->save();

            $this->success('操作成功!');
        }

       return $this->view->fetch();
    }
}
