<?php

namespace app\admin\controller\business;

use app\admin\library\Tools;
use app\common\controller\Backend;
use app\common\model\AttrSku;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 商品管理
 *
 * @icon fa fa-circle-o
 */
class Product extends Backend
{
    
    /**
     * Product模型对象
     * @var \app\admin\model\business\Product
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\business\Product;
        $this->modelValidate = true;
        $this->view->assign("siteswitchList", $this->model->getSiteswitchList());
        $this->view->assign("hotswitchList", $this->model->getHotswitchList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
//        $this->searchFields = ['title', 'description'];
        $bid = input('ids');
        $this->assign('bid', $bid);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            $bid = input('bid');
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->where('product.business_id', $bid)
                    ->with(['deliverytpl', 'skus'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where('product.business_id', $bid)
                    ->with(['deliverytpl',  'skus'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
				$row->getRelation('deliverytpl')->visible(['title']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $bid = input("bid");
        $this->assign('bid',$bid);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {

                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }


                $attrs = $this->request->post("attrs/a");
                if (empty(json_decode($attrs['sku']))) {
                    return $this->error("请设置商品规格");
                }
                $rules = [
                    'group' => 'require|array|min:1',
                    'market_price' => 'require|array|min:1',
                    'price' => 'require|array|min:1',
                    'stock' => 'require|array|min:1',
                    'weight' => 'require|array|min:1',
                    'sku' => 'require'
                ];
                $msg = [
                    'group.require' => '请设置商品规格'
                ];
                $result = $this->validate($attrs, $rules, $msg);
                if (true !== $result) {
                    return $this->error($result);
                }
                if (count($attrs['group']) != count(json_decode($attrs['sku'])[0])) {
                    return $this->error('规格子项不可以为空');
                }

                $params['market_price'] = $attrs['market_price'][0];

                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);

                    $data = AttrSku::createOrUpdate($this->model, $attrs);

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $bid = input("bid");
        $this->assign('bid',$bid);
        $row = $this->model->with(['skus'])->find($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);


                $attrs = $this->request->post("attrs/a");
                if (empty(json_decode($attrs['sku']))) {
                    return $this->error("请设置商品规格");
                }
                $rules = [
                    'group' => 'require|array|min:1',
                    'market_price' => 'require|array|min:1',
                    'price' => 'require|array|min:1',
                    'stock' => 'require|array|min:1',
                    'weight' => 'require|array|min:1',
                    'sku' => 'require'
                ];
                $msg = [
                    'group.require' => '请设置商品规格'
                ];
                $result = $this->validate($attrs, $rules, $msg);
                if (true !== $result) {
                    return $this->error($result);
                }
                if (count($attrs['group']) != count(json_decode($attrs['sku'])[0])) {
                    return $this->error('规格子项不可以为空');
                }
                $params['market_price'] = $attrs['market_price'][0];
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    $data = AttrSku::createOrUpdate($row, $attrs);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $attrItems = [];
        if (!empty($row->skus)) {
            $result = [];
            foreach ($row->skus as $i => $item) {
                $result[] = explode(',', $item->value);
            }
            $attrItems = Tools::reverseDescartes($result);
        }
        $this->view->assign('attrItems', collection($attrItems));
        $this->view->assign('skus', json_encode($row['skus']));

        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
