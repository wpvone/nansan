<?php

namespace app\admin\controller\business;

use app\common\controller\Backend;

/**
 * Dashboard
 *
 * @icon fa fa-circle-o
 */
class Index extends Backend
{
    /**
     * 选择城市
     */
    public function citySelector()
    {
        if ($this->request->isAjax()) {
            $list = \app\admin\model\Area::getTreeArray([1, 2]);
            $list = [['id' => 0, 'name' => '全国', 'childlist' => $list]];
            return $this->success('', null, $list);
        }
        return $this->view->fetch();
    }

}
