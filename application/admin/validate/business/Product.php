<?php

namespace app\admin\validate\business;

use think\Validate;

class Product extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'title|商品标题' => 'require|max:60',
        'description|商品描述' => 'require|max:60'
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => ['title', 'description'],
        'edit' => ['title', 'description'],
    ];

}
