<?php

namespace app\admin\model\mall;

use think\Model;

class Business extends Model
{
    // 表名
    protected $name = 'business';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    // 追加属性
    protected $append = [

    ];
    

    







    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'admin_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function apply()
    {
        return $this->belongsTo('app\admin\model\Apply', 'apply_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
