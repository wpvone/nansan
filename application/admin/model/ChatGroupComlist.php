<?php

namespace app\admin\model;

use think\Model;


class ChatGroupComlist extends Model
{

    

    

    // 表名
    protected $name = 'chat_group_comlist';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function chatgroup()
    {
        return $this->belongsTo('ChatGroup', 'group_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
