<?php

namespace app\admin\model;

use think\Model;


class ChatGroupuser extends Model
{

    

    

    // 表名
    protected $name = 'chat_groupuser';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_top_text'
    ];
    

    
    public function getIsTopList()
    {
        return ['1' => __('Is_top 1'), '2' => __('Is_top 2')];
    }


    public function getIsTopTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_top']) ? $data['is_top'] : '');
        $list = $this->getIsTopList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function chatgroup()
    {
        return $this->belongsTo('ChatGroup', 'chat_group_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
