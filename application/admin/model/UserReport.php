<?php

namespace app\admin\model;

use think\Model;


class UserReport extends Model
{

    

    

    // 表名
    protected $name = 'user_report';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function report()
    {
        return $this->belongsTo('Report', 'report_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function post()
    {
        return $this->belongsTo('Post', 'post_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
