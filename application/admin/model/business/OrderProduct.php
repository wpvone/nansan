<?php

namespace app\admin\model\business;

use think\Model;
use traits\model\SoftDelete;

class OrderProduct extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'order_product';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [

    ];
    

    







    public function order()
    {
        return $this->belongsTo('app\admin\model\Order', 'order_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function product()
    {
        return $this->belongsTo('app\admin\model\Product', 'product_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function attrsku()
    {
        return $this->belongsTo('app\admin\model\AttrSku', 'attr_sku_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
