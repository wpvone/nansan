<?php

namespace app\admin\model\business;

use think\Model;
use traits\model\SoftDelete;

class Order extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'is_evaluate_text',
        'pay_type_text',
        'pay_status_text',
        'is_delivery_text',
        'delivery_time_text',
        'pay_time_text',
        'is_received_text',
        'received_time_text',
        'status_text'
    ];
    

    
    public function getIsEvaluateList()
    {
        return ['1' => __('Is_evaluate 1'), '2' => __('Is_evaluate 2')];
    }

    public function getPayTypeList()
    {
        return ['1' => __('Pay_type 1'), '2' => __('Pay_type 2'), '3' => __('Pay_type 3'), '4' => __('Pay_type 4'), '5' => __('Pay_type 5')];
    }

    public function getPayStatusList()
    {
        return ['1' => __('Pay_status 1'), '2' => __('Pay_status 2'), '3' => __('Pay_status 3')];
    }

    public function getIsDeliveryList()
    {
        return ['1' => __('Is_delivery 1'), '2' => __('Is_delivery 2')];
    }

    public function getIsReceivedList()
    {
        return ['1' => __('Is_received 1'), '2' => __('Is_received 2')];
    }

    public function getStatusList()
    {
        return ['1' => __('Status 1'), '2' => __('Status 2'), '3' => __('Status 3'), '4' => __('Status 4'), '5' => __('Status 5'), '6' => __('Status 6'), '7' => __('Status 7'), '8' => __('Status 8')];
    }


    public function getIsEvaluateTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_evaluate']) ? $data['is_evaluate'] : '');
        $list = $this->getIsEvaluateList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPayTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_type']) ? $data['pay_type'] : '');
        $list = $this->getPayTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPayStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_status']) ? $data['pay_status'] : '');
        $list = $this->getPayStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsDeliveryTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_delivery']) ? $data['is_delivery'] : '');
        $list = $this->getIsDeliveryList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getDeliveryTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['delivery_time']) ? $data['delivery_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getPayTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['pay_time']) ? $data['pay_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getIsReceivedTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_received']) ? $data['is_received'] : '');
        $list = $this->getIsReceivedList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getReceivedTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['received_time']) ? $data['received_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setDeliveryTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setPayTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setReceivedTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function user()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
