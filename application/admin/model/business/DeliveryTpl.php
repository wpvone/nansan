<?php

namespace app\admin\model\business;

use think\Model;


class DeliveryTpl extends Model
{


    // 表名
    protected $name = 'delivery_tpl';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [

    ];


    public function deliveryRule()
    {
        return $this->hasMany('app\admin\model\business\DeliveryRule', 'tpl_id', 'id');
    }

}
