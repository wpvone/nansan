<?php

namespace app\admin\model\business;

use think\Model;
use traits\model\SoftDelete;

class Product extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'product';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'siteswitch_text',
        'hotswitch_text'
    ];
    

    
    public function getSiteswitchList()
    {
        return ['0' => __('Siteswitch 0'), '1' => __('Siteswitch 1')];
    }

    public function getHotswitchList()
    {
        return ['0' => __('Hotswitch 0'), '1' => __('Hotswitch 1')];
    }


    public function getSiteswitchTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['siteswitch']) ? $data['siteswitch'] : '');
        $list = $this->getSiteswitchList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getHotswitchTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['hotswitch']) ? $data['hotswitch'] : '');
        $list = $this->getHotswitchList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function skus() {
        return $this->hasMany('app\common\model\AttrSku', 'product_id', 'id');
    }

//
//    public function attrs() {
//        return $this->hasMany('app\common\model\AttrSku', 'product_id', 'id');
//    }



    public function categories()
    {
        return $this->belongsTo('app\admin\model\Categories', 'categories_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function deliverytpl()
    {
        return $this->belongsTo('app\admin\model\DeliveryTpl', 'delivery_tpl_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
