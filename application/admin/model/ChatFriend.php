<?php

namespace app\admin\model;

use think\Model;


class ChatFriend extends Model
{

    

    

    // 表名
    protected $name = 'chat_friend';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_black_text'
    ];
    

    
    public function getIsBlackList()
    {
        return ['1' => __('Is_black 1'), '2' => __('Is_black 2')];
    }


    public function getIsBlackTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_black']) ? $data['is_black'] : '');
        $list = $this->getIsBlackList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function friend()
    {
        return $this->belongsTo('User', 'friend_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
