<?php

return [
    'Id'            => 'ID',
    'User_id'       => '用户id',
    'Post_id'       => '帖子id',
    'Report_id'     => '举报id',
    'Createtime'    => '举报时间',
    'Updatetime'    => '更新时间',
    'User.nickname' => '昵称',
    'Report.name'   => '举报名称',
    'Post.name'     => '帖子标题'
];
