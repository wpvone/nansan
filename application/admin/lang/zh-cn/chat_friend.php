<?php

return [
    'Id'            => 'ID',
    'User_id'       => '用户',
    'Friend_id'     => '好友',
    'Note'          => '对好友的备注',
    'Createtime'    => '成为好友时间',
    'Updatetime'    => '变更时间',
    'Is_black'      => '拉黑状态',
    'Is_black 1'    => '正常',
    'Is_black 2'    => '拉黑',
    'User.nickname' => '昵称',
    'User.mobile'   => '手机号',
    'Friend.nickname' => '好友昵称',
    'Friend.mobile'   => '好友手机号'
];
