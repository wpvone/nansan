<?php

return [
    'Id'            => 'ID',
    'User_id'       => '用户ID',
    'Apply_num'     => '入驻号码',
    'Apply_name'    => '入驻名称',
    'Real_name'     => '联系人',
    'Mobile'        => '联系电话',
    'Status'        => '审核状态',
    'Status 1'      => '审核中',
    'Status 2'      => '已同意',
    'Status 3'      => '已驳回',
    'Createtime'    => '申请时间',
    'Updatetime'    => '审核时间',
    'User.nickname' => '昵称'
];
