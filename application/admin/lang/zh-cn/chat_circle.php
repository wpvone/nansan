<?php

return [
    'Id'            => 'ID',
    'User_id'       => '用户',
    'Content'       => '内容',
    'Images'        => '图片',
    'Createtime'    => '创建时间',
    'Updatetime'    => '修改时间',
    'User.nickname' => '昵称'
];
