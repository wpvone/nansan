<?php

return [
    'Id'         => 'ID',
    'Pid'        => '父级ID',
    'User_id'    => '用户ID',
    'Circle_id'  => '朋友圈ID',
    'Content'    => '内容',
    'Createtime' => '创建时间'
];
