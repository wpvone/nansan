<?php

return [
    'Id'            => 'ID',
    'User_id'       => '用户',
    'Friend_id'     => '好友',
    'Why'           => '投诉原因',
    'Createtime'    => '创建时间',
    'User.nickname' => '昵称',
    'Friend.nickname' => '好友昵称'
];
