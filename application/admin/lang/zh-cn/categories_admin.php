<?php

return [
    'Id'            => 'ID',
    'Title'         => '分类名',
    'Weigh'         => '排序',
    'Business_id'   => '店铺',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'Business.name' => '店铺名称'
];
