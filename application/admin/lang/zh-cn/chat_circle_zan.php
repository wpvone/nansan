<?php

return [
    'Id'         => 'ID',
    'User_id'    => '用户ID',
    'To_uid'     => '被赞ID',
    'Circle_id'  => '朋友圈点赞',
    'Comment_id' => '回复ID',
    'Status'     => '状态',
    'Count'      => '计数',
    'Createtime' => '创建时间'
];
