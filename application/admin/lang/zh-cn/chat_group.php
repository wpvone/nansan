<?php

return [
    'Id'            => 'ID',
    'User_id'       => '群主',
    'Name'          => '群名',
    'Avatar'        => '群图像',
    'Createtime'    => '创建时间',
    'Updatetime'    => '修改时间',
    'Note_content'  => '群公告',
    'User.nickname' => '群主'
];
