<?php

return [
    'Id'         => 'ID',
    'Pid'        => '父级id',
    'Name'       => '类型名称',
    'Image'      => '类型图片',
    'Is_show'    => '是否首页显示',
    'Is_show 1'  => '不显示',
    'Is_show 2'  => '显示',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间'
];
