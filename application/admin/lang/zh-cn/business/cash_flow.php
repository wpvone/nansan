<?php

return [
    'Id'                    => 'ID',
    'User_id'               => '用户ID',
    'Flow_sn'               => '流水号码',
    'User_bank_id'          => '银行卡ID',
    'Money'                 => '金额',
    'Type'                  => '提现类型',
    'Type 1'                => '银行卡',
    'Type 2'                => '其他',
    'Createtime'            => '充值时间',
    'Status'                => '状态',
    'Status 1'              => '审核中',
    'Status 2'              => '成功',
    'Status 3'              => '驳回',
    'Remark'                => '备注',
    'Bankname'              => '银行名',
    'Bank_num'              => '银行卡号',
    'Bank_name_zhi'         => '支行名',
    'Card_name'             => '持卡人名',
    'User.username'         => '用户名',
    'User.nickname'         => '昵称',
    'Userbank.bank_name'    => '开户支行名',
    'Userbank.account_name' => '开户人姓名',
    'Userbank.bank_account' => '银行卡号码'
];
