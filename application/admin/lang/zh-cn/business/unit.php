<?php

return [
    'Id'         => 'ID',
    'Key'        => '单位名',
    'Value'      => '英文对应字母',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
