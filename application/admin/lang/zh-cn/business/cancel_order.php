<?php

return [
    'Id'                => 'ID',
    'User_id'           => '用户ID',
    'Order_id'          => '订单ID',
    'Createtime'        => '申请时间',
    'Type'              => '申请类型',
    'Type 1'            => '取消订单',
    'Type 2'            => '申请退款',
    'Updatetime'        => '更新时间',
    'Status'            => '状态',
    'Status 1'          => '待审核',
    'Status 2'          => '成功',
    'Status 3'          => '驳回',
    'Remark'            => '用户备注',
    'Order_cancel_id'   => '申请原因ID',
    'Money'             => '金额',
    'User.username'     => '用户名',
    'Order.order_num'   => '订单编码',
    'Order.product_id'  => '商品ID',
    'Order.attr_name'   => '规格一名',
    'Order.sku_name'    => '规格二名',
    'Order.one_price'   => '单价',
    'Order.buy_num'     => '够买数量',
    'Order.total_price' => '总价',
    'Ordercancel.name'  => '订单取消原因'
];
