<?php

return [
    'Id'              => 'ID',
    'Order_id'        => '订单',
    'Product_id'      => '商品',
    'Product_title'   => '商品名',
    'Attr_sku_id'     => '规格ID',
    'One_price'       => '商品单价',
    'Description'     => '商品描述',
    'Image'           => '商品图片',
    'Attr_sku_name'   => '商品规格',
    'Buy_num'         => '数量',
    'Product_price'   => '应付金额',
    'Discount_price'  => '优惠金额',
    'Delivery_price'  => '邮费',
    'Order_price'     => '订单金额(含运费)',
    'Order.order_num' => '订单编码',
    'Product.title'   => '商品名',
    'Attrsku.price'   => '销售价',
    'Attrsku.stock'   => '库存'
];
