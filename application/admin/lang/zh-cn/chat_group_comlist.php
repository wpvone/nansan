<?php

return [
    'Id'             => 'ID',
    'User_id'        => '用户',
    'Msg'            => '投诉内容',
    'Createtime'     => '创建时间',
    'Group_id'       => '群组',
    'User.nickname'  => '昵称',
    'Chatgroup.name' => '群名'
];
