<?php

return [
    'Id'           => 'ID',
    'Category_id'  => '分类',
    'Dateprice_id' => '时间价格ID',
    'User_id'      => '用户ID',
    'Title'        => '标题',
    'Brief'        => '介绍',
    'Images'       => '图片',
    'Mobile'       => '电话',
    'Province'     => '省',
    'City'         => '市',
    'Area'         => '区',
    'Area_detail'  => '省市区',
    'Createtime'   => '创建时间',
    'Status'       => '状态',
    'Status -1'    => '审核失败',
    'Status 0'     => '未付款',
    'Status 1'     => '待审核',
    'Status 2'     => '成功',
    'Runtime'      => '开始时间',
    'Endtime'      => '结束时间'
];
