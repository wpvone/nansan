<?php

return [
    'Id'         => 'ID',
    'Sheng'      => '省',
    'Shi'        => '市',
    'Qu'         => '区县',
    'Createtime' => '创建时间',
    'Updatetime' => '修改时间'
];
