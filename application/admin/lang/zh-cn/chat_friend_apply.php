<?php

return [
    'Id'            => 'ID',
    'Send_user_id'  => '发起申请的用户',
    'Get_user_id'   => '收到申请的用户',
    'Msg'           => '认证消息',
    'Status'        => '状态',
    'Status 1'      => '申请中',
    'Status 2'      => '通过',
    'Status 3'      => '驳回',
    'Createtime'    => '申请时间',
    'Updatetime'    => '审核时间',
    'Note'          => '备注',
    'User.nickname' => '昵称',
    'Friend.nickname' => '好友昵称'
];
