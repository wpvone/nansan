<?php

return [
    'Id'            => 'ID',
    'User_id'       => '发帖人',
    'Name'          => '帖子标题',
    'Type_id'       => '帖子类型',
    'Content'       => '帖子内容',
    'Images'        => '图片列表',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'Posttype.name' => '帖子类型',
    'User.username' => '发帖人'
];
