<?php

return [
    'Id'             => 'ID',
    'User_id'        => '用户',
    'Chat_group_id'  => '群',
    'Createtime'     => '进群时间',
    'In_group_name'  => '群内昵称',
    'Is_top'         => '置顶状态',
    'Is_top 1'       => '正常',
    'Is_top 2'       => '置顶',
    'User.nickname'  => '昵称',
    'Chatgroup.name' => '群名'
];
