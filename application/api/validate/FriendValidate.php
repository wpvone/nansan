<?php

namespace app\api\validate;

use think\Validate;

class FriendValidate extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'content' => 'require',
        'images' => 'require',
        'page' => 'require|number',
        'pageSize' => 'require|number',
        'uid' => 'require|number',
    ];

    /**
     * 提示消息
     */
    protected $message = [

    ];

    /**
     * 字段描述
     */
    protected $field = [

    ];

    /**
     * 验证场景
     */
    protected $scene = [
        'publish' => ['content','images'],
        'index' => ['page', 'pageSize'],
        'gfc' => ['uid', 'page', 'pageSize'],
    ];
}