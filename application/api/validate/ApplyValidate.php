<?php

namespace app\api\validate;

use think\Validate;

class ApplyValidate extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'apply_num|入驻号码' => 'require|length:11',
        'apply_name|入驻名称' => 'require|chsAlpha',
        'mobile|联系电话' => 'require|length:11',
        'real_name|联系人' => 'require|chs',
        'page|页数' => 'number',
        'pageSize|每页个数' => 'number',
    ];

    /**
     * 验证场景
     */
    protected $scene = [
        'index' => ['page', 'pageSize'],
        'add' => ['apply_num', 'apply_name', 'mobile', 'real_name', 'pay_password'],
    ];
}