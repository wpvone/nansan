<?php

namespace app\api\validate;

use think\Validate;

class PostValidate extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'pid|父级id' => 'require|in:0,1,2',
        'type_id|帖子类型' => 'require|number',
        'page|页数' => 'number',
        'pageSize|每页个数' => 'number',
        'name|帖子标题' => 'require',
        'content|帖子内容' => 'require',
        'images|帖子图片' => 'require',
        'id|帖子ID' => 'require|number',
        'ids' => 'require',
        'post_id|帖子ID' => 'require|number',
        'report_id|举报类型ID' => 'require|number',
    ];

    /**
     * 提示消息
     */
    protected $message = [

    ];

    /**
     * 字段描述
     */
    protected $field = [

    ];

    /**
     * 验证场景
     */
    protected $scene = [
        'type_list' => ['pid'],
        'index' => ['type_id', 'page', 'pageSize'],
        'add' => ['name', 'type_id', 'content', 'images'],
        'detail' => ['id'],
        'edit' => ['id','name', 'type_id', 'content', 'images'],
        'delete' => ['id'],
        'my_list' => ['page', 'pageSize'],
        'operate' => ['id'],
        'collect_list' => ['page', 'pageSize'],
        'del_collect' => ['ids'],
        'add_comment' => ['post_id','content'],
        'add_report' => ['post_id', 'report_id']
    ];
}