<?php

namespace app\api\controller;

use app\admin\model\ChatCircle;
use app\admin\model\ChatCircleAds;
use app\admin\model\ChatCircleComment;
use app\admin\model\ChatCircleZan;
use app\api\validate\FriendValidate;
use app\common\controller\Api;
use app\common\model\ChatCircle as circleModel;
use app\common\model\ChatFriend;
use app\common\model\Config;
use think\Request;

class FriendCircle extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 发表朋友圈
     * @param Request $request
     * @param circleModel $cModel
     */
    public function publish(Request $request, circleModel $cModel)
    {
        $resData = $request->request();
        if(!$resData['images'] && !$resData['content']){
            $this->error('发布的内容不能为空');
        }
        $user = $this->auth->getUser();
        $resData['user_id'] = $user->id;
        $cModel->allowField(true)->save($resData);
        $this->success('发布成功');
    }

    /**
     * 获取我的朋友圈
     * @param Request $request
     * @param ChatFriend $cfModel
     * @param circleModel $cModel
     * @throws \think\exception\DbException
     */
    public function index(Request $request, ChatFriend $cfModel, circleModel $cModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, FriendValidate::class . '.index');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            if(!$page) $page = 1;
            if(!$pageSize) $pageSize = 10;

            $user = $this->auth->getUser();
            $where = [
                'is_black' => 1,
                'user_id' => $user->id,
            ];
            $firend_list = $cfModel->where($where)->column('friend_id');
            $firend_list[] = $user->id;
            $data = $cModel->where('user_id', 'in', $firend_list)
                ->with('user')
                ->order('createtime desc')
                ->paginate($pageSize);
            if($data){
                $data = $data->toArray()['data'];
                foreach ($data as &$item){
                    $item['newimages'] = [];
                    $item['type'] = 1;
                    if ($item['images']){
                        foreach (explode(',', $item['images']) as $kk => $vv){
                            if(strpos($vv,'.mp4')){
                                //是视频
                                $item['newimages'][] = ['type'=>'video','url'=>$vv];
                            }else{
                                $item['newimages'][] = ['type'=>'image','url'=>$vv];
                            }
                        }
                    }


                    /** 新加入的功能 */
                    // 获取最新两条评论
                    $item['posts'] = ChatCircleComment::where('circle', $item['id'])
                        ->order('id desc')
                        ->page(1, 2)
                        ->select();
                }
            }

            $dataArr = [];
            if($data && Config::where(['name' => 'ads_run', 'group' => 'Circle'])->value('value') == 1) {
                $x = 0;$i = 0;$ad = 0;
                $adslist = ChatCircleAds::select();
                $adsnum  = $adslist->count();
                foreach ($data as $k => $vv) {
                    $i++;if($i == 3) {
                        if($ad <= $adsnum){
                            $dataArr[$k] = $adslist[$ad];
                        }
                        $ad = 0;
                        $i = 0;
                    }else{
                        $dataArr[$k] = $data[$x];
                        $x++;
                    }
                }
            }else{
                $dataArr = $data;
            }

            $list = [];
            $list['list']  = $dataArr;
            $list['background'] = $user->friend_image;
            $this->success('获取成功', $list);
        }
    }

    /**
     * 查看别人的朋友圈
     * @param Request $request
     * @param circleModel $cModel
     * @throws \think\exception\DbException
     */
    public function get_friend_circle(Request $request, circleModel $cModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, FriendValidate::class . '.gfc');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $user = \app\common\model\User::get($uid);
            $result = $cModel->where('user_id', $uid)->order('id desc')->paginate($pageSize);
            $data['user_info'] = $user;
            if($result){
                $data['list'] = $result->toArray()['data'];
                foreach ($data['list'] as &$item){
                    $item['type'] = 1;
                    $item['images'] = explode(',', $item['images']);
                    $month = date('m', strtotime($item['createtime']));
                    $day = date('d', strtotime($item['createtime']));
                    $item['createtime'] = $month . '/' . $day;

                    /** 新加入的功能 */
                    // 获取最新两条评论
                    $item['posts'] = ChatCircleComment::where('circle', $item['id'])
                        ->order('id desc')
                        ->page(1, 2)
                        ->select();
                }


                $dataArr = [];
                if($data && Config::where(['name' => 'ads_run', 'group' => 'Circle'])->value('value') == 1) {
                    $x = 0;$i = 0;$ad = 0;
                    $adslist = ChatCircleAds::select();
                    $adsnum  = $adslist->count();
                    foreach ($data as $k => $vv) {
                        $i++;if($i == 3) {
                            if($ad <= $adsnum){
                                $dataArr[$k] = $adslist[$ad];
                            }
                            $ad = 0;
                            $i = 0;
                        }else{
                            $dataArr[$k] = $data[$x];
                            $x++;
                        }
                    }
                }else{
                    $dataArr = $data;
                }
            }
            $this->success('获取成功', $dataArr);
        }
    }

    /**
     * 删除朋友圈
     */
    public function del_circle(){
        $user = $this->auth->getUser();
        $circle_id = input('circle_id') ? input('circle_id') : $this->error('请指定朋友圈');
        $circle = db('chat_circle')
            ->where('id',$circle_id)
            ->where('user_id',$user->id)
            ->find();
        if (!$circle){
            $this->error('朋友圈指定错误');
        }
        $res = db('chat_circle')->where('id',$circle_id)->delete();
        if ($res){

            /** 新加入的功能 */
            // 删除评论
            $posts = ChatCircleComment::where('circle', $circle_id)->select();
            if($posts) {
                foreach ($posts as $k => $v) {
                    $posts[$k]->delect();
                }
            }

            $this->success('删除朋友圈成功');
        }else{
            $this->error('删除朋友圈失败');
        }
    }

    /**
     * 朋友圈点赞
     */
    public function zan_circle(){
        $uid = $this->auth->id;
        $circle_id = input('cid', 0);
        empty($circle_id) and $this->error('无ID参数');

        $operate   = input('operate');
        empty($operate) and $this->error('无操作参数');

        // 获取朋友圈数据
        $circle = ChatCircle::where('id', $circle_id)->find();
        empty($circle) and $this->error('没有该数据');

        if($operate == 'zan') {
            // 查询是否已被赞
            $where['user_id']   = $uid;
            $where['to_uid']    = $circle['user_id'];
            $where['circle_id'] = $circle_id;
            $where['status']    = 1;
            $zan = ChatCircleZan::where($where)->find();
            if($zan) $this->error('此文章已赞过');

            // $where['count']      = 1;
            $where['createtime'] = time();
            $arr = ChatCircleZan::create($where);
            empty($arr) and $this->error('点赞失败');
            $this->success('点赞成功');

        } elseif ($operate == 'cancel') {
            // 查询是否已被赞
            $where['user_id']   = $uid;
            $where['to_uid']    = $circle['user_id'];
            $where['circle_id'] = $circle_id;
            $where['status']    = 1;
            $zan = ChatCircleZan::where($where)->find();
            if(!$zan) $this->error('此文章没赞过');

            $zan->status = 0;
            $zan->save();
            $this->success('点赞取消');
        }else{
            // 操作参数错误
            $this->error('操作参数错误');
        }
    }

    /**
     * 回复朋友圈
     */
    public function comment_circle()
    {
        $uid = $this->auth->id;

        // 接收的朋友圈ID参数
        $circle_id = input('cid', 0);
        empty($circle_id) and $this->error('无ID参数');
        // 接收的消息内容参数
        $content   = input('content');
        empty($message) and $this->error('无消息内容');
        // 接收被回复的回复ID
        $post_id   = input('pid', 0);

        $ins = [
            'pid'       => $post_id,
            'user_id'   => $uid,
            'circle_id' => $circle_id,
            'content'   => $content,
            'createtime'=> time()
        ];

        $arr = ChatCircleComment::create($ins);
        empty($arr) and $this->error('评论失败');
        $this->success('评论成功');
    }

    // 朋友圈评论
    public function circle_posts()
    {
        // 接收的朋友圈ID参数
        $circle_id = input('cid', 0);
        empty($circle_id) and $this->error('无ID参数');

        $page = input('page', 1);
        $size = input('size', 10);

        $arr = ChatCircleComment::where(['pid' => 0, 'circle_id' => $circle_id])
            ->order('id desc')
            ->page($page, $size)
            ->select();

        if($arr) {
            foreach ($arr as &$item) {
                $item['floor'] = ChatCircleComment::where('id', $item['pid'])
                    ->order('id desc')
                    ->page(1, 10)
                    ->select();
            }
        }

        $this->success('评论成功');
    }


    // 删除回复
    public function del_comment()
    {
        $uid = $this->auth->id;

        // 接收的回复ID参数
        $comment_id = input('pid', 0);
        empty($comment_id) and $this->error('无ID参数');

        $arr = ChatCircleComment::where(['id' => $comment_id, 'user_id' => $uid])->find();
        if(!$arr) $this->error('没有数据');

        $arr->delete();

        $this->success('删除成功');
    }
}