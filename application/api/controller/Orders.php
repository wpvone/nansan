<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\CancelOrder;
use app\common\model\Evaluate;
use app\common\model\OrderProduct;
use app\common\model\OrderRefundDetail;
use think\Db;
use think\Exception;
use app\common\model\Order;

class Orders extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    // 我的订单

    /**
     * @param Order $oModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     *
     * $status  '订单状态:1=待付款,2=退款中,3=待发货,4=待收货,5=待评价,6=订单完成,7=取消订单审核中,8=订单已取消'
     */
    public function indexList(Order $oModel)
    {
        $status = $this->request->param('status') ?? '1';
        $user = $this->auth->getUserinfo();
        $page = input('page') ? input('page') : 1;
        $pageSize = input('pageSize') ? input('pageSize') : 1;
        $where = ['status' =>  ['IN',$status], 'user_id' => $user['id']];
        $data = $oModel::orderList($where, $page, $pageSize);
        $this->success('ok', $data);
    }

    /**
     * $id
     * 订单详细
     */
    public function detail()
    {
        $id = $this->request->param('id')?? $this->error('参数错误');
        $user = $this->auth->getUserinfo();
        $where = [
            'user_id' => $user['id'],
            'id' => $id
        ];
        $oModel = new Order();
        $data = $oModel::orderDetail($where);
        if (!$data) $this->error('暂无此订单资源!');
        $this->success('ok', $data);
    }

    //取消订单原因
    public function cancelList()
    {
        $data = Db::name('order_cancel')->order('sort desc')->select();
        $this->success('ok', $data);
    }

    //取消订单
    // $id 订单ID
    public function cancelOrder()
    {
        $cancel_id = input('cancel_id') ?? $this->error('请选择取消原因!');
        $id = input('id') ?? $this->error('参数缺少!');
        $data = Db::name('order')->where([
            'id' => $id,
            'status' => '1'
        ])->find();
        if (!$data) $this->error('此订单状态已经改变!');
        $user = $this->auth->getUserinfo();
        Db::startTrans();
        try {
            Db::name('order')->where('id', $id)->update([
                'status' => '8',
                'updatetime' => time(),
                'cancel_id' => $cancel_id
            ]);

            Db::name('cancel_order')->insert([
                'user_id' => $user['id'],
                'order_id' => $id,
                'createtime' => time(),
                'type' => '1',
                'status' => '2',
                'order_cancel_id' => $cancel_id,
                'money' => 0, // 总价
                'msg' => json_encode([
                    0 => ['title' => '订单已取消', 'time' => time()],
                    1 => [ 'title' => '已提交到后台,等待后台审核!', 'time' => time()],
                ]),
                'Progress' =>  '订单已取消!'
            ]);


            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            $this->error('取消失败!');
        }
        $this->success('等待后台审核!');

    }

    // 申请退款
    public function refund()
    {
        $cancel_id = input('cancel_id') ?? $this->error('请选择原因!');
        $id = input('id') ?? $this->error('参数缺少!');
        $data = Db::name('order')->where([
            'id' => $id
        ])->find();
        $admin_id = $data['admin_id'];
        if (!$data) $this->error('此订单状态已经改变!');
        $user = $this->auth->getUserinfo();
        Db::startTrans();
        try {
            Db::name('order')->where('id', $id)->update([
                'status' => '2',
                'updatetime' => time(),
            ]);

            Db::name('cancel_order')->insert([
                'admin_id'=>$admin_id,
                'user_id' => $user['id'],
                'order_id' => $id,
                'createtime' => time(),
                'type' => '2',
                'status' => '1',
                'order_cancel_id' => $cancel_id,
                'money' => Db::name('order')->where('id', $id)->value('actual_price'), // 总价
                'msg' => json_encode([0 => [ 'title' => '已提交到后台,等待后台审核!',
                    'time' => time()]]),
                'Progress' =>  '等待后台审核!'
            ]);

            $refund_data = ['order_id' => $id, 'user_id' => $data['user_id'], 'info' => '您的退款申请已提交，请等待后台审核', 'createtime' => time(), 'is_now' => 1];
            OrderRefundDetail::where(['order_id' => $id, 'user_id' => $data['user_id'], 'is_now' => 1])->update(['is_now' => 0]);
            OrderRefundDetail::insert($refund_data);

            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        $this->success('等待后台审核!');

    }


    /**
     * 确认收货
     * $id
     */
    public function confirmOrder()
    {
        $id = input('id') ?? $this->error('缺少参数!');
        $order = db('order')->find($id);
        if ($order['status'] == 5){
            $this->error('请勿重复收货');
        }
        if ($order['status'] != 4){
            $this->error('该订单不可收货');
        }
        $ret = Db::name('order')->where('id', $id)
            ->update([
                'status' => '5',
                'is_evaluate' => '2',
                'is_received' =>'1',
                'received_time' => time()
            ]);

        $opModel = new  OrderProduct();
        $opInfo = $opModel->where('order_id', $id)->select();
        if (!$opInfo) $this->error('暂无订单信息!');

        foreach ($opInfo as $k => $v) {
            // 累计卖出
            Db::name('product')->where('id', $v['product_id'])->setInc('sell_num', $v['buy_num']);
            db('business')->where('id',db('product')->where('id',$v['product_id'])->value('business_id'))->setInc('sell_num');

        }

        $this->success('收货成功!', $ret);
    }

    /**
     * 用户评价
     *
     */
    public function evaluate()
    {
        $user = $this->auth->getUserinfo();
        $order_id = $this->request->param('order_id');
        $content = $this->request->param('content');
        $images = $this->request->param('images') ?? '';
        $product_star = $this->request->param('product_star') ?? 5;
        $express_star = $this->request->param('express_star') ?? 5;
        $business_star = $this->request->param('business_star') ?? 5;
        if (!$order_id || !$content)
        {
            $this->error('请补全信息');
        }

        if (mb_strlen($content) >60) $this->error('评论不能超过60字!');

        $eModel = new Evaluate();

        $res = $eModel->where([
            'user_id' => $user['id'],
            'order_id' => $order_id,
        ])->find();

        if ($res) {
            $this->error('请勿重复评价!');
        }

        $orderModel = new  Order();
        $opModel = new  OrderProduct();

        $orderInfo = $orderModel::get($order_id); // 订单
        $opInfo = $opModel->where('order_id', $order_id)->select();

        if (!$opInfo) $this->error('暂无订单信息!');
        $ids = ',';

//        $p_ids = [];
        foreach ($opInfo as $k => $v) {
            $ids .= $v['product_id'].',';
//            array_push($p_ids, $v['product_id']);
//            Db::name('product')->where('id', $v['product_id'])->setInc('sell_num', $v['buy_num']);
            if ($product_star == 5) { // 好评
                Db::name('product')->where('id', $v['product_id'])->setInc('favo_coms');
            } elseif ($product_star >=3) { // 中评
                Db::name('product')->where('id', $v['product_id'])->setInc('mid_coms');
            } else { // 差评
                Db::name('product')->where('id', $v['product_id'])->setInc('bad_coms');
            }
        }

        $ret = Db::name('evaluate')->insert([
            'admin_id' => $orderInfo->admin_id,
            'user_id' => $user['id'],
            'order_id' => $order_id,
            'product_id' => $ids,
            'content' => $content,
            'images' => $images,
            'product_star' => $product_star,
            'express_star' => $express_star,
            'business_star' => $business_star,
            'createtime' => time()
        ]);

        $orderInfo->is_evaluate='1';
        $orderInfo->is_received = '1';
        $orderInfo->status = '6';
        $orderInfo->save();
        $this->success('评价成功!', $ret);
    }

    /**
     * 退款信息
     * order_id 订单ID
     */
    public function cancelDetail(CancelOrder $coModel)
    {
        $user = $this->auth->getUserinfo();
        $order_id = $this->request->param('order_id');
        $ret = $coModel::detail(['order_id' => $order_id, 'user_id' => $user['id']]);
        $this->success('ok', $ret);
    }

    public function get_cancel_detail()
    {
        $order_id = $this->request->param('order_id');
        $user = $this->auth->getUser();
        $data = OrderRefundDetail::where('user_id', $user->id)->where('order_id', $order_id)->select();
        if($data){
            foreach ($data as $item){
                $item->createtime = date('Y-m-d H:i:s',$item->createtime);
            }
        }
        $this->success('获取成功', $data);
    }


}



