<?php


namespace app\api\controller;


use app\api\controller\language\First;
use app\common\model\ChatGroupuser;
use function GuzzleHttp\Psr7\str;

class Chatlist extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];

    /**
     * 获取好友列表(不含首字母)
     */
    public function get_friend_listnofirst(){
        $user = $this->auth->getUser();
        $list = model('chat_friend')
            ->with('friend')
            ->where('user_id',$user->id)
            ->where('is_black','1')
            ->order('friend_id asc')
            ->select();
        $list = collection($list)->toArray();
        foreach ($list as $key => $value){
            if ($value['friend_id'] == 1){
                unset($list[$key]);
            }
            if ($value['note']){
                $list[$key]['friend']['nickname'] = $value['note'];
            }
        }
        $this->success('ok',$list);
    }


    /**
     * 获取好友列表
     */
    public function get_friend_list(){
        $user = $this->auth->getUser();
        $list = model('chat_friend')
            ->with('friend')
            ->where('user_id',$user->id)
            ->where('is_black','1')
            ->order('friend_id asc')
            ->select();
        $list = collection($list)->toArray();
        foreach ($list as $key => $value){
            if ($value['friend_id'] == 1){
                unset($list[$key]);
            }
            if ($value['note']){
                $list[$key]['friend']['nickname'] = $value['note'];
            }
        }
        $arr = [];
        $arr['A'] = [];
        $arr['B'] = [];
        $arr['C'] = [];
        $arr['D'] = [];
        $arr['E'] = [];
        $arr['F'] = [];
        $arr['G'] = [];
        $arr['H'] = [];
        $arr['I'] = [];
        $arr['J'] = [];
        $arr['K'] = [];
        $arr['L'] = [];
        $arr['M'] = [];
        $arr['N'] = [];
        $arr['O'] = [];
        $arr['P'] = [];
        $arr['Q'] = [];
        $arr['R'] = [];
        $arr['S'] = [];
        $arr['T'] = [];
        $arr['U'] = [];
        $arr['V'] = [];
        $arr['W'] = [];
        $arr['X'] = [];
        $arr['Y'] = [];
        $arr['Z'] = [];
        $arr['#'] = [];
        $first = new First();
        foreach ($list as $key => $value){
            $str = $first->getFirstchar($value['friend']['nickname']);
            $str = strtoupper(substr($str,0,1));
            if (!strstr('ABCDEFGHIJKLMNOPQRSTUVWXYZ',$str)){

                $str = '#';
            }
            $arr[$str][] = $value;
        }
        foreach ($arr as $key => $value){
            if (!$value){
                unset($arr[$key]);
            }
        }
        $this->success('ok',$arr);
    }

    /**
     * 获取群组列表
     */
    public function get_group_list(){
        $user = $this->auth->getUser();
        $list = model('chat_groupuser')
            ->with('groupinfo')
            ->where('user_id',$user->id)
            ->order('id DESC')
            ->select();
        $this->success('ok',$list);
    }

    /**
     * 在好友或群组中查找
     */
    public function search_in_fp(){
        $search = input('search') ? input('search'):$this->error('请先输入搜索内容');
        $user = $this->auth->getUser();
        $Model = new \app\common\model\ChatFriend();
        $notefriend = $Model::where('note','like','%'.$search.'%')
            ->with('friend')->select();
        $notefriend_ids = array_column($notefriend,'friend_id');
        $nicknamefriend = $Model::hasWhere('friend',['nickname'=>['like','%'.$search.'%']])
            ->with('friend')
            ->where('user_id',$user->id)
            ->where('friend_id','not in',$notefriend_ids)
            ->select();
        $nicknamefriend = collection($nicknamefriend)->toArray();
        $notefriend = collection($notefriend)->toArray();
        $friend_list = array_merge($nicknamefriend,$notefriend);

        $GroupModel = new ChatGroupuser();
        $group_list = $GroupModel::hasWhere('groupinfo',['name'=>['like','%'.$search.'%']])
            ->with('groupinfo')
            ->where('ChatGroupuser.user_id',$user->id)
            ->select();
        $data = [];
        $data['friend_list'] = $friend_list;
        $data['group_list'] = $group_list;
        $this->success('ok',$data);
    }
}