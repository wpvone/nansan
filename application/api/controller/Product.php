<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Product as ProductModel;
use app\common\model\CategoriesAdmin as caModel;
use app\common\model\AttrSku;

/**
 * Class Product
 * @package app\api\controller
 * 商品列表展示类, 商品详细, 规格展示
 */
class Product extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 获取店铺商品分类列表
     */
    public function type_list(caModel $caModel)
    {
        $business_id = input('business_id') ? input('business_id') : $this->error('缺少关键参数“business_id”');
        $data = $caModel->where('business_id', $business_id)->field('id,title')->order('weigh desc')->select();
        $this->success('获取成功', $data);
    }
    /**
     * 商品列表
     */
    public function productList(ProductModel $pModel)
    {
        $business_id = input('business_id') ? input('business_id') : $this->error('缺少关键参数“business_id”');
        $type_id = input('type_id') ? input('type_id') : 0;
        $page = input('page') ? input('page') : 1;
        $pageSize = input('pageSize') ? input('pageSize') : 10;
        $where = ['business_id' => $business_id];
        if($type_id){
            // $where['categories_admin_id'] = $type_id;
            $where['categories_id'] = $type_id;
        }
        $data = $pModel::cateProList($where, 'createtime desc', $page, $pageSize);
        $this->success('ok', $data);
    }

    
    /**
     * 商品信息详细
     */
    public function detail(ProductModel $pModel)
    {
        $product_id = input('product_id')?? $this->error('参数未知!');
        $this->success('ok', $pModel::proDetail($product_id));
    }

    /**
     * 规格信息
     *
     * $param int $product_id 商品ID
     */
    public function skuDetail(AttrSku $paModel)
    {
        $prid = input('product_id') ?? $this->error('参数错误!');
        $data = $paModel::getInfo($prid);
        if (!$data) $this->error('暂无数据!');
        $this->success('ok', $data);
    }
}
