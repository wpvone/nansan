<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\AttrSku;
use app\common\model\Business;
use app\common\model\OrderProduct;
use app\common\model\UserCart;
use think\Db;
use think\Exception;
use app\common\model\Product;

class Order extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     * 订单确认页面信息
     *
     * @param int $type 1=立即购买, 2=购物车
     * @param $area_id 用户地址area_id
     *
     * 1
     * @param  int $product_id 商品ID
     * @param int $attr_sku_id 规格Id
     * @param int $number 购买数量
     * @param int $business_id   商户ID
     *
     * 2
     * @param  int $ids 购物车IDs
     *
     */
    public function index()
    {
        $ucModel = new UserCart();
        $user = $this->auth->getUserinfo();
        $area_id = input('area_id') ?? '';
        $ids = input('ids') ?? $this->error('请选择商品!');
        $data = $this->allPay($ids, $area_id);
        $this->success('ok', $data);
    }


    /**
     * 订单确认页面信息(多个)()
     *
     * $ids 购物车ID
     */
    protected function allPay($ids, $area_id='')
    {
        // 收货地址相关
        if (!$area_id) {
            $user = $this->auth->getUserinfo();
            $address = $this->getAddress($user['id']); // 收货地址
        } else {
            $address = Db::name('user_address')
                ->field('id, user_id, area_id, address, name, mobile, is_default, detail_address')
                ->find($area_id);
        }

        if (!$address) { // 没有收货地址
            $address = [];
        }

        $ids = trim($ids, ',');
        $cartModel = new UserCart();
        $cartData = $cartModel
//            ->with(['business'])
            ->where('id', 'IN', $ids)
            ->group('business_id')
            ->select();
        $cartArr = collection($cartData)->toArray();

        // 商户的ids
        $bids = array_column($cartArr,'business_id');

        $bsModel = new Business();

        // 取出所有信息
        $data = $bsModel->with(['userCart' => function($q)use($ids) {
            $q->with(['attrSku', 'product' => function($query){
                $query->with([ 'deliTpl' => function($qy) {
                    $qy->with([
                        'deliveryRule' => function($qu){
                            $qu->order('id desc');
                        }]);
                }]);
            }])
                ->where('id', 'IN', $ids);
        }])
            ->where('id', 'IN', $bids)
            ->select();

        $data = collection($data)->toArray();

        $arr = array();
        $total = [
            'num' => 0,
            'price' => 0
        ];
        foreach ($data as $k => $v) {
            $arr[$k]['business_id'] = $v['id'];
            $arr[$k]['name'] = $v['name'];
            $arr[$k]['image'] = $v['image'];
            $arr[$k]['count'] = count($v['user_cart']); // 件数
            $total['num'] +=  $arr[$k]['count'];

            $arr[$k]['total_price'] = 0; // 件数
            $arr[$k]['total_exp'] = 0; // 件数
            $arr[$k]['product_price'] = 0;

            foreach ($v['user_cart'] as $key => $value) {
                $arr[$k]['user_cart'][$key]['cart_id'] = $value['id'];
                $arr[$k]['user_cart'][$key]['attr_sku_id'] = $value['attr_sku_id'];
                $arr[$k]['user_cart'][$key]['buy_num'] = $value['buy_num'];
                $arr[$k]['user_cart'][$key]['product_id'] = $value['product_id'];
                $arr[$k]['user_cart'][$key]['attr_sku_keys'] = $value['attr_sku']['keys'];
                $arr[$k]['user_cart'][$key]['attr_sku_value'] = $value['attr_sku']['value'];
                $arr[$k]['user_cart'][$key]['price'] = $value['attr_sku']['price'];
                $arr[$k]['user_cart'][$key]['stock'] = $value['attr_sku']['stock'];
                $arr[$k]['user_cart'][$key]['market_price'] = $value['attr_sku']['market_price'];
                $arr[$k]['user_cart'][$key]['vip_price'] = $value['attr_sku']['vip_price'];
                $arr[$k]['user_cart'][$key]['image'] = $value['attr_sku']['image'];
//                $arr[$k]['user_cart'][$key]['categories_id'] = $value['product']['categories_id'];
                $arr[$k]['user_cart'][$key]['title'] = $value['product']['title'];
                $arr[$k]['user_cart'][$key]['head_image'] = $value['product']['head_image'];
                $arr[$k]['user_cart'][$key]['delivery_tpl_id'] = $value['product']['delivery_tpl_id'];
                $arr[$k]['user_cart'][$key]['deli_tpl'] = $value['product']['deli_tpl'];
                $arr[$k]['user_cart'][$key]['default_exp_price'] = $value['product']['default_exp_price'];

                if ($arr[$k]['user_cart'][$key]['buy_num'] >  $arr[$k]['user_cart'][$key]['stock']) {
                    $this->error($arr[$k]['user_cart'][$key]['title'] . '库存只有'.$value['attr_sku']['stock']  .'件!');
                }

                if ($arr[$k]['user_cart'][$key]['buy_num'] <  1) {
                    $this->error('购买数量不能小于1!');
                }

                if ($value['product']['deli_tpl'] && isset($address['area_id'])) { // 没有运费模板信息.默认的运费

                    $exp_price = $this->expPrice($address['area_id'], $value['product']['deli_tpl'], $arr[$k]['user_cart'][$key]['buy_num']);
                    if ($exp_price) {
                        $arr[$k]['user_cart'][$key]['default_exp_price'] = $exp_price;
                    }
                }
                unset($arr[$k]['user_cart'][$key]['deli_tpl']);
                $arr[$k]['total_exp'] += $arr[$k]['user_cart'][$key]['default_exp_price'];
                $arr[$k]['product_price'] += $arr[$k]['user_cart'][$key]['price'] * $arr[$k]['user_cart'][$key]['buy_num'];
                $arr[$k]['total_price'] += $arr[$k]['user_cart'][$key]['price'] * $arr[$k]['user_cart'][$key]['buy_num'] + $arr[$k]['user_cart'][$key]['default_exp_price'];
            }
            $total['price'] +=  $arr[$k]['total_price'];
        }

        return ['goods' => $arr, 'address' => $address, 'total' => $total];
    }

    /**
     * @param $product_id
     * @param $attr_sku_id
     * @param $number
     *
     * 确认订单信息(暂时不用)
     */
    protected function orderInfo($product_id, $attr_sku_id, $number,  $area_id='')
    {

        $number = intval($number);
        if ($number < 1) $this->error('购买数量不能小于1!');
        $pModel = new Product();
        $data = $pModel::goodsDetail($product_id, $attr_sku_id);
        if (!$data) $this->error('没有此商品信息');
        if ($data['attr_sku_one']['stock'] < $number) $this->error('库存不足,最多买'.$data['attr_sku_one']['stock'].'件!');
        if (!$area_id) {
            $user = $this->auth->getUserinfo();
            $address = $this->getAddress($user['id']); // 收货地址
        } else {
            $address = Db::name('user_address')->field('id, user_id, area_id, address, name, mobile, is_default, detail_address')
                ->find($area_id);
        }

        if (!$address) { // 没有收货地址
            $address = [];
        }

        $tpl = $data['deli_tpl']; // 运费模板
        if ($tpl && isset($address['area_id'])) { // 没有运费模板信息.默认的运费

            $exp_price = $this->expPrice($address['area_id'], $tpl, $number);
            if ($exp_price) {
                $data['default_exp_price'] = $exp_price;
            }
        }
        $arr =  ['address' => $address];
        $business = $data['business'];
        $arr['goods'][0]['business_id'] = $business['id'];
        $arr['goods'][0]['name'] = $business['name'];
        $arr['goods'][0]['image'] = $business['image'];
        $arr['goods'][0]['count'] = 1;
        $arr['goods'][0]['total_price'] = $data['default_exp_price'] + $number*$data['attr_sku_one']['price'];
        $arr['goods'][0]['total_exp'] =  $data['default_exp_price'];
        $arr['goods'][0]['user_cart'] = [];
        //立刻购买就一个
        $arr['goods'][0]['user_cart'][0]['attr_sku_id'] = $data['attr_sku_one']['id'];
        $arr['goods'][0]['user_cart'][0]['buy_num'] = $number;
        $arr['goods'][0]['user_cart'][0]['product_id'] = $data['id'];
        $arr['goods'][0]['user_cart'][0]['product_id'] = $data['id'];
        $arr['goods'][0]['user_cart'][0]['attr_sku_keys'] = $data['attr_sku_one']['keys'];
        $arr['goods'][0]['user_cart'][0]['attr_sku_value'] = $data['attr_sku_one']['value'];
        $arr['goods'][0]['user_cart'][0]['price'] = $data['attr_sku_one']['price'];
        $arr['goods'][0]['user_cart'][0]['stock'] = $data['attr_sku_one']['stock'];
        $arr['goods'][0]['user_cart'][0]['market_price'] = $data['attr_sku_one']['market_price'];
        $arr['goods'][0]['user_cart'][0]['vip_price'] = $data['attr_sku_one']['vip_price'];
        $arr['goods'][0]['user_cart'][0]['image'] = $data['attr_sku_one']['image'];
        $arr['goods'][0]['user_cart'][0]['categories_id'] = $data['categories_id'];
        $arr['goods'][0]['user_cart'][0]['title'] = $data['title'];
        $arr['goods'][0]['user_cart'][0]['head_image'] = $data['head_image'];
        $arr['goods'][0]['user_cart'][0]['delivery_tpl_id'] = $data['delivery_tpl_id'];
        $arr['goods'][0]['user_cart'][0]['default_exp_price'] = $data['default_exp_price'];

        $arr['total']['num'] = $number;
        $arr['total']['price'] = $arr['goods'][0]['total_price'];

        return $arr;

    }

    /**
     * @param  string $param 参数
     * $area_id  收货地址ID
     *
     * 创建订单
     */
    public function creates()
    {
        $param = $this->request->param('param')?? $this->error('缺少参数!');
        $area_id = $this->request->param('area_id') ??  $this->error('请选择收货地址!');
        $tempData = html_entity_decode($param);
        $arr = json_decode($tempData, true);
        $ucModel = new UserCart();
        $oModel = new \app\common\model\Order();
        $poModel = new OrderProduct();
        $pModel = new Product();
        $myArr = [];
        $user = $this->auth->getUserinfo();
        $ids = '';
        $address = Db::name('user_address')->find($area_id);
        if (!$address) $this->error('请选择收货地址');
        foreach ($arr as $k => $v) {
            // 存入order
            $myArr['order_num'] = makeOrder();
            $myArr['user_id'] = $user['id'];
            $myArr['pay_status'] = '1';
            $myArrrr['status'] = '1';
            $myArr['address_json'] = json_encode($address);
            $myArr['saler_remark'] = $v['mark'] ?? '无';
            $myArr['createtime'] = time();
            $myArr['business_id'] = $v['business_id'];
            $adminInfo = null;
            $adminInfo = Db::name('business')->where('id', $v['business_id'])->find();
            if (!$adminInfo) $this->error('暂无商户信息!');
            $cartIds = trim($v['cart_id'], ',');
            $cartInfo = $ucModel->where('id', 'IN', $cartIds)
                ->field('id, user_id, attr_sku_id, buy_num, product_id, business_id')
                ->select();
            if (!$cartInfo) $this->error('暂无购物车数据');
            $handOrder = $oModel->allowField(true)->insertGetId($myArr);
            $ids .= $handOrder. ',';
            $data = []; // 所有商品信息
            $actual_price = 0;
            $total_exp = 0;
            foreach ($cartInfo as $key => $value) {
                $ret = $pModel::goodsDetail($value['product_id'], $value['attr_sku_id']);
                $stock =  $ret['attr_sku_one']['stock']; //库存
                if ($value['buy_num'] > $stock) $this->error($ret['title'].'库存不足,最多购买'.$stock .'件!');
                $express_price = $this->expPrice($address['area_id'], $ret['deli_tpl'], $value['buy_num']); // 运费
                if ($express_price) {
                    $ret['express_price'] = $express_price;
                } else {
                    $ret['express_price'] = $ret['default_exp_price'];
                }
                $data[$key]['order_id'] = $handOrder; // 订单ID
                $data[$key]['product_id'] = $value['product_id'];
                $data[$key]['attr_sku_id'] = $value['attr_sku_id'];
                $data[$key]['attr_sku_name'] = $ret['attr_sku_one']['value'];
                $data[$key]['one_price'] = $ret['attr_sku_one']['price'];
                $data[$key]['buy_num'] = $value['buy_num'];
                $data[$key]['delivery_price'] = $ret['express_price'];
                $data[$key]['product_price'] = $value['buy_num'] * $ret['attr_sku_one']['price'];
                $data[$key]['order_price'] = $ret['express_price'] + ($value['buy_num'] * $ret['attr_sku_one']['price']);
                $data[$key]['product_title'] = $ret['title'];
                $data[$key]['image'] = $ret['head_image'];
                $data[$key]['description'] = $ret['description'];
                Db::name('attr_sku')->where('id', $value['attr_sku_id'])->setDec('stock', $value['buy_num']);
                $actual_price +=  ($ret['express_price'] + ($value['buy_num'] * $ret['attr_sku_one']['price']));
                $total_exp +=  $ret['express_price'];
            }
            $poModel->allowField(true)->saveAll($data);
            Db::name('order')->where('id', $handOrder)->update([
                'actual_price' => $actual_price,
                'delivery_price' => $total_exp
            ]);
            $ucModel::destroy($cartIds, true);
        }
        $this->success('创建成功!', ['ids' => trim($ids, ',')]);
    }

    /**
     * 付款金额
     *
     * @param str $ids 订单ids
     */
    public function totalMoney()
    {
        $ids = input('ids', '') ?? $this->error('参数错误!');
        $user = $this->auth->getUserinfo();

        $ids = trim($ids, ',');

        $ordersInfo = Db::name('order')->where([
            'user_id' => $user['id'],
            'id' => ['IN', $ids]
        ])->select();
        $totalMoney = 0;
        if ($ordersInfo) {
            foreach ($ordersInfo as $k => $v) {
                $totalMoney += $v['actual_price'];
            }
        }

        $this->success('ok', ['totalMoney' => $totalMoney]);
    }

    /**
     * 统一结算购物车(弃用)
     *
     * @param int $type 1=立刻下单 2=购物车
     * @param $area_id 发货地址
     * @param $saler_remark 备注
     *
     * 1=立刻下单
     * @param  int $product_id 商品ID
     * @param int $attr_sku_id 规格Id
     * @param int $buy_num 购买数量
     *
     *2=购物车
     * $cart_ids  购物车IDs  1,2,3,4
     *
     */
    public function settlement()
    {
        $type = input('type') ?? $this->error('参数不正确!');
        $area_id = input('area_id') ?? $this->error('请选择收货地址!');
        $saler_remark = input('saler_remark') ?? '';

        switch ($type) {  // 1=立刻下单
            case 1:
                $product_id = input('product_id') ?? $this->error('缺少商品ID参数!');
                $attr_sku_id = input('attr_sku_id') ?? $this->error('缺少规格Id参数!');
                $buy_num = input('buy_num') ?? $this->error('输入购买数量!');
                $buy_num = intval($buy_num);
                $orderId = $this->createOrder($product_id, $attr_sku_id, $area_id, $buy_num, $saler_remark);
                $this->success('创建订单成功!', $orderId);

                break;
            case 2: // 2=购物车

                $cart_ids = input('cart_ids') ?? $this->error('缺少参数!');
                $cart_ids = trim($cart_ids, ',');
                $ucModel = new UserCart();

                $cartInfo = $ucModel->where('id', 'in', $cart_ids)
                    ->field('id, user_id, attr_sku_id, buy_num, product_id')
                    ->select();
                if (!$cartInfo) $this->error('暂时数据');
                $user = $this->auth->getUserinfo();
                // 存入order
                $arr['order_num'] = makeOrder();
                $arr['user_id'] = $user['id'];
                $arr['pay_status'] = '1';
                $arr['status'] = '1';
                $address = Db::name('user_address')->find($area_id);
                if (!$address) $this->error('请选择收货地址');
                $arr['address_json'] = json_encode($address);
                $arr['saler_remark'] = $saler_remark;
                $arr['createtime'] = time();

                $oModel = new \app\common\model\Order();
                $poModel = new OrderProduct();

                Db::startTrans();
                try {
                    $handOrder = $oModel->allowField(true)->save($arr);
                    $orderId = $oModel->getLastInsID();

                    $pModel = new Product();

                    $data = []; // 所有商品信息

                    $actual_price = 0;
                    foreach ($cartInfo as $k => $value) {
                        $ret = $pModel::goodsDetail($value['product_id'], $value['attr_sku_id']);

                        $stock =  $ret['attr_sku_one']['stock']; //库存

                        if ($value['buy_num'] > $stock) $this->error($ret['title'].'库存不足,最多购买'.$stock .'件!');

                        $express_price = $this->expPrice($address['area_id'], $ret['deli_tpl'], $value['buy_num']); // 运费


                        if ($express_price) {
                            $ret['express_price'] = $express_price;
                        } else {
                            $ret['express_price'] = $ret['default_exp_price'];
                        }
                        $data[$k]['order_id'] = $orderId; // 订单ID
                        $data[$k]['product_id'] = $value['product_id'];
                        $data[$k]['attr_sku_id'] = $value['attr_sku_id'];
                        $data[$k]['attr_sku_name'] = $ret['attr_sku_one']['value'];
                        $data[$k]['one_price'] = $ret['attr_sku_one']['price'];
                        $data[$k]['buy_num'] = $value['buy_num'];
                        $data[$k]['delivery_price'] = $ret['express_price'];
                        $data[$k]['product_price'] = $value['buy_num'] * $ret['attr_sku_one']['price'];
                        $data[$k]['order_price'] = $ret['express_price'] + ($value['buy_num'] * $ret['attr_sku_one']['price']);
                        $data[$k]['product_title'] = $ret['title'];
                        $data[$k]['image'] = $ret['head_image'];
                        $data[$k]['description'] = $ret['description'];

                        Db::name('attr_sku')->where('id', $value['attr_sku_id'])->setDec('stock', $value['buy_num']);
                        $actual_price +=  ($ret['express_price'] + ($value['buy_num'] * $ret['attr_sku_one']['price']));

                    }
                    $poModel->allowField(true)->saveAll($data);

                    $oModel->actual_price = $actual_price;
                    $oModel->save();
                    $ucModel::destroy($cart_ids, true);
                    Db::commit();

                } catch (Exception $e) {
                    Db::rollback();
                    $this->error('创建订单失败!');
                }
                $this->success('创建成功!', $orderId);
                break;
        }

    }


    /**
     * 提交创建订单
     *
     * @param  int $product_id 商品IDS
     * @param int $attr_sku_id 规格IdS
     * @param int $buy_num 购买数量
     * @param $area_id 发货地址
     * @param $saler_remark 备注
     */
    public function create()
    {
        $product_id = input('product_id') ?? $this->error('缺少商品ID参数!');
        $attr_sku_id = input('attr_sku_id') ?? $this->error('缺少规格Id参数!');
        $area_id = input('area_id') ?? $this->error('请选择发货地址!');
        $buy_num = input('buy_num') ?? $this->error('输入购买数量!');
        $saler_remark = input('saler_remark');
        $buy_num = intval($buy_num);
        $this->success('创建订单成功!', $this->createOrder($product_id, $attr_sku_id, $area_id, $buy_num, $saler_remark));
    }



    /***
     * @param $area_id 用户地址area_id
     * @param $tpl 运费魔板
     * @param $number 购买数量
     * @return float|int
     *
     * 计算运费
     */
    protected function expPrice($area_id, $tpl, $number)
    {

        $first_price = 0;
        $rest_price = 0;
        $area_id = explode(',', $area_id);
        $count1 = count($area_id);
        if ($tpl['delivery_rule']) { // 规则
            foreach ($tpl['delivery_rule'] as $k => $v) {
                if ($v['area_ids'] == '0') {
                    $area_ids = ['0'];
                } else {
                    $area_ids = explode(',', $v['area_ids']);
                }
                $count2 = count($area_ids) + $count1;

                $count3 = count(array_keys(array_flip($area_id)+array_flip($area_ids))); // 合并去重

                if (in_array(0, $area_ids) || $count3 < $count2) { // 说明在里面 0 位全国
                    $first_price = $v['first_price'];
                    $rest_price = $v['rest_price'];
                    break;
                }

            }
        }

        if (!$tpl['type']) { // 计费方式:0=按件数
            $exp_price = $first_price ? $first_price : 0;
            $number--;
            if ($number > 0) {
                $exp_price = $exp_price + ($rest_price * $number);
            }

        } else { // 计费方式:1=按重量,
            $exp_price = $first_price ? $first_price : 0;
            $number--;
            if ($number > 0) {
                $exp_price = $exp_price + ($rest_price * $number);
            }
        }
        return $exp_price;

    }

    /**
     * 获取用户的地址
     *
     * @param $uid 用户ID
     */
    protected function getAddress($uid)
    {
        // 获取用户默认的收货地址
        $address = Db::name('user_address')
            ->where('user_id', $uid)
            ->order('is_default')
//            ->order('updatetime desc')
            ->field('id, user_id, area_id, address, name, mobile, is_default, detail_address')
            ->find();

        if (!$address) { // 用户没有收货地址
            return false;
        }

        // 有收货地址情况
        return $address;
    }



    /**
     * @param $product_id
     * @param $attr_sku_id
     * @param $area_id
     * @param $buy_num
     * @param string $saler_remark
     *
     * 创建订单
     */
    protected function createOrder($product_id, $attr_sku_id, $area_id, $buy_num, $saler_remark = '')
    {
        if ($buy_num < 1) $this->error('购买数量不能小于1!');

        $address = Db::name('user_address')->find($area_id);
        if (!$address) $this->error('选择发货地址!');

        $pModel = new Product();

        $data = $pModel::goodsDetail($product_id, $attr_sku_id);

        if (!$data) $this->error('此商品已经售罄!');

        $stock =  $data['attr_sku_one']['stock']; //库存

        if ($buy_num > $stock) $this->error('库存不足,最多购买'.$stock .'件!');
        $arr = [];
        $user = $this->auth->getUserinfo();
        $express_price = $this->expPrice($address['area_id'], $data['deli_tpl'], $buy_num); // 运费
        if ($express_price) {
            $order_arr['delivery_price'] = $express_price;
        } else {
            $order_arr['delivery_price'] = $data['default_exp_price'];
        }

        // 存入order
        $arr['order_num'] = makeOrder();
        $arr['user_id'] = $user['id'];
        $arr['pay_status'] = '1';
        $arr['status'] = '1';
        $arr['address_json'] = json_encode($address);
        $arr['saler_remark'] = $saler_remark;
        $arr['createtime'] = time();

        // 存入orderProduct
        $order_arr['product_id'] = $product_id;
        $order_arr['attr_sku_id'] = $attr_sku_id;
        $order_arr['attr_sku_name'] = $data['attr_sku_one']['value'];
        $order_arr['one_price'] = $data['attr_sku_one']['price'];
        $order_arr['buy_num'] = $buy_num;
        $order_arr['product_title'] = $data['title'];
        $order_arr['image'] = $data['head_image'];
        $order_arr['product_price'] = $buy_num * $order_arr['one_price'];
        $order_arr['order_price'] = $order_arr['product_price'] + $order_arr['delivery_price'];
        $order_arr['description'] = $data['description'];
        $oModel = new \app\common\model\Order();
        $poModel = new OrderProduct();
        $skuModel = new AttrSku();

        Db::startTrans();
        try {
            $handOrder = $oModel->allowField(true)->save($arr);
            $orderId = $oModel->getLastInsID();
            $order_arr['order_id'] = $orderId;
            $poHand = $poModel->allowField(true)->save($order_arr);

            // 扣除库存
            $handSku = $skuModel::get($attr_sku_id);
            $handSku->stock = ($handSku->stock) - $buy_num;
            $handSku->save();
            $oModel->actual_price = $order_arr['order_price'];
            $oModel->save();

            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            $this->error('创建订单失败!');
        }

        return $orderId;

    }


    /**
     * 订单支付
     *
     * @param $order_id 订单id
     * @parm $type 支付方式 支付类型:1=微信,2=支付宝,3=银行卡,4=余额,5=其他
     * @param $password 支付密码
     */
    public function update(\app\common\model\Order $oModel)
    {
        $id = input('order_id') ?? $this->error('缺少参数!');
        $type = input('type') ?? 1;
        $password = input('password') ?? $this->error('请输入密码!');
        $user = $this->auth->getUser();

        $orderInfo = $oModel->where([
            'id' => ['IN', $id],
            'user_id' => $user['id']
        ])->select();
        $count = $oModel->where([
            'id' => ['IN', $id],
            'user_id' => $user['id']
        ])->select();
        $cnums = count(explode(',', $id));

        if (count($count) !== $cnums) $this->error('订单参数补全!');
        $total_price = 0;
        $saveData = [];
        foreach ($orderInfo as $k => $value) {
            $total_price += $value['actual_price'];
            $saveData[$k] = [
                'id' => $value['id'],
                'status' => '3',
                'pay_time' => time(),
                'updatetime' => time(),
                'pay_type' => '4',
                'pay_status' => '2'
            ];
        }

        /**
         * 对接支付
         */
        if ($type == 4) {


            if ($total_price > $user->money) $this->error('余额不足!');
            if(!$user->pay_password) $this->error('请设置实付密码');
            if (md5(md5($password) . $user->salt) != $user->pay_password) $this->error('密码错误!');

            Db::startTrans();
            try {
                // 用户扣钱
                $user->money = $user->money - $total_price;
                $user->save();
                $res = $oModel->saveAll($saveData);
                $data = [
                    'user_id' => $user->id,
                    'money' => $total_price,
                    'type' => 1,
                    'remark' => '购买商品',
                    'order_sn' => $orderInfo[0]->order_num,
                    'createtime' => time()
                ];
                db('consp_flow')->insert($data);
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
                $this->error('支付失败!');
            }


        } else {
            $this->error('目前只支持余额支付!');
        }

        $this->success('支付成功!');
    }

}


