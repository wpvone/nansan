<?php


namespace app\api\controller;


use GatewayClient\Gateway;
use think\Db;
use think\Exception;

class Chatmoney extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];

    /**
     * 发红包
     */
    public function send_parket(){
        $num = input('num') ? input('num') : $this->error('请输入数量');
        $money = input('money') ? input('money') : $this->error('请输入金额');
//        $type = input('type') ? input('type') : $this->error('请指定红包类型');//1=普通红包,2=拼手气红包
        $type = 1;//1=普通红包,2=拼手气红包
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定好友或者群ID');
        $chat_type = input('chat_type') ? input('chat_type') : $this->error('聊天类型');//1=好友,2=群聊
        $pay_password = input('pay_password') ? input('pay_password') : $this->error('请输入支付密码');
        $note = input('note') ? input('note'):'恭喜发财,大吉大利';
        if (!is_numeric($num) || $num < 1){
            $this->error('请指定正确的数量');
        }
        if (!is_numeric($money) || $money < 0.01 * $num){
            $this->error('请输入正确的金额,每包最低0.01元');
        }
        if (!in_array($chat_type,['1','2'])){
            $this->error('聊天类型指定错误');
        }
        $user = $this->auth->getUser();
        if ($user->pay_password != md5(md5($pay_password).$user->salt)){
            $this->error('支付密码错误');
        }
        if ($chat_type == 1){
            //好友
            $is_friend = db('chat_friend')//好友列表
                ->where('user_id',$user->id)//自己
                ->where('friend_id',$friend_id)//好友
                ->find();
            if (!$is_friend){
                $this->error('好友ID指定错误');
            }
        }else{
            //群
            $is_member = db('chat_groupuser')//群用户
                ->where('user_id',$user->id)//自己
                ->where('chat_group_id',$friend_id)//群
                ->find();
            if (!$is_member){
                $this->error('群ID指定错误');
            }
        }
        if ($type == 1){
            if ($user->money < $money * $num){
                $this->error('余额不足');
            }
            if($money * $num > 200){
                $this->error('红包单次金额最多200元');
            }
            //普通红包
            $this->normal_parket($user,$num,$money,$friend_id,$chat_type,$note);//发普通红包
        }elseif ($type == 2){
            //拼手气红包
            //比较难
            if ($user->money < $money){
                $this->error('余额不足');
            }
            $this->error('先发普通红包哈');
            //
            //补
            //
        }else{
            $this->error('请指定正确的红包类型');
        }
    }

    /**
     * 普通红包
     * @param $user object 用户信息
     * @param $num int 数量
     * @param $money float 金额
     * @param  $friend_id int 好友/群id
     * @param  $chat_type int 1=好友/2=群
     * @param  $note string 备注说明
     */
    public function normal_parket($user,$num,$money,$friend_id,$chat_type,$note){
        $res = false;
        //普通红包:定义好 多少个包 ,每包多少钱 嘿嘿!
        Db::startTrans();
        try {
            $user->money -= $num * $money;
            $user->save();
            $send_parket = [];
            $send_parket['user_id'] = $user->id;//发包人
            $send_parket['friend_id'] = $friend_id;//好友/群ID
            $send_parket['packet_type'] = '1';//普通红包
            $send_parket['chat_type'] = $chat_type;//聊天类型
            $send_parket['money'] = $num * $money;//红包总金额
            $send_parket['packet_num'] = $num;//红包总个数
            $send_parket['wait_num'] = $num;//带领取个数
            $send_parket['createtime'] = time();//发包时间
            $send_parket['note'] = $note;//备注说明
            $parket_id = db('chat_packet')->insertGetId($send_parket);//红包ID
            parket_moneywater($user->id,$money * $num,1,$parket_id);//插入红包流水记录
            $recive = [];
            for ($i = 0; $i < $num; $i++){
                $recive[$i]['chat_parket_id'] = $parket_id;//红包id
                $recive[$i]['money'] = $money;//金额
                $recive[$i]['createtime'] = time();//发包时间
            }
            $res = db('chat_rparket')->insertAll($recive);//插入带领取的分配包
            Db::commit();
        }catch (Exception $exception){
            Db::rollback();
            $this->error($exception->getMessage());
        }
        if ($res){
            if ($chat_type == 1){
                //好友
//                $message_arr = [];
//                $message_arr['type'] = 'user';
//                $message_arr['categorytype'] = 'static';
//                $message_arr['from_id'] = $user->id;
//                $message_arr['to_id'] = $friend_id;
//                $message_arr['msg'] = [];
//                $message_arr['msg']['id'] = 222222222;
//                $message_arr['msg']['time'] = time();
//                $message_arr['msg']['type'] = 'redEnvelope';
//                $message_arr['msg']['userinfo'] = [];
//                $message_arr['msg']['userinfo']['uid'] = $user->id;
//                $message_arr['msg']['userinfo']['username'] = $user->nickname;
//                $message_arr['msg']['userinfo']['face'] = $user->avatar;
//                $message_arr['msg']['content'] = [];
//                $message_arr['msg']['content']['text'] = $parket_id;
//                $content = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
//
//                Gateway::sendToUid($friend_id,$content);
//                Gateway::sendToUid($user->id,$content);
            }else{
                //群
//                $message_arr = [];
//                $message_arr['type'] = 'user';
//                $message_arr['categorytype'] = 'group';
//                $message_arr['from_id'] = $user->id;
//                $message_arr['to_id'] = $friend_id;
//                $message_arr['msg'] = [];
//                $message_arr['msg']['id'] = 222222222;
//                $message_arr['msg']['time'] = time();
//                $message_arr['msg']['type'] = 'redEnvelope';
//                $message_arr['msg']['userinfo'] = [];
//                $message_arr['msg']['userinfo']['uid'] = $user->id;
//                $message_arr['msg']['userinfo']['username'] = $user->nickname;
//                $message_arr['msg']['userinfo']['face'] = $user->avatar;
//                $message_arr['msg']['content'] = [];
//                $message_arr['msg']['content']['text'] = $parket_id;
//                $content = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
//
//                Gateway::sendToGroup($friend_id,$content);
            }
            $this->success('发出红包成功',$parket_id);
        }else{
            $this->error('发出红包失败');
        }
    }

    /**
     * 领红包
     */
    public function recive_parket(){
        $user = $this->auth->getUser();
        $parket_id = input('parket_id') ? input('parket_id') : $this->error('请先指定红包ID');
        $parket = model('chat_packet')
            ->with('parketmaster')
            ->find($parket_id);//获取红包信息
        if (!$parket){
            $this->error('红包指定错误');
        }
        if ($parket['chat_type'] == 1){
            //好友
            $is_friend = db('chat_friend')
                ->where(['user_id'=>$user->id,'friend_id'=>$parket['friend_id']])
                ->whereOr(['user_id'=>$parket['friend_id'],'friend_id'=>$user->id])
                ->find();
            if (!$is_friend){
                $this->error('红包指定错误');
            }
        }else{
            $group = db('chat_group')
                ->find($parket['friend_id']);
            if (!$group){
                $this->error('群组不存在');
            }
            //群
            $is_member = db('chat_groupuser')
                ->where('user_id',$user->id)
                ->where('chat_group_id',$parket['friend_id'])
                ->find();
            if (!$is_member){
                $this->error('红包指定错误');
            }
        }
        $recive_parket = db('chat_rparket')
            ->where('chat_parket_id',$parket_id)
            ->where('user_id',$user->id)
            ->find();
        if ($parket['chat_type'] == 1 && $parket['user_id'] == $user->id){
            //自己发的红包
//            $this->get_parket_info($parket_id,true);
            $this->error('发都发了,别想要了');
        }
        if ($recive_parket ){
            //已经领过了 ,给你看红包领取详情吧
//            $this->get_parket_info($parket_id,false);
            $this->error('请勿重复领取');
        }
        $res = false;
        Db::startTrans();
        try {
            $recive_parket = db('chat_rparket')
                ->where('chat_parket_id',$parket_id)
                ->where('user_id',null)
                ->orderRaw('rand()')
                ->find();
            if ($recive_parket){
                db('chat_rparket')
                    ->where('id',$recive_parket['id'])
                    ->update(['user_id'=>$user->id,'updatetime'=>time()]);//更改红包领取状态

                $user->money += $recive_parket['money'];
                parket_moneywater($user->id,$recive_parket['money'],2,$parket_id);
                $res = db('chat_packet')
                    ->where('id',$parket_id)
                    ->dec('wait_num')
                    ->update(['updatetime'=>time()]);
            }
            Db::commit();
        }catch (Exception $exception){
            Db::rollback();
            $this->error($exception->getMessage());
        }
        if ($res){
            if ($parket['chat_type'] == 1){
                //好友
                $content = [];
                $content['type'] = 'system';
                $content['to_id'] = $parket['user_id'];//接受者 对方的id
                $content['from_id'] = $user->id;
                $content['categorytype'] = 'static';
                $content['personorgroup'] = 1;
                $content['msg'] = [];
                $content['msg']['id'] = 0;
                $content['msg']['type'] = 'redEnvelope';
                $content['msg']['content'] = [];
                $content['msg']['content']['text'] = ' 你领取了' .$parket['parketmaster']['nickname'].'的红包';
                $content['msg']['noselfinfo'] = [];
                $content['msg']['noselfinfo']['face'] = $parket['parketmaster']['avatar'];
                $content['msg']['noselfinfo']['groupname'] = $parket['parketmaster']['nickname'];
                Gateway::sendToUid($user->id,json_encode($content,JSON_UNESCAPED_UNICODE));
                $content['to_id'] = $user->id;//接受者 对方的id
                $content['msg']['content']['text'] = $user->nickname.' 领取了你的红包';
                $content['msg']['noselfinfo'] = [];
                $content['msg']['noselfinfo']['face'] = $user->avatar;
                $content['msg']['noselfinfo']['groupname'] = $user->nickname;
                Gateway::sendToUid($parket['user_id'],json_encode($content,JSON_UNESCAPED_UNICODE));
            }else{
                //群
                $content = [];
                $content['type'] = 'system';
                $content['group_id'] = $parket['friend_id'];//群id
                $content['from_id'] = $user->id;
                $content['categorytype'] = 'group';
                $content['personorgroup'] = 2;
                $content['msg'] = [];
                $content['msg']['id'] = 0;
                $content['msg']['type'] = 'redEnvelope';
                $content['msg']['content'] = [];
                $content['msg']['content']['text'] = $user->nickname.' 领取了' .$parket['parketmaster']['nickname'].'的红包';
                $content['msg']['to_group_info'] = [];
                $content['msg']['to_group_info']['face'] = $group['avatar'];
                $content['msg']['to_group_info']['groupname'] = $group['name'];
                Gateway::sendToGroup($parket['friend_id'],json_encode($content,JSON_UNESCAPED_UNICODE));
            }

//            $this->get_parket_info($parket_id,false);
            $this->success('领取成功');
        }else{
//            $this->get_parket_info($parket_id,false);
            $this->error('手快有,手慢无,慢了慢了');
        }
    }

    /**
     * 获取红包领取详情
     */
    public function get_parket_info($parket_id,$self=false){
        $user = $this->auth->getUser();
        $my_recive = db('chat_rparket')
            ->where('chat_parket_id',$parket_id)
            ->where('user_id',$user->id)
            ->find();
        $data = [];
        $parket = model('chat_packet')
            ->with('parketmaster')
            ->find($parket_id);//红包信息
        if ($my_recive){
            //已经领过啦
            $data['my']['money'] = $my_recive['money'];
        }else{
            //没抢到
            if ($parket['user_id'] == $user->id){
                $self = true;
            }
            if ($parket['chat_type'] == 1){
                if ($self){
                    $data['my']['money'] = $parket['money'];
                }else{
                    $data['my']['money'] = $parket['money'];
                }
            }else{
                if ($self){
                    $data['my']['money'] = '被抢光了';
                }else{
                    $data['my']['money'] = '被抢光了';
                }
            }

        }

        $send_is_friend = db('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$parket['parketmaster']['id'])
            ->find();
        if ($send_is_friend){
            //说明发包人是好友
            $parket['parketmaster']['nickname'] = $send_is_friend['note'] ? $send_is_friend['note'] : $parket['parketmaster']['nickname'];
        }
        $data['parketmaster'] = $parket['parketmaster'];//发包人的信息
        $data['note'] = $parket['note'];
        $list = model('chat_rparket')
            ->with('reciveuser')
            ->where('chat_parket_id',$parket_id)
            ->order('updatetime DESC')
            ->select();
        $list = collection($list)->toArray();
        $data['num_count'] = count($list);
        $data['money_count'] = 0;//红包总金额
        $data['yi_money'] = 0;//已领的金额
        $data['yi_count'] = 0;//已领的数量
        $over = true;
        $data['recive_list'] = [];//领取记录
        foreach ($list as $key => $value){
            $data['money_count'] += $value['money'];
            if (!isset($value['reciveuser']) || !$value['reciveuser']){
                //没领完
                $over = false;
            }else{
                $ling_is_friend = db('chat_friend')
                    ->where('user_id',$user->id)
                    ->where('friend_id',$value['reciveuser']['id'])
                    ->find();
                if ($ling_is_friend){
                    //领红包人 是好友
                    $value['reciveuser']['nickname'] = $ling_is_friend['note'] ? $ling_is_friend['note'] : $value['reciveuser']['nickname'];
                }
                $data['recive_list'][] = [
                    'userinfo'=>$value['reciveuser'],
                    'money'=>$value['money'],
                    'updatetime'=>date('i:s',$value['updatetime'])
                ];
                $data['yi_money'] += $value['money'];
                $data['yi_count']++;
            }
        }
        if ($over){
            //领完了
            $timecha = $list[0]['updatetime'] - $list[$data['num_count']-1]['updatetime'];//领取时间差
            $min = intval($timecha / 60);//分钟
            $sec = $timecha - ($min * 60);//秒数
            $data['msg'] = $data['num_count'].'个红包共'.$data['money_count'].'元, '.$min.'分'.$sec.'秒抢完了';
        }else{
            //没领完
            $data['msg'] = $data['num_count'].'个红包共'.$data['money_count'].'元';
        }
        $this->success('ok',$data);
    }

    /**
     * 转账
     */
    public function transfer_money(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请先指定好友');
        $money = input('money') ? input('money') : $this->error('请输入转账金额');
        $note = input('note') ? input('note') :'';
        $pay_pwd = input('pay_pwd') ? input('pay_pwd'):$this->error('请输入支付密码');
        if ($money > $user->money){
            $this->error('账户余额不足');
        }
        if($money > 20000){
            $this->error('转账单次最多20000元');
        }
        if ($user->pay_password != md5(md5($pay_pwd).$user->salt)){
            $this->error('请输入支付密码');
        }
        $is_friend = db('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();
        if (!$is_friend){
            $this->error('好友指定错误');
        }
        $res = false;
        Db::startTrans();
        try {
            $user->money -= $money;
            $user->save();
            $tranfer = [];
            $tranfer['user_id'] = $user->id;
            $tranfer['friend_id'] = $friend_id;
            $tranfer['money'] = $money;
            $tranfer['note'] = $note;
            $tranfer['createtime'] = time();
            $tranfer['status'] = '1';
            $tranfer['overtime'] = time() + 86400;//一天
            $res = db('chat_transfer_money')->insertGetId($tranfer);
            Db::commit();
        }catch (Exception $exception){
            Db::rollback();
            $this->error($exception->getMessage());
        }
        if ($res){
//            $message_arr = [];
//            $message_arr['type'] = 'user';
//            $message_arr['categorytype'] = 'static';
//            $message_arr['from_id'] = $user->id;
//            $message_arr['to_id'] = $friend_id;
//            $message_arr['msg'] = [];
//            $message_arr['msg']['id'] = 222222222;
//            $message_arr['msg']['time'] = time();
//            $message_arr['msg']['type'] = 'zhuanEnvelope';
//            $message_arr['msg']['userinfo'] = [];
//            $message_arr['msg']['userinfo']['uid'] = $user->id;
//            $message_arr['msg']['userinfo']['username'] = $user->nickname;
//            $message_arr['msg']['userinfo']['face'] = $user->avatar;
//            $message_arr['msg']['content'] = [];
//            $message_arr['msg']['content']['text'] = $res;
//            $content = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
//            Gateway::sendToUid($friend_id,$content);
//            Gateway::sendToUid($user->id,$content);
            $this->success('转账成功',$res);
        }else{
            $this->error('转账失败');
        }
    }

    /**
     * 获取转账信息
     */
    public function get_transfer_money(){
        $user = $this->auth->getUser();
        $tranfer_id = input('tranfer_id') ? input('tranfer_id') : $this->error('请指定转账记录');
        $tranfer = db('chat_transfer_money')->find($tranfer_id);
        if (!$tranfer){
            $this->error('转账记录指定错误');
        }
        $data = [];
        if ($tranfer['status'] == 1){
            //待领取
            if ($tranfer['user_id'] == $user->id){
                //自己发起的转账
                $data['operate'] = false;
                $data['status'] = $tranfer['status'];
                $data['money'] = $tranfer['money'];
                $friend = model('chat_friend')
                    ->with('friend')
                    ->where('user_id',$user->id)
                    ->where('friend_id',$tranfer['friend_id'])
                    ->find();
                if (!$friend){
                    $this->error('好友解除,转账暂不可领取');
                }
                $data['title'] = '等待'.$friend['note'] ? $friend['note']:$friend['friend']['nickname'].'确认收款';
                $data['msg'] = '一天内好友未确认,将退还给你';
                $data['createtime'] = date('Y-m-d H:i:s',$tranfer['createtime']);
                $data['updatetime'] = '';

            }elseif ($tranfer['friend_id'] == $user->id){
                //好友发起的转账
                $data['operate'] = true;
                $data['status'] = $tranfer['status'];
                $data['money'] = $tranfer['money'];
                $data['title'] = '待确认收款';
                $data['msg'] = '一天内未确认,将退还给对方';
                $data['createtime'] = date('Y-m-d H:i:s',$tranfer['createtime']);
                $data['updatetime'] = '';

            }else{
                $this->error('转账记录指定错误');
            }
        }elseif ($tranfer['status'] == 2){
            //已收款
            $data['title'] = '已收款';
            $data['money'] = $tranfer['money'];
            $data['status'] = $tranfer['status'];
            $data['createtime'] = date('Y-m-d H:i:s',$tranfer['createtime']);
            $data['updatetime'] = date('Y-m-d H:i:s',$tranfer['updatetime']);
        }else{
            //已退回
            $data['title'] = '已退回';
            $data['money'] = $tranfer['money'];
            $data['status'] = $tranfer['status'];
            $data['createtime'] = date('Y-m-d H:i:s',$tranfer['createtime']);
            $data['updatetime'] = date('Y-m-d H:i:s',$tranfer['updatetime']);
        }
        $this->success('ok',$data);
    }

    /**
     * 转账收款
     */
    public function recive_tranfer(){
        $user = $this->auth->getUser();
        $tranfer_id = input('tranfer_id') ? input('tranfer_id') : $this->error('请指定转账记录');
        $tranfer = db('chat_transfer_money')->find($tranfer_id);
        if (!$tranfer){
            $this->error('转账记录指定错误');
        }
        if ($tranfer['status'] != 1){
            $this->error('该转账不可收款');
        }
        //待领取
        if ($tranfer['user_id'] == $user->id){
            //自己发起的转账
            $this->error('请等待对方收款');
        }elseif ($tranfer['friend_id'] == $user->id){
            //好友发起的转账
            Db::startTrans();
            try {
                $user->money += $tranfer['money'];
                $user->save();
                db('chat_transfer_money')
                    ->where('id',$tranfer['id'])
                    ->update([
                       'status'=>'2',
                       'updatetime'=>time()
                    ]);
                Db::commit();
            }catch (Exception $exception){
                Db::rollback();
                $this->error($exception->getMessage());
            }
        }else{
            $this->error('转账记录指定错误');
        }
        $this->success('收款成功');
    }
}