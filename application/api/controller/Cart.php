<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\AttrSku;
use app\common\model\UserCart;
use think\Db;

/**
 * Class Cart
 * @package app\api\controller
 *
 * 购物车模块
 */
class Cart extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     * 显示购物车列表
     *
     * @return \think\Response
     * $page
     * $limit
     */
    public function index(UserCart $ucModel)
    {
        $user = $this->auth->getUserinfo();
        $business_id = input('business_id') ? input('business_id') : $this->error('缺少关键参数“business_id”');

        $data = $ucModel->where(['business_id' => $business_id, 'user_id' => $user['id']])
            ->with([
                'product' => function($q) {
                    $q->field('id, categories_id, title, description, head_image, sell_num, low_price');
                },
                'attrSku',
            ])
            ->select();
        $count = $ucModel->where(['business_id' => $business_id, 'user_id' => $user['id']])
            ->with([
                'product' => function($q) {
                    $q->field('id, categories_id, title, description, head_image, sell_num, low_price');
                },
                'attrSku',
            ])
            ->count();
        $all_price = 0;
        if($data){
            foreach ($data as $item){
                $all_price += $item->attr_sku->price * $item->buy_num;
            }
        }
        $this->success('ok', ['list' => $data, 'count' => $count, 'all_price' => $all_price]);
    }


    /**
     * 修改购物车信息
     *
     * @param int $id ID
     * @param  int $buy_num 购买数量
     * @param  int  $attr_sku_id 规格 ID
     */
    public function update(UserCart $ucModel)
    {
        $param = $this->request->except('id');
        $id = input('id') ?? $this->error('缺少ID');
        $ucData = $ucModel->find($id);
        $skuData = AttrSku::get($ucData->attr_sku_id);
        if($param['buy_num'] > $skuData->stock) $this->error('库存不足');
        $ret = $ucModel->allowField(true)->save($param, ['id' => $id]);
        $this->success('success', $ret);

    }


    /**
     * 购买数量加减
     *
     * @param int $id ID
     * @param string $type 1=加一 2=减1
     */
    public function buyNum(UserCart $ucModel)
    {
        $id = input('id') ?? $this->error('缺少ID');
        $type = input('type') ?? $this->error('类型参数缺少');
        $hand = $ucModel->with(['attrSku'])
            ->find($id);
        if (!$hand) $this->error('没有此条信息!');
        if ($type == 1) { // ++
            if ($hand->buy_num >= $hand->attr_sku->stock) $this->error('库存不足!');
            $hand->buy_num++;
        } elseif ($type == 2) { // --
            if ($hand->buy_num == 1) $this->error('不能低于1');
            $hand->buy_num--;
        }
        $hand->save();
        $this->success('操作成功');
    }


    /**
     * 创建购物车
     *
     * @param int $id ID
     * @param  int $product_id 商品ID
     * @param  int  $attr_sku_id 规格 ID
     * @param int $business_id 商户id
     * @param int $buy_num
     */
    public function create(UserCart $ucModel)
    {
        $product_id = input('product_id') ?? $this->error('缺少商品ID');
        $sku_id = input('attr_sku_id') ?? $this->error('缺少规格参数!');
        $buy_num = input('buy_num') ?? $this->error('购买数量不能空');
        $business_id = input('business_id') ?? $this->error('缺少商户id');
        $user = $this->auth->getUserinfo();

        $data = $ucModel::where([
            'product_id' => $product_id,
            'attr_sku_id' => $sku_id,
            'user_id' => $user['id']
        ])->with('attrSku')->find();
        if ($data) {
            $data->buy_num = ($data->buy_num) + $buy_num;
            if($data->buy_num > $data->attr_sku->stock) $this->error('库存不足');
            $data->save();
        } else {
            $sku_data = AttrSku::get($sku_id);
            if($buy_num > $sku_data->stock) $this->error('库存不足');
            $ret = $ucModel->allowField(true)->save([
                'product_id' => $product_id,
                'user_id' => $user['id'],
                'attr_sku_id' => $sku_id,
                'buy_num' => $buy_num,
                'createtime' => time(),
                'business_id' => $business_id
            ]);
        }
        $this->success('加入购物车成功!');
    }


    /**
     * 删除购物车指定资源
     *
     * @param  int  $id
     *
     */
    public function delete(UserCart $ucModel)
    {
        $id = input('id') ?? $this->error('请选择商品!');
//        $id = trim($id, ',');
        $ret = $ucModel::where('business_id', $id)->delete();
        $this->success('操作成功!', $ret);
    }

    /**
     * 判断购物车的商品库存是否足够
     *
     * $cartIds
     *
     */
    public function stockInfo()
    {
        $cartIds = input('cart_ids') ?? $this->error('参数错误!');
        $card = new UserCart();
        $data = $card->with('attrSku')
            ->where('id', 'IN', $cartIds)
            ->select();
        foreach ($data as $k => $v) {
            if (empty($v['attr_sku'])) $this->error('库存不足');
            if ($v['attr_sku']['stock'] < $v['buy_num']) $this->error($v['attr_sku']['value'].' 库存仅剩'.$v['attr_sku']['stock'].'件!');
        }
        $this->success('提交成功!');
    }

}
