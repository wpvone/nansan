<?php

namespace app\api\controller;

use app\admin\model\EasypeopleDateprice;
use app\api\controller\language\First;
use app\common\controller\Api;
use app\common\model\Category;

/**
 * 便民接口
 */
class Easypeople extends Api
{

    protected $noNeedLogin = ['index', 'test1'];
    protected $noNeedRight = ['*'];

    /**
     * 便民电话列表
     */
    public function index()
    {
        $page = input('page', 1);
        $size = input('size', 10);
        $cid  = input('cid', 0);
        $area = input('area', '');
        $where['category_id'] = $cid;
        if($area) {
            $deArea = explode('/', $area);
            if(conut($deArea) == 2) {
                $where['city'] = $deArea[0];
                $where['area'] = $deArea[1];
            }else{
                $where['province'] = $deArea[0];
                $where['city'] = $deArea[1];
                $where['area'] = $deArea[2];
            }
        }
        if(empty($cid)) {
            $arr = \app\admin\model\Easypeople::order('id desc')->page($page, $size)->select();
        } else {
            $arr = \app\admin\model\Easypeople::where($where)
                ->order('id desc')
                ->page($page, $size)
                ->select();
        }
        empty($arr) and $this->error('数据为空');
        $this->success('获取成功', $arr);
    }

    /**
     * 便民电话分类
     */
    public function category()
    {
        $arr = Category::where(['type' => 'easypeople', 'status' => 'normal'])
            ->order('weigh desc')
            ->page(1, 10)
            ->select();
        empty($arr) and $this->error('无分类信息');
        $this->success('获取成功', $arr);
    }

    /**
     * 便民电话详情
     */
    public function detail()
    {
        $eid = input('id');
        empty($eid) and $this->error('无ID参数');

        $arr = \app\admin\model\Easypeople::where('id', $eid)->find();
        empty($arr) and $this->error('无该详情');

        //检测CDN是否开启 并赋值
        $baseURL = '//' . $_SERVER['HTTP_HOST'];

        //正则替换文章内图片路径
        $pregstr = '<img style="max-width:100%" src="' . $baseURL;
        $arr['brief'] = preg_replace('/(<img.+?src=")(.*?)/',$pregstr, $arr['brief']);

        $this->success('获取成功', $arr);
    }

    /**
     * 入驻时间价格
     */
    public function dateprice()
    {
        $arr = EasypeopleDateprice::select();
        $this->success('获取成功', $arr);
    }

    /**
     * 入驻申请
     */
    public function apply()
    {
        if($this->request->isPost()) {

            $cid = input('category_id', 0);
            empty($cid) and $this->error('请传入分类ID');
            $dateprice_id = input('dateprice_id', 0);
            empty($dateprice_id) and $this->error('请传入');
            $title = input('title', 0);
            empty($cid) and $this->error('请传入名称');
            $brief = input('brief', 0);
            empty($cid) and $this->error('请传入描述');
            $images = input('images', 0);

            $mobile = input('mobile', 0);
            empty($mobile) and $this->error('请传入手机号');
            $area = input('area', 0);
            empty($area) and $this->error('请传入地址');
            if($area) {
                $areaArr = explode('/', $area);
                if(count($areaArr) == 2) {
                    $city  = $areaArr[0];
                    $areas = $areaArr[0];
                }else{
                    $province = $areaArr[0];
                    $city  = $areaArr[0];
                    $areas = $areaArr[0];
                }
            }

            $ins = [
                'category_id'  => $cid,
                'dateprice_id' => $dateprice_id,
                'user_id'      => $this->auth->id,
                'title'        => $title,
                'brief'        => $brief,
                'images'       => $images,
                'mobile'       => $mobile,
                'province'     => $province,
                'city'         => $city,
                'area'         => $areas,
                'area_detail'  => $area,
                'createtime'   => time()
            ];



            // 这里走支付逻辑

            $arr = \app\admin\model\Easypeople::create($ins);
            empty($arr) and $this->error('提交失败');
            $this->success('提交成功', $arr);
        }
    }
}