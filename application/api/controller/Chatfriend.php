<?php


namespace app\api\controller;

use GatewayClient\Gateway;
use think\Db;
use think\Exception;

/**
 * Class 聊天-好友相关
 * @package app\api\controller
 */
class Chatfriend extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];
    public function delApplyLog($apply_id = null){
        $user = $this->auth->getUser();
        $apply = db('chat_friend_apply')
            ->where('id',$apply_id)
            ->where('send_user_id|get_user_id',$user->id)//自己的ID
            ->find();
        if(!$apply){
            $this->error('记录已删除');
        }
        if($apply['status'] == 1 && $apply['send_user_id'] == $user->id){
            //自己发起的申请,对方未审核
            db('chat_friend_apply')->where('id',$apply_id)->delete();
        }else{
            $delete = explode(',',trim($apply['no_look'],','));
            $delete[] = $user->id;
            if(in_array($apply['send_user_id'],$delete) && in_array($apply['get_user_id'],$delete)){
                //两个人都删了
                db('chat_friend_apply')->where('id',$apply_id)->delete();
            }else{
                $deletestr = ','.implode(',',$delete).',';
                db('chat_friend_apply')->where('id',$apply_id)->update([
                    'no_look'=>$deletestr
                ]);
            }
        }
        $this->success('删除成功');
    }
    /**
     * 申请-添加好友
     */
    public function add_friend(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id'):$this->error('请指定欲添加的好友');
        if ($user->id == $friend_id){
            $this->error('不可添加自己为好友');
        }
        $friend = db('user')->where('id',$friend_id)->find();
        if (!$friend){
            $this->error('好友不存在');
        }
        $msg = input('msg') ? input('msg') : '';//申请原因
        $note = input('note') ? input('note') :'';//备注
        $is_friend = db('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();
        if ($is_friend){
            $this->error('已经是好友,请勿重复添加');
        }
        $is_apply = db('chat_friend_apply')
            ->where('send_user_id',$user->id)//自己的ID
            ->where('get_user_id',$friend_id)//好友ID
            ->where('status','1')//申请中
            ->find();
        if($is_apply){
            $this->error('请勿重复申请,请等待对方审核');
        }
        $is_applys = db('chat_friend_apply')
            ->where('send_user_id',$friend_id)//好友的id
            ->where('get_user_id',$user->id)//自己的id
            ->where('status','1')//申请中
            ->find();
        if($is_applys){
            $this->error('对方已申请成为你的好友,请审核');
        }
        $data = [];
        $data['send_user_id'] = $user->id;
        $data['get_user_id'] = $friend_id;
        $data['msg'] = $msg;
        $data['createtime'] = time();
        $data['note'] = $note;
        $res = db('chat_friend_apply')->insert($data);
        if ($res){
            Gateway::sendToUid($friend_id,json_encode(['type'=>'friendApply','content'=>'有人欲添加你为好友'],JSON_UNESCAPED_UNICODE));
            $this->success('发起好友申请成功');
        }else{
            $this->error('发起好友申请失败');
        }
    }

    /**
     * 获取好友申请列表
     */
    public function get_apply_list(){
        $user = $this->auth->getUser();
        $page = input('page') ? input('page'):1;
        $limit = input('limit') ? input('limit'):10;
        $list = model('chat_friend_apply')
            ->with('send_user_infos')//获取发起申请的用户信息
            ->with('get_user_infos')//获取接收者的用户信息
            ->where('send_user_id|get_user_id',$user->id)
            ->where('no_look','NOT LIKE','%,'.$user->id.',%')
            ->order('status ASC')
            ->page($page,$limit)
            ->select();
        $data = [];
        $list = collection($list)->toArray();
        foreach ($list  as $key => $value){
            $data[$key]['id'] = $value['id'];
            if ($value['send_user_id'] == $user->id){
                //自己是申请人
                $userinfo =$value['get_user_infos'];//获取展示用的用户信息
                $data[$key]['nickname'] = $value['note'] ? $value['note'] : $userinfo['nickname'];//自己发起的申请,设置了备注就显示备注,没有就显示昵称
                if ($value['status'] == 1){
                    $data[$key]['status_text'] = '等待验证';
                }elseif ($value['status'] == 2){
                    $data[$key]['status_text'] = '通过审核';
                }elseif ($value['status'] == 3){
                    $data[$key]['status_text'] = '申请拒绝';
                }
                $data[$key]['operate'] = false;//自己不需要操作
            }else{
                //对方是申请人
                $userinfo =$value['send_user_infos'];//获取展示用的用户信息
                $data[$key]['operate'] = true;//需要自己进行审核
                $data[$key]['nickname'] = $userinfo['nickname'];//昵称
                if ($value['status'] == 1){
                    $data[$key]['operate'] = true;//需要自己进行审核
                    $data[$key]['status_text'] = '2个验证按钮';
                }elseif ($value['status'] == 2){
                    $data[$key]['operate'] = false;//需要自己进行审核
                    $data[$key]['status_text'] = '通过审核';
                }elseif ($value['status'] == 3){
                    $data[$key]['operate'] = false;//需要自己进行审核
                    $data[$key]['status_text'] = '申请拒绝';
                }
            }
            $data[$key]['status'] = $value['status'];//状态
            $data[$key]['avatar'] = $userinfo['avatar'];//头像
            $data[$key]['gender'] = $userinfo['gender'];//性别
            $data[$key]['user_id'] = $userinfo['id'];
            $data[$key]['createtime'] = date('Y-m-d H:i:s',$value['createtime']);//发起申请的时间
            $data[$key]['msg'] = $value['msg'];//申请原因
        }
        $this->success('ok',$data);
    }

    /**
     * 通过好友申请
     */
    public function pass_friend_apply(){
        $user = $this->auth->getUser();
        $apply_id = input('apply_id') ? input('apply_id') : $this->error('请先指定申请记录');
        $apply = db('chat_friend_apply')
            ->where('id',$apply_id)
            ->where('get_user_id',$user->id)
            ->find();
        if (!$apply){
            $this->error('申请记录指定错误');
        }
        if ($apply['status'] != 1){
            $this->error('该申请记录已处理');
        }
        $is_friend = db('chat_friend')
            ->where('user_id',$apply['send_user_id'])
            ->where('friend_id',$apply['get_user_id'])
            ->find();
        if ($is_friend){
            $this->error('已经是好友,请勿重复添加');
        }
        $res = false;
        //通过申请就是加好友啦
        Db::startTrans();
        try {
            db('chat_friend_apply')
                ->where('id',$apply_id)
                ->update(['status'=>'2','updatetime'=>time()]);//变更申请记录状态
            $send_friend = []; //发起申请人的好友记录
            $send_friend['user_id'] = $apply['send_user_id'];
            $send_friend['friend_id'] = $apply['get_user_id'];
            $send_friend['note'] = $apply['note'];
            $send_friend['createtime'] = time();
            $get_friend = []; //收到者的好友记录
            $get_friend['user_id'] = $apply['get_user_id'];
            $get_friend['friend_id'] = $apply['send_user_id'];
            $get_friend['note'] = '';
            $get_friend['createtime'] = time();
            $friends = [$send_friend,$get_friend];
            $res = db('chat_friend')->insertAll($friends);//插入好友记录
            Db::commit();
        }catch (Exception $exception){
            Db::rollback();
            $this->error($exception->getMessage());
        }
        if ($res){
            $message = [];
            $message['type'] = 'system';
            $message['msg'] = [];
            $message['msg']['id'] = 0;
            $message['msg']['type'] = 'text';
            $message['msg']['content'] = [];
            $message['msg']['content']['text'] = '我们已经是好友啦,开始聊天吧';
            $message['from_id'] = $apply['send_user_id'];
            $message['to_id'] = $apply['get_user_id'];
            $message['categorytype'] = 'user';
            $message['personorgroup'] = 1;
            $message['msg']['noselfinfo'] = [];
            $send_user = db('user')->find($apply['send_user_id']);
            $message['msg']['noselfinfo']['face'] = $send_user['avatar'];
            $message['msg']['noselfinfo']['username'] = $send_user['nickname'];
            Gateway::sendToUid($apply['get_user_id'],json_encode($message,JSON_UNESCAPED_UNICODE));
            $message['to_id'] = $apply['send_user_id'];
            $message['from_id'] = $apply['get_user_id'];
            $message['categorytype'] = 'user';
            $message['personorgroup'] = 1;
            $get_user = db('user')->find($apply['get_user_id']);
            $message['msg']['noselfinfo']['face'] = $get_user['avatar'];
            $message['msg']['noselfinfo']['username'] = $apply['note']? $apply['note'] : $get_user['nickname'];
            Gateway::sendToUid($apply['send_user_id'],json_encode($message,JSON_UNESCAPED_UNICODE));
            $this->success('通过审核成功');
        }else{
            $this->error('通过审核失败');
        }
    }

    /**
     * 拒绝好友申请
     */
    public function refuse_apply(){
        $user = $this->auth->getUser();
        $apply_id = input('apply_id') ? input('apply_id') : $this->error('请先指定申请记录');
        $apply = db('chat_friend_apply')
            ->where('id',$apply_id)
            ->where('get_user_id',$user->id)
            ->find();
        if (!$apply){
            $this->error('申请记录指定错误');
        }
        if ($apply['status'] != 1){
            $this->error('该申请记录已处理');
        }
        $res = db('chat_friend_apply')
            ->where('id',$apply_id)
            ->update(['status'=>'3','updatetime'=>time()]);
        if ($res){
            $this->success('拒绝好友申请成功');
        }else{
            $this->error('拒绝好友申请失败');
        }
    }

    /**
     * 搜索好友
     */
    public function search_friend(){
        $search = input('search') ? input('search') : $this->error('请输入搜索内容');
        $list = db('user')
            ->field('id,nickname,avatar,idnum,gender')
            ->where('username|nickname|mobile|idnum',$search)
            ->select();
        $this->success('ok',$list);
    }

    /**
     * 获取搜索好友的详情展示
     */
    public function get_search_detail(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请先指定好友');
        $friend = db('user')
            ->field('id,nickname,avatar,gender,idnum')
            ->find($friend_id);
        if (!$friend){
            $this->error('用户不存在');
        }
        $circlelist = db('chat_circle')
            ->where('user_id',$friend_id)
            ->where('images','not null')
            ->order('id DESC')
            ->limit(3)
            ->select();
        $is_friend = db('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();
        $circle_images = [];
        foreach ($circlelist as $value){
            $circle_images = array_merge($circle_images,explode(',',trim($value['images'],',')));
        }
        $data = [];
        $data['id'] = $friend_id;
        $data['nickname'] = $friend['nickname'];
        $data['avatar'] = $friend['avatar'];
        $data['gender'] = $friend['gender'];
        $data['idnum'] = $friend['idnum'];
        $data['images'] = $circle_images;
        if ($is_friend){
            $data['is_friend'] = false;
        }else{
            $data['is_friend'] = true;
        }
        $this->success('ok',$data);
    }

    /**
     * 获取系统消息及好友申请 小红点数字
     */
    public function sys_msg_num(){
        $user = $this->auth->getUser();
        $wait_operate_num = db('chat_friend_apply')
            ->where('get_user_id',$user->id)//别人向自己申请的
            ->where('status','1')
            ->count();
        $wait_sys_msg_num = db('chat_msg_noread')
            ->where('noread_user_ids','like','%,'.$user->id.',%')//未读用户中有自己
            ->count();
        $data = [];
        $data['msg_num'] = $wait_sys_msg_num;//系统未读消息展示数量
        $data['apply_num'] = $wait_operate_num;//好友申请展示数量
        $this->success('ok',$data);
    }

    /**
     * 获取系统消息列表
     */
    public function get_sys_msg_list(){
        $user = $this->auth->getUser();
        $page = input('page') ? input('page') : 1;
        $limit = input('limit') ? input('limit') :10;
        $user_id = $user->id;
        $list = model('chat_sys_msg')
            ->with(['noread'=>function($query) use ($user_id){
                $query->where('noread_user_ids', 'like', '%,'.$user_id.',%')->field('id');
            }])
            ->field('id,createtime,name')
            ->order('id DESC')
            ->page($page,$limit)
            ->select();
        $list = collection($list)->toArray();
        foreach ($list as $key => $value){
            $list[$key]['createtime'] = date('Y-m-d H:i:s',$value['createtime']);
            if (isset($value['noread']['id'])){
                $list[$key]['status'] = 1;//未读
            }else{
                $list[$key]['status'] = 2;//已读
            }
        }
        $this->success('ok',$list);
    }

    /**
     * 获取系统消息详情
     */
    public function get_sys_msg_detail(){
        $msg_id = input('msg_id') ? input('msg_id') : $this->error('请先指定消息');
        $msg = db('chat_sys_msg')
            ->find($msg_id);
        if (!$msg){
            $this->error('消息指定错误');
        }
        $user = $this->auth->getUser();
        $isnoread = db('chat_msg_noread')->where('msg_id',$msg_id)
            ->find();
        $noread_users = explode(',',trim($isnoread['noread_user_ids'],','));
        if (in_array($user->id,$noread_users)){
            $key = array_search($user->id,$noread_users);
            unset($noread_users[$key]);
            $noread_users_str = ','.implode(',',$noread_users).',';
            db('chat_msg_noread')->where('id',$isnoread['id'])
                ->update(['noread_user_ids'=>$noread_users_str,'updatetime'=>time()]);
        }
        $msg['createtime'] = date('Y-m-d H:i:s',$msg['createtime']);
        $this->success('ok',$msg);
    }

    /**
     * 获取好友 -聊天信息页面信息--10-20-59
     */
    public function get_friend_info(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定好友');
        $friend = model('chat_friend')
            ->with('friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();//好友信息
        if(!$friend){
            $this->error('不是好友');
        }
        $data = [];
        $data['id'] = $friend_id;
        if ($friend['note']){
            $data['note'] = $friend['note'];
        }else{
            $data['note'] = $friend['friend']['nickname'];
        }
        $data['nickname'] = $friend['friend']['nickname'];
        //$data['username'] = $friend['username'];
        $data['avatar'] = $friend['friend']['avatar'];
        $data['idnum'] = $friend['friend']['idnum'];
        $data['black'] = $friend['is_black'];//1=正常,2=拉黑
        $data['gender'] = $friend['friend']['gender'];//性别1=男2=女
        $circlelist = db('chat_circle')
            ->where('user_id',$friend_id)
            ->where('images','not null')
            ->order('id DESC')
            ->limit(3)
            ->select();
        $circle_images = [];
        foreach ($circlelist as $value){
            $circle_images = array_merge($circle_images,explode(',',trim($value['images'],',')));
        }
        $data['images'] = $circle_images;//

        $this->success('ok',$data);
    }

    /**
     * 好友/拉黑/取消拉黑
     */
    public function black_cancel_confirm(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定好友');
        if ($friend_id == 1){
            $this->error('无法拉黑客服');
        }
        $friend = model('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();//好友信息
        if (!$friend){
            $this->error('不是好友或已被删除');
        }
        if ($friend['is_black'] == 1){
            //正常
            db('chat_friend')->where('id',$friend['id'])->update(['is_black'=>'2','updatetime'=>time()]);
            $this->success('拉黑好友成功');
        }else{
            //已经拉黑
            db('chat_friend')->where('id',$friend['id'])->update(['is_black'=>'1','updatetime'=>time()]);
            $this->success('取消拉黑成功');
        }

    }

    /**
     * 删除好友
     */
    public function del_friend(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定好友');
        $friend = model('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();//好友信息
        if (!$friend){
            $this->error('好友指定错误');
        }
        Db::startTrans();
        try {
            $res = db('chat_friend')
                ->where(['user_id'=>$user->id,'friend_id'=>$friend_id])
                ->delete();
            $res = db('chat_friend')
                ->where(['friend_id'=>$user->id,'user_id'=>$friend_id])
                ->delete();
            Db::commit();
        }catch (Exception $exception){
            Db::rollback();
//            $this->error($exception->getMessage());
        }
        if (!$res){
            $this->error('好友删除失败');
        }else{
            $this->success('好友删除成功');
        }
    }

    /**
     * 设置好友备注
     */
    public function set_friend_note(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定好友');
        $note = input('note') ? input('note') :$this->error('请输入备注');
        $friend = model('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();//好友信息
        if (!$friend){
            $this->error('好友指定错误');
        }
        db('chat_friend')->where('id',$friend['id'])->update(['note'=>$note,'updatetime'=>time()]);
        $this->success('设置成功');
    }

    /**
     * 黑名单列表
     */
    public function black_list(){
        $user = $this->auth->getUser();
        $list = model('chat_friend')
            ->with('friend')
            ->where('user_id',$user->id)
            ->where('is_black','2')
            ->select();
        $data = [];
        $list = collection($list)->toArray();
        foreach ($list as $key => $value){
            if ($value['note']){
                $data[$key]['nickname'] = $value['note'];
            }else{
                $data[$key]['nickname'] = $value['friend']['nickname'];
            }
            $data[$key]['avatar'] = $value['friend']['avatar'];
            $data[$key]['id'] = $value['friend']['id'];
        }
        $this->success('ok',$data);
    }

    /**
     * 获取自己的二维码名片
     */
    public function get_my_qrcode(){
        $user = $this->auth->getUser();
        $data = [];
        $data['nickname'] = $user->nickname;
        $data['avatar'] = $user->avatar;
        $data['gender'] = $user->gender;
        $json_qrcode = [];
        $json_qrcode['type'] = 'friend';
        $json_qrcode['idnum'] = $user->idnum;
        $json_qrcode['id'] = $user->id;
        $data['qrcode'] = $this->request->domain().'/qrcode/build?text='.urlencode(json_encode($json_qrcode,JSON_UNESCAPED_UNICODE));
        $this->success('ok',$data);
    }
}