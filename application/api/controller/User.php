<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\library\Ems;
use app\common\library\Sms;
use fast\Random;
use think\Cache;
use think\Validate;
use GatewayClient\Gateway;

/**
 * 会员接口
 */
class User extends Api
{
    protected $noNeedLogin = ['login', 'mobilelogin', 'register', 'resetpwd', 'changeemail', 'changemobile', 'third', 'test', 'forget_pwd'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 会员中心
     */
    public function index()
    {
        $this->success('', ['welcome' => $this->auth->nickname]);
    }

    /**
     * 会员登录
     *
     * @param string $account  账号
     * @param string $password 密码
     */
    public function login()
    {
        $account = $this->request->request('account');
        $password = $this->request->request('password');
        if (!$account || !$password) {
            $this->error(__('Invalid parameters'));
        }
        $ret = $this->auth->login($account, $password);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 手机验证码登录
     *
     * @param string $mobile  手机号
     * @param string $captcha 验证码
     */
    public function mobilelogin()
    {
        $mobile = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        if (!Sms::check($mobile, $captcha, 'mobilelogin')) {
            $this->error(__('Captcha is incorrect'));
        }
        $user = \app\common\model\User::getByMobile($mobile);
        if ($user) {
            if ($user->status != 'normal') {
                $this->error(__('Account is locked'));
            }
            //如果已经有账号则直接登录
            $ret = $this->auth->direct($user->id);
        } else {
            $ret = $this->auth->register($mobile, Random::alnum(), '', $mobile, []);
        }
        if ($ret) {
            Sms::flush($mobile, 'mobilelogin');
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注册会员
     *
     * @param string $username 用户名
     * @param string $password 密码
     * @param int $question_id 问题ID
     * @param string $answer 答案
     */
    public function register()
    {
        $username = $this->request->request('username');
        $password = $this->request->request('password');
        $question = $this->request->request('question_id');
        $answer = $this->request->request('answer');
        if (!$username || !$password) {
            $this->error(__('Invalid parameters'));
        }
        $mobile = '';
        if (Validate::regex($username, "^1\d{10}$")) {
            $mobile = $username;
        }
        $ret = $this->auth->register($username, $password, '', $mobile, ['idnum'=>$this->createIdnum(),'question_id' => $question, 'answer' => $answer, 'avatar' => '/assets/img/avatar.png']);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $service = [];
            $service['user_id'] = 1;
            $service['friend_id'] = $data['userinfo']['id'];
            $service['createtime'] = time();
            $userfriend = [];
            $userfriend['user_id'] = $data['userinfo']['id'];
            $userfriend['friend_id'] = 1;
            $userfriend['createtime'] = time();
            $insert = [$service,$userfriend];//2条好友记录
            db('chat_friend')->insertAll($insert);
            $this->success(__('Sign up successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }
    /**
     * 生成唯一邀请码
     */
    public function createIdnum(){
        $str = 'ABCDEFGHIJKMINOPQRSTUVWXYZ1234567890';
        $newidnum = '';
        for ($i = 0; $i <= 5; $i++){
            //8位邀请码
            $newidnum .= $str{mt_rand(0,35)};
        }
        $res = db('user')->where('idnum',$newidnum)->find();
        if ($res){
            $newidnum = $this->createIdnum();
        }
        return $newidnum;

    }
    /**
     * 退出登录
     */
    public function logout()
    {
        $user = $this->auth->getUser();
        $client_id = input('client_id') ? input('client_id') : '';
        if ($client_id){
            Gateway::unbindUid( $client_id,$user->id);
        }
        $this->auth->logout();
        $this->success(__('Logout successful'));
    }

    /**
     * 修改会员个人信息
     *
     * @param string $avatar   头像地址
     * @param string $username 用户名
     * @param string $nickname 昵称
     * @param string $bio      个人简介
     */
    public function profile()
    {
        $user = $this->auth->getUser();
        $nickname = $this->request->request('nickname');
        $avatar = $this->request->request('avatar');
        $gender = $this->request->request('gender');
        $firend_image = $this->request->request('firend_image');
        if ($nickname) $user->nickname = $nickname;
        if ($avatar) $user->avatar = $avatar;
        if ($gender) $user->gender = $gender;
        if ($firend_image) $user->firend_image = $firend_image;
        $user->save();
        if ($avatar){
             $this->update_group_avatar($user->id);
        }
        $this->success();
    }

    /**
     * 修改头像,变更群头像
     */
    protected function update_group_avatar($user_id){
        $user_all_groups = db('chat_groupuser')
            ->field('id,chat_group_id')
            ->where('user_id',$user_id)
            ->select();//获取该用户的所有群组
        foreach ($user_all_groups as $key => $value){
            //遍历所有的群组id,判断是否需要变更头像
            $list = model('chat_groupuser')
                ->with('user')
                ->where('chat_group_id',$value['chat_group_id'])
                ->order('id asc')
                ->limit(9)
                ->select();
            $groupavatar = [];
            $is_change = false;
            foreach ($list as $kk => $vv){
                $groupavatar[] = $vv['user']['avatar'];
                if ($vv['user_id'] == $user_id){
                    //只在次数处理头像变更
                    $is_change = true;
                }
            }
            if ($is_change){
                getGroupAvatar($groupavatar,true,ROOT_PATH.'public/groupavatar/'.$value['chat_group_id'].'date_'.date('YmdHis').'.jpg');
                db('chat_group')->where('id',$value['chat_group_id'])->update(['avatar'=>'/groupavatar/'.$value['chat_group_id'].'date_'.date('YmdHis').'.jpg']);
            }
        }
        return $is_change;
    }

    /**
     * 修改邮箱
     *
     * @param string $email   邮箱
     * @param string $captcha 验证码
     */
    public function changeemail()
    {
        $user = $this->auth->getUser();
        $email = $this->request->post('email');
        $captcha = $this->request->request('captcha');
        if (!$email || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::is($email, "email")) {
            $this->error(__('Email is incorrect'));
        }
        if (\app\common\model\User::where('email', $email)->where('id', '<>', $user->id)->find()) {
            $this->error(__('Email already exists'));
        }
        $result = Ems::check($email, $captcha, 'changeemail');
        if (!$result) {
            $this->error(__('Captcha is incorrect'));
        }
        $verification = $user->verification;
        $verification->email = 1;
        $user->verification = $verification;
        $user->email = $email;
        $user->save();

        Ems::flush($email, 'changeemail');
        $this->success();
    }

    /**
     * 修改手机号
     *
     * @param string $mobile   手机号
     * @param string $captcha 验证码
     */
    public function changemobile()
    {
        $user = $this->auth->getUser();
        $mobile = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        if (\app\common\model\User::where('mobile', $mobile)->where('id', '<>', $user->id)->find()) {
            $this->error(__('Mobile already exists'));
        }
        $result = Sms::check($mobile, $captcha, 'changemobile');
        if (!$result) {
            $this->error(__('Captcha is incorrect'));
        }
        $verification = $user->verification;
        $verification->mobile = 1;
        $user->verification = $verification;
        $user->mobile = $mobile;
        $user->save();

        Sms::flush($mobile, 'changemobile');
        $this->success();
    }

    /**
     * 第三方登录
     *
     * @param string $platform 平台名称
     * @param string $code     Code码
     */
    public function third()
    {
        $url = url('user/index');
        $platform = $this->request->request("platform");
        $code = $this->request->request("code");
        $config = get_addon_config('third');
        if (!$config || !isset($config[$platform])) {
            $this->error(__('Invalid parameters'));
        }
        $app = new \addons\third\library\Application($config);
        //通过code换access_token和绑定会员
        $result = $app->{$platform}->getUserInfo(['code' => $code]);
        if ($result) {
            $loginret = \addons\third\library\Service::connect($platform, $result);
            if ($loginret) {
                $data = [
                    'userinfo'  => $this->auth->getUserinfo(),
                    'thirdinfo' => $result
                ];
                $this->success(__('Logged in successful'), $data);
            }
        }
        $this->error(__('Operation failed'), $url);
    }

    /**
     * 重置密码
     *
     * @param string $mobile      手机号
     * @param string $newpassword 新密码
     * @param string $captcha     验证码
     */
    public function resetpwd()
    {
        $type = $this->request->request("type");
        $mobile = $this->request->request("mobile");
        $email = $this->request->request("email");
        $newpassword = $this->request->request("newpassword");
        $captcha = $this->request->request("captcha");
        if (!$newpassword || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if ($type == 'mobile') {
            if (!Validate::regex($mobile, "^1\d{10}$")) {
                $this->error(__('Mobile is incorrect'));
            }
            $user = \app\common\model\User::getByMobile($mobile);
            if (!$user) {
                $this->error(__('User not found'));
            }
            $ret = Sms::check($mobile, $captcha, 'resetpwd');
            if (!$ret) {
                $this->error(__('Captcha is incorrect'));
            }
            Sms::flush($mobile, 'resetpwd');
        } else {
            if (!Validate::is($email, "email")) {
                $this->error(__('Email is incorrect'));
            }
            $user = \app\common\model\User::getByEmail($email);
            if (!$user) {
                $this->error(__('User not found'));
            }
            $ret = Ems::check($email, $captcha, 'resetpwd');
            if (!$ret) {
                $this->error(__('Captcha is incorrect'));
            }
            Ems::flush($email, 'resetpwd');
        }
        //模拟一次登录
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, '', true);
        if ($ret) {
            $this->success(__('Reset password successful'));
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 设置密码
     */
    public function set_pwd()
    {
        $oldpassword = $this->request->request("oldpassword");
        $newpassword = $this->request->request("newpassword");
        if(!$oldpassword) $this->error('请输入旧密码');
        if(!$newpassword) $this->error('请输入新密码');

        $user = $this->auth->getUser();
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, $oldpassword);
        if ($ret) {
            $this->success(__('Reset password successful'));
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 忘记密码
     */
    public function forget_pwd()
    {
        $account = $this->request->request("account");
        $question_id = $this->request->request("question_id");
        $answer = $this->request->request("answer");
        $newpassword = $this->request->request("newpassword");
        $newpassword_again = $this->request->request("newpassword_again");

        if(!$account) $this->error('请输入用户名');
        if(!$question_id) $this->error('请选择问题');
        if(!$answer) $this->error('请输入答案');
        if(!$newpassword) $this->error('请输入新密码');
        if(!$newpassword_again) $this->error('请再次输入新密码');

        $user = \app\common\model\User::getByUsername($account);
        if(!$user) $this->error('未找到此用户！');
        if($user->question_id != $question_id) $this->error('当前选择的问题与该用户设置的密保问题不符！');
        if($user->answer != $answer) $this->error('答案错误！');
        if($newpassword !== $newpassword_again) $this->error('两次输入的密码不相同！');
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, '', true);
        if ($ret) {
            $this->success(__('Reset password successful'));
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 重设密码
     */
    public function reset_pwd()
    {
        $question_id = $this->request->request("question_id");
        $answer = $this->request->request("answer");
        $newpassword = $this->request->request("newpassword");
        $newpassword_again = $this->request->request("newpassword_again");

        if(!$question_id) $this->error('请选择问题');
        if(!$answer) $this->error('请输入答案');
        if(!$newpassword) $this->error('请输入新密码');
        if(!$newpassword_again) $this->error('请再次输入新密码');

        $user = $this->auth->getUser();
        if(!$user) $this->error('未找到此用户！');
        if($user->question_id != $question_id) $this->error('当前选择的问题与该用户设置的密保问题不符！');
        if($user->answer != $answer) $this->error('答案错误！');
        if($newpassword !== $newpassword_again) $this->error('两次输入的密码不相同！');
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, '', true);
        if ($ret) {
            $this->success(__('Reset password successful'));
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 设置支付密码
     */
    public function set_pay_pwd()
    {
        $question_id = $this->request->request("question_id");
        $answer = $this->request->request("answer");
        $newpassword = $this->request->request("pay_password");
        if(!$question_id) $this->error('请选择问题');
        if(!$answer) $this->error('请输入答案');
        if(!$newpassword) $this->error('请输入新密码');
        $user = $this->auth->getUser();
        if(!$user) $this->error('未找到此用户！');
        if($user->question_id != $question_id) $this->error('当前选择的问题与该用户设置的密保问题不符！');
        if($user->answer != $answer) $this->error('答案错误！');
        $user->pay_password = md5(md5($newpassword) . $user->salt);
        $user->save();
        $this->success('设置密码成功');
    }

    /**
     * 重设支付密码
     */
    public function reset_pay_pwd()
    {
        $oldpassword = $this->request->request("oldpassword");
        $newpassword = $this->request->request("pay_password");
        if(!$oldpassword) $this->error('请输入原密码');
        if(!$newpassword) $this->error('请输入新密码');
        $user = $this->auth->getUser();
        if(md5(md5($oldpassword) . $user->salt) != $user->pay_password) $this->error('旧密码错误');
        if(!$user) $this->error('未找到此用户！');
        $user->pay_password = md5(md5($newpassword) . $user->salt);
        $user->save();
        $this->success(__('Reset password successful'));
    }

    public function get_user_infos()
    {
        $this->success('获取成功', $this->auth->getUser());
    }

    public function chongzhi()
    {
        $pn_id = input('pn_id') ? input('pn_id') : $this->error('请选择充值项');
        $pay_pwd = input('pay_pwd') ? input('pay_pwd') : $this->error('请输入支付密码');
        $pn_data = db('post_num')->find($pn_id);
        $user = $this->auth->getUser();
        if(!$user->pay_password) $this->error('请设置支付密码', '', 201);
        if(md5(md5($pay_pwd) . $user->salt) != $user->pay_password) $this->error('支付密码错误');
        if($user->money < $pn_data['price']) $this->error('余额不足');
        $user->money -= $pn_data['price'];
        $user->buy_post_num += $pn_data['num'];
        $data = [
            'user_id' => $user->id,
            'money' => $pn_data['price'],
            'type' => 3,
            'remark' => '购买发帖次数',
            'createtime' => time()
        ];
        db('consp_flow')->insert($data);
        $user->save();
        $this->success('充值成功');
    }

    public function test()
    {
        $this->auth->direct(1);
        $this->success('获取成功', $this->auth->getUserinfo());
    }
    //数字摇号机
    public function shuziyaohao(){
        $user = $this->auth->getUser();
        $yaohao = '';
        if ($user->yaohao){
            $yaohao = $user->yaohao;
        }else{
            for ($i = 0; $i<3;$i++){
                $rand = rand(0,9);
                $yaohao .= $rand;
            }
        }
        $arr = [];
        $arr[] = $yaohao{0};
        $arr[] = $yaohao{1};
        $arr[] = $yaohao{2};
        $this->success('ok',$arr);
    }
}
