<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\UserAddress;

class Address extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     * 显示用户收货地址列表
     *
     */
    public function index(UserAddress $uaModel)
    {
        $user = $this->auth->getUserinfo();
        $list = $uaModel::getList($user['id']);
        // if (!$list) $this->error('暂无数据!');
        $this->success('ok', $list);
    }

    /**
     * 新增收货地址
     *
     * @param string $address 收货地址
     * @param string $name 收件人姓名
     * @param string $mobile 电话
     * @param string $detail_address 详细地址
     * @param int   $is_default  是否为默认地址:1=否,2=是',
     *
     */
    public function create(UserAddress $uaModel)
    {
        $param = $this->request->param();
        $user = $this->auth->getUserinfo();
        if (!$uaModel::addInfo($user['id'], $param)) $this->error('该地址已经存在!');
        $this->success('新增成功!');
    }

    /**
     * 显示编辑收货地址
     *
     * @param  int  $id
     */
    public function edit(UserAddress $uaModel)
    {
        $id = input('id') ?? $this->error('参数错误!');
        $data = $uaModel::with('area')
        	->field('id, user_id, area_id, address, name, mobile, is_default, detail_address, code')
            ->find($id);
        $arr = explode(',', $data['address']);
        $addresArr = [];
        foreach ($arr as $k => $value) {
            $addresArr[$k] = ['name' => $value];
        }

        $data['arr'] = $addresArr;

        $this->success('success', $data);
    }

    /**
     * 保存更新收货地址
     *
     * @param  int  $id
     */
    public function update(UserAddress $uaModel)
    {
        $param = $this->request->param();
        $user = $this->auth->getUserinfo();
        $this->success('保存成功!', $uaModel::updateInfo($user['id'], $param));
    }

    /**
     * 删除指定收货地址
     *
     * @param  int  $id
     */
    public function delete(UserAddress $uaModel)
    {
        $id = $this->request->param('id') ?? $this->error('fail');
        $this->success('删除成功!', $uaModel::destroy($id, true));
    }
}
