<?php


namespace app\api\controller;


class Shop extends \app\common\controller\Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 获取对应店铺的商品分类
     */
    public function get_category_list(){
        $admin_id = input('shop_id') ? input('shop_id'):$this->error('请先指定店铺');
        $list = db('shop_category')
            ->field('id,name')
            ->where('admin_id',$admin_id)//指定店铺
            ->where('pid',0)//顶级分类,反正没啥用
            ->order('id ASC')
            ->select();
        $this->success('ok',$list);
    }

    /**
     * 获取商品列表
     */
    public function get_products(){
        $admin_id = input('shop_id') ? input('shop_id'):$this->error('请先指定店铺');
        $category_id = input('category_id') ? input('category_id') : $this->error('请指定商品分类');
        $category_arr = $this->get_son_category([$category_id]);//获取所有分类
        echo '<pre>';
        print_r($category_arr);
        die();
    }

    /**
     * 获取分类下所有下级分类
     */
    public function get_son_category($category_arr){
        $category_arr = db('shop_category')
            ->where('pid','in',$category_arr)
            ->column('id');
        if ($category_arr){
            return $category_arr = array_merge($category_arr,$this->get_son_category($category_arr));
        }
        return $category_arr;
    }
}