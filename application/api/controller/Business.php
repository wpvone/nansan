<?php


namespace app\api\controller;

use app\common\model\Product;
use app\common\model\BusinessType;
use app\common\model\Business as bModel;

/**
 * 商户店铺类
 * @package app\api\controller
 */
class Business extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];
    protected $noNeedLogin = ['*'];

    /**
     * 获取商铺分类
     */
    public function type_list()
    {
        $this->success('获取成功', BusinessType::order('weigh desc')->field('id,name')->select());
    }

    /**
     * 获取商铺列表
     */
    public function get_list()
    {
        $type_id = input('type_id') ? input('type_id') : 0;
        $page = input('page') ? input('page') : 1;
        $pageSize = input('pageSize') ? input('pageSize') : 10;
        $type_id ? $where = ['type_id' => $type_id] : $where = [];
        $data = bModel::where($where)->field('id,name,image,brand,sell_num')->paginate($pageSize);
        $this->success('获取成功', $data);
    }

    /**
     * 获取商铺首页信息
     */
    public function index()
    {
        $id = input('business_id') ? input('business_id') : $this->error('缺少关键参数“business_id”');
        $data = bModel::field('id,name,image,brand,sell_num')->find($id);
        $this->success('获取成功', $data);
    }
}