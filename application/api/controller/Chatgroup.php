<?php


namespace app\api\controller;


use GatewayClient\Gateway;
use think\Db;
use think\Exception;

class Chatgroup extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];

    /**
     * 发起群聊
     */
    public function launch_group(){
        $user = $this->auth->getUser();
        $friend_ids = input('friend_ids') ? input('friend_ids'):$this->error('请先指定进群好友');
        $friends = db('user')
            ->field('id,nickname,avatar')
            ->where('id','in',trim($friend_ids,','))
            ->select();
        if (!$friends){
            $this->error('进群好友指定错误');
        }
        Db::startTrans();
        try {
            $group = [];
            $group['user_id'] = $user->id;
            $group['name'] = $user->nickname.','.implode(',',array_column($friends,'nickname'));
            $group['createtime'] = time();
            $group_id = db('chat_group')->insertGetId($group);
            if ($group_id){
                $group_avatar = getGroupAvatar(array_merge([$user->avatar],array_column($friends,'avatar')),true,ROOT_PATH.'public/groupavatar/'.$group_id.'.jpg');
                db('chat_group')->where('id',$group_id)->update(['avatar'=>'/groupavatar/'.$group_id.'.jpg']);
            }else{
                $this->error('发起群聊失败');
            }
            array_unshift($friends,['id'=>$user->id,'nickname'=>$user->nickname,'avatar'=>$user->avatar]);
            $members = [];
            foreach ($friends as $key => $value){
                $members[$key]['user_id'] = $value['id'];
                $members[$key]['chat_group_id'] = $group_id;
                $members[$key]['createtime'] = time();
            }
            db('chat_groupuser')->insertAll($members);
            foreach ($friends as $value){
                //遍历绑定群聊
                $client_ids = Gateway::getClientIdByUid($value['id']);//获取所有绑定的Client_id
                foreach ($client_ids as $client_id){
                    Gateway::joinGroup($client_id,$group_id);
                }
            }
            $content = [];
            $content['type'] = 'system';
            $content['group_id'] = $group_id;
            $content['from_id'] = $user->id;
            $content['categorytype'] = 'group';
            $content['personorgroup'] = 2;
            $content['msg'] = [];
            $content['msg']['id'] = 0;
            $content['msg']['type'] = 'text';
            $content['msg']['content'] = [];
            $content['msg']['content']['text'] = '我们开始聊天吧';
            $content['msg']['to_group_info'] = [];
            $content['msg']['to_group_info']['face'] = '/groupavatar/'.$group_id.'.jpg';
            $content['msg']['to_group_info']['groupname'] = $group['name'];
            Gateway::sendToGroup($group_id,json_encode($content,JSON_UNESCAPED_UNICODE));
            Db::commit();
        }catch (Exception $exception){
            Db::rollback();
            $this->error($exception->getMessage());
        }
        $data = [];
        $data['group_id'] = $group_id;
        $data['nickname'] = $group['name'];
        $data['avatar'] = '/groupavatar/'.$group_id.'.jpg';

        $this->success('发起群聊成功',$data);
    }

    /**
     * 加入群聊
     */
    public function into_group(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if ($is_member){
            $this->error('请勿重复添加');
        }
        $group = db('chat_group')->find($group_id);
        $res = $this->user_into_group($user->id,$group_id);
        if ($res){
            $content = [];
            $content['type'] = 'system';
            $content['group_id'] = $group_id;
            $content['msg'] = [];
            $content['msg']['id'] = 0;
            $content['msg']['type'] = 'text';
            $content['msg']['content'] = [];
            $content['msg']['content']['text'] = '欢迎 '.$user->nickname.' 进入群聊';
            $content['msg']['to_group_info']['face'] = $group['avatar'];
            $content['msg']['to_group_info']['groupname'] = $group['name'];
            Gateway::sendToGroup($group_id,json_encode($content,JSON_UNESCAPED_UNICODE));
            $this->success('进群成功');
        }else{
            $this->success('进群失败');
        }
    }

    /**
     * 离开群聊
     */
    public function leave_group(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $res = $this->user_out_group($user->id,$group_id);
        $group = db('chat_group')->find($group_id);
        if ($res){
            $content = [];
            $content['type'] = 'system';
            $content['group_id'] = $group_id;
            $content['msg'] = [];
            $content['msg']['id'] = 0;
            $content['msg']['type'] = 'text';
            $content['msg']['content'] = [];
            $content['msg']['content']['text'] = $user->nickname.' 离开群聊';
            $content['msg']['to_group_info']['face'] = $group['avatar'];
            $content['msg']['to_group_info']['groupname'] = $group['name'];
            Gateway::sendToGroup($group_id,json_encode($content,JSON_UNESCAPED_UNICODE));
            $this->success('离开群聊成功');
        }else{
            $this->error('离开群聊失败');
        }
    }

    /**
     * 群管理----10-21-77
     */
    public function group_manager(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->error('您不是群成员');
        }
        $user_id = $user->id;
        $group = model('chat_group')
            ->with(['groupuser'=>function($query) use ($user_id){
                $query
                    ->with('user')
                    ->with(['friend'=>function($q) use ($user_id){
                        $q->where('user_id',$user_id);
                    }])
                    ->order('id ASC');
            }])
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群指定错误');
        }
        $member_list = [];
        foreach ($group['groupuser'] as $key => $value){
            //如果群成员是好友 ,则展示好友的备注或昵称
            if ($value['friend'] && $value['friend']['note']){
                //是好友,且有设置备注
                $member_list[$key]['nickname'] = $value['friend']['note'];
            }elseif ($value['in_group_name']){
                //不是好友,但该群成员设置了群内昵称
                $member_list[$key]['nickname'] = $value['in_group_name'];
            }else{
                //没有设置群内昵称
                $member_list[$key]['nickname'] = $value['user']['nickname'];
            }
            $member_list[$key]['avatar'] = $value['user']['avatar'];
            $member_list[$key]['id'] = $value['user']['id'];//群成员用户id
        }
        $data = [];
        $data['list'] = $member_list;
        $data['group_name'] = $group['name'];
        $data['is_top'] = $is_member['is_top'];
        if($group['user_id'] == $user->id){
            $data['is_master'] = true;
        }else{
            $data['is_master'] = false;
        }
        $this->success('ok',$data);
    }

    /**
     * 拉人进群
     */
    public function invite_into_group(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $friend_ids = input('friend_ids') ? input('friend_ids') : $this->error('请指定好友');//用户ids
        $group = model('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群指定错误');
        }
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->error('你不是群成员,不可拉人进群');
        }
        $friend_is_member = db('chat_groupuser')
            ->where('user_id','in',$friend_ids)
            ->where('chat_group_id',$group_id)
            ->find();
        if ($friend_is_member){
            $this->error('存在好友已是群成员');
        }
        $friends = explode(',',trim($friend_ids,','));
        $members_name = implode(',',db('user')->where('id','in',$friends)->column('nickname'));
        foreach ($friends as $friend_id){
            $res = $this->user_into_group($friend_id,$group_id);
        }
        if ($res){
            //  {type:"system",msg:{id:0,type:"text",content:{text:"欢迎进入HM-chat聊天室"}}},
            $content = [];
            $content['type'] = 'system';
            $content['group_id'] = $group_id;
            $content['msg'] = [];
            $content['msg']['id'] = 0;
            $content['msg']['type'] = 'text';
            $content['msg']['content'] = [];
            $content['msg']['content']['text'] = '欢迎 '.$members_name.' 进入群聊';
            $content['msg']['to_group_info']['face'] = $group['avatar'];
            $content['msg']['to_group_info']['groupname'] = $group['name'];
            Gateway::sendToGroup($group_id,json_encode($content,JSON_UNESCAPED_UNICODE));
            $this->success('拉人进群成功');
        }else{
            $this->error('拉人进群失败');
        }
    }

    /**
     * 踢人出群
     */
    public function eliminate_group_user(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $friend_ids = input('friend_ids') ? input('friend_ids') : $this->error('请指定离开群聊的用户');
        $group = db('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群指定错误');
        }
        if ($group['user_id'] != $user->id){
            $this->error('只有群主有踢人权限');
        }
        $friends = explode(',',trim($friend_ids,','));
        $members_name = implode(',',db('user')->where('id','in',$friends)->column('nickname'));
        foreach ($friends as $friend_id){
            $res = $this->user_out_group($friend_id,$group_id);
        }
        if ($res){
            $content = [];
            $content['type'] = 'system';
            $content['group_id'] = $group_id;
            $content['msg'] = [];
            $content['msg']['id'] = 0;
            $content['msg']['type'] = 'text';
            $content['msg']['content'] = [];
            $content['msg']['content']['text'] = $members_name.' 离开群聊';
            $content['msg']['to_group_info']['face'] = $group['avatar'];
            $content['msg']['to_group_info']['groupname'] = $group['name'];
            Gateway::sendToGroup($group_id,json_encode($content,JSON_UNESCAPED_UNICODE));
            $this->success('踢出群聊成功');
        }else{
            $this->error('踢出群聊失败');
        }
    }

    /**
     * 内部使用的   用户进群
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    protected function user_into_group($user_id,$group_id){

        $group = model('chat_group')
            ->with(['groupuser'=>function($query){
                $query->with('user')->order('id ASC');
            }])
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群指定错误');
        }
        $groupname = '';
        $groupavatar = [];
        $update_data = [];
        $i = 0;
        if (strpos($group['name'],',') && mb_strlen($group['name'] < 20)){
            //存在 ,  则表明 群名是默认拼接的
            foreach ($group['groupuser'] as $key => $value){
                $i++;
                $groupname .= $value['user']['nickname'];
                $groupavatar[] = $value['user']['avatar'];
                if ($i == 9){
                    break;
                }
            }
        }else{
            foreach ($group['groupuser'] as $key => $value){
                $i++;
                $groupavatar[] = $value['user']['avatar'];
                if ($i == 9){
                    break;
                }
            }
        }
        if ($groupname){
            $update_data['name'] = $groupname;
        }
        $update_data['updatetime'] = time();
        db('chat_group')->where('id',$group_id)->update($update_data);
        $group_user = [];
        $group_user['user_id'] = $user_id;
        $group_user['chat_group_id'] = $group_id;
        $group_user['createtime'] = time();
        $res = db('chat_groupuser')->insert($group_user);
        $imagestatus = getGroupAvatar($groupavatar,true,ROOT_PATH.'public/groupavatar/'.$group_id.'date_'.date('YmdHis').'.jpg');
        if ($imagestatus){
            db('chat_group')->where('id',$group_id)->update(['avatar'=>'/groupavatar/'.$group_id.'date_'.date('YmdHis').'.jpg']);
        }
        if ($res){
            //遍历绑定群聊
            $client_ids = Gateway::getClientIdByUid($user_id);//获取所有绑定的Client_id
            foreach ($client_ids as $client_id){
                Gateway::joinGroup($client_id,$group_id);
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 内部使用 用户离开群聊
     */
    protected function user_out_group($user_id,$group_id){
        $group = model('chat_group')
            ->with(['groupuser'=>function($query){
                $query->with('user')->order('id ASC');
            }])
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群指定错误');
        }
        $is_member = db('chat_groupuser')
            ->where('user_id',$user_id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->error('用户不是群成员');
        }
        if ($user_id == $group['user_id']){
            //群主退群
            $lastmaster = db('chat_groupuser')
                ->where('chat_group_id',$group_id)
                ->where('user_id','<>',$user_id)
                ->order('id asc')
                ->find();
            if ($lastmaster){
                db('chat_group')->where('id',$group_id)->update(['user_id'=>$lastmaster['user_id']]);
            }
        }
        $friend = model('user')->find($user_id);
        $groupname = '';
        $i = 0;
        if (strpos($group['name'],',') && strpos($group['name'],$friend->nickname)) {
            //存在 ,  则表明 群名是默认拼接的 而其自己的名字在其中
            foreach ($group['groupuser'] as $key => $value){
                if ($value['user']['id'] != $friend->id){
                    $i++;
                    $groupname .= $value['user']['nickname'];
                    $groupavatar[] = $value['user']['avatar'];
                }
                if ($i == 9){
                    break;
                }
            }
        }else{
            foreach ($group['groupuser'] as $key => $value){
                $i++;
                $groupavatar[] = $value['user']['avatar'];
                if ($i == 9){
                    break;
                }
            }
        }
        $res = db('chat_groupuser')->where('id',$is_member['id'])->delete();
        $imagestatus = getGroupAvatar($groupavatar,true,ROOT_PATH.'public/groupavatar/'.$group_id.'date_'.date('YmdHis').'.jpg');
        if ($imagestatus){
            db('chat_group')->where('id',$group_id)->update(['avatar'=>'/groupavatar/'.$group_id.'date_'.date('YmdHis').'.jpg']);
        }
        if ($res){
            //遍历退出群聊
            $client_ids = Gateway::getClientIdByUid($user_id);//获取所有绑定的Client_id
            foreach ($client_ids as $client_id){
                Gateway::leaveGroup($client_id,$group_id);
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 设置群聊名称
     */
    public function set_group_name(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $name = input('name') ? input('name'): $this->error('请输入群名称');
        if (strpos($name,',')){
            $this->error('群名不能含有 ' . ',' .' ');
        }
        if (mb_strlen($name) > 20){
            $this->error('请控制群名长度小于20位');
        }
        $group = db('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群组指定错误');
        }
        if ($group['user_id'] != $user->id){
            $this->error('只有群主才可以修改群名称');
        }
        $res = db('chat_group')
            ->where('id',$group_id)
            ->update(['name'=>$name,'updatetime'=>time()]);
        if ($res){
            $this->success('群名设置成功');
        }else{
            $this->error('群名设置失败');
        }
    }

    /**
     * 群二维码名片
     */
    public function group_qrcode(){
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $group = db('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群组指定错误');
        }
        $data = [];
        $data['name'] = $group['name'];
        $data['avatar'] = $group['avatar'];
        $json_qrcode = [];
        $json_qrcode['type'] = 'group';
        $json_qrcode['id'] = $group_id;
        $json_qrcode['name'] = $group['name'];
        $json_qrcode['avatar'] = $group['avatar'];
        $data['qrcode'] = $this->request->domain().'/qrcode/build?text='.urlencode(json_encode($json_qrcode,JSON_UNESCAPED_UNICODE));
        $this->success('ok',$data);
    }

    /**
     * 设置群内昵称
     */
    public function set_iner_group_name(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $name = input('name') ? input('name'):$this->error('请输入昵称');
        $group = db('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群组指定错误');
        }
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->error('用户不是群成员');
        }
        if ($name == $is_member['in_group_name']){
            $this->success('设置群内昵称成功');
        }
        $res = db('chat_groupuser')
            ->where('id',$is_member['id'])
            ->update(['in_group_name'=>$name]);
        if ($res){
            $this->success('设置群内昵称成功');
        }else{
            $this->error('设置群内昵称失败');
        }
    }

    /**
     * 设置修改群公告
     */
    public function set_group_notice(){
        $group_id = input('group_id') ? input('group_id'): $this->error('请指定群组');
        $notice_content = input('note_content') ? input('note_content') : $this->error('请输入群公告内容');
        $user = $this->auth->getUser();
        $group = db('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群组指定错误');
        }
        if ($group['user_id'] != $user->id){
            $this->error('只有群主才可以操作群公告');
        }
        if ($notice_content == $group['note_content']){
            $this->success('设置成功');
        }
        $res = db('chat_group')->where('id',$group_id)->update(['note_content'=>$notice_content,'updatetime'=>time()]);
        if ($res){
            $this->success('设置成功');
        }else{
            $this->error('设置失败');
        }
    }

    /**
     * 获取群公告内容
     */
    public function get_group_notice(){
        $group_id = input('group_id') ? input('group_id'): $this->error('请指定群组');
        $group = db('chat_group')
            ->find($group_id);//群信息
        if (!$group){
            $this->error('群组指定错误');
        }
        $this->success('ok',$group['note_content']);
    }

    /**
     * 获取群聊投诉项
     */
    public function get_group_com(){
        $list = db('chat_group_commsg')
            ->select();
        $this->success('ok',$list);
    }

    /**
     * 投诉举报群组
     */
    public function com_group(){
        $user = $this->auth->getUser();
        $msg = input('msg') ? input('msg') : $this->error('请指定投诉内容');
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定投诉群组');
        $group = db('chat_group')
            ->find($group_id);
        if (!$group){
            $this->error('群组指定错误');
        }
        $data = [];
        $data['user_id'] = $user->id;
        $data['group_id'] = $group_id;
        $data['msg'] = $msg;
        $res = db('chat_group_comlist')->where($data)->whereTime('createtime','d')->find();
        if ($res){
            $this->error('今日已经投诉举报该群啦,请等待管理员审核');
        }
        $data['createtime'] = time();
        $res = db('chat_group_comlist')->insert($data);
        if ($res){
            $this->success('投诉成功');
        }else{
            $this->error('投诉失败');
        }
    }

    /**
     * 置顶_取消置顶
     */
    public function topno(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id') : $this->error('请指定群组');
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->error('用户不是群成员');
        }
        if ($is_member['is_top'] == 1){
            db('chat_groupuser')->where('id',$is_member['id'])->update(['is_top'=>'2']);
            $this->success('置顶成功');
        }else{
            db('chat_groupuser')->where('id',$is_member['id'])->update(['is_top'=>'1']);
            $this->success('取消置顶成功');
        }
    }
}