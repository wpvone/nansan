<?php


namespace app\api\controller;
use GatewayClient\Gateway;
/**
 * Class 聊天相关
 * @package app\api\controller
 */
class Chat extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];

    /**
     * 好友聊天
     */
    public function friend_chat(){
        $user = $this->auth->getUser();
        $message = input('message') ? input('message') : $this->success('请输入发送内容');
        $friend_id = input('friend_id') ? input('friend_id'): $this->error('请指定发送好友对象');
        if (!Gateway::isUidOnline($user->id)){
            $this->error('断线重连');
        }
        if ($friend_id == 1 && $user->id == $friend_id){
            $friend['note'] = $user->nickname;
        }else{
            $friend = db('chat_friend')
                ->where('friend_id',$user->id)
                ->where('user_id',$friend_id)
                ->find();//查看对方对自己的好友状态
            if (!$friend){
                $this->error('不是好友或被对方删除');
            }
            $friend_my = db('chat_friend')
                ->where('user_id',$user->id)
                ->where('friend_id',$friend_id)
                ->find();//查看自己对 对方 的好友状态
            if (!$friend_my){
                $this->error('不是好友或被对方删除');
            }
            if($friend['is_black'] == 2 || $friend_my['is_black'] == 2){
                $this->error('消息已发出,但被拒收了');
            }
        }
        $content = htmlspecialchars_decode($message);
        $content = json_decode($content,true);
        $content['msg']['userinfo']['face'] = $user->avatar;
        if ($friend['note']){
            $content['msg']['userinfo']['username'] = $friend['note'];
        }else{
            $content['msg']['userinfo']['username'] = $user->nickname;
        }
        $content_to_friend = json_encode($content,JSON_UNESCAPED_UNICODE);
        $content['msg']['userinfo']['username'] = $user->nickname;
        $content_to_my = json_encode($content,JSON_UNESCAPED_UNICODE);
        $data = [];
        $data['user_id'] = $user->id;
        $data['friend_id'] = $friend_id;
        $data['message'] = $content_to_friend;
        $data['createtime'] = time();
        if (Gateway::isUidOnline($friend_id)){
            //好友在线
            $res = db('chat_friend_massage')->insert($data);
            if ($res){
                Gateway::sendToUid($user->id,$content_to_my);
                Gateway::sendToUid($friend_id,$content_to_friend);
                $this->success('发送成功');
            }else{
                $this->error('发送失败');
            }
        }else{
            //好友不在线
            $data['is_service'] = '2';//为送达
            $res = db('chat_friend_massage')->insert($data);
            if ($res){
                Gateway::sendToUid($user->id,$content_to_my);
                $this->success('发送成功');
            }else{
                $this->error('发送失败');
            }
        }
    }

    /**
     * 群聊
     */
    public function group_chat(){
        $user = $this->auth->getUser();
        $message = input('message') ? input('message') : $this->success('请输入发送内容');
        $group_id = input('group_id') ? input('group_id'): $this->error('请指定发送群组对象');
        if (!Gateway::isUidOnline($user->id)){
            $this->error('断线重连');
        }
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->error('您已不是群成员');
        }
        $group = db('chat_group')->find($group_id);
        $content = htmlspecialchars_decode($message);
        $content = json_decode(stripslashes($content),true);
        $content['msg']['userinfo']['face'] = $user->avatar;
        $content['msg']['userinfo']['username'] = $user->nickname;
        $content['msg']['to_group_info']['face'] = $group['avatar'];
        $content['msg']['to_group_info']['groupname'] = $group['name'];
        $content = stripslashes(json_encode($content,JSON_UNESCAPED_UNICODE));
        $data = [];
        $data['user_id'] = $user->id;
        $data['group_id'] = $group_id;
        $data['message'] = $content;
        $data['noread_members'] = ','.implode(',',db('chat_groupuser')->where('chat_group_id',$group_id)->column('user_id')).',';
        $data['createtime'] = time();
        $res = db('chat_group_message')->insert($data);
        if ($res){
            Gateway::sendToGroup($group_id,$content);
            $this->success('发送成功');
        }else{
            $this->error('发送失败');
        }
    }

    /**
     * 用户重新登录断线重连 消息补推
     */
    public function user_init(){
        $user = $this->auth->getUser();
        $client_id = input('client_id') ? input('client_id') : $this->error('请指定webscoket链接ID');
        if(!Gateway::isOnline($client_id)){
            $this->error('当期client_id无效');
        }
        Gateway::bindUid($client_id,$user->id);
        $group_ids = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->column('chat_group_id');//获取加入的所有群组
        foreach ($group_ids as $group_id){
            Gateway::joinGroup($client_id,$group_id);
        }
        $friend_msg_list = db('chat_friend_massage')
            ->where('friend_id',$user->id)
            ->where('is_service','2')
            ->select();
        foreach ($friend_msg_list as $value){
            Gateway::sendToUid($user->id,$value['message']);
        }
        db('chat_friend_massage')
            ->where('friend_id',$user->id)
            ->where('is_service','2')
            ->update(['is_service'=>'1']);
        $groups = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->column('id');
        $group_msg_list = db('chat_group_message')
            ->field('id,message,noread_members')
            ->where('group_id','in',$groups)
            ->where('noread_members','like','%,'.$user->id.',%')
            ->select();
        foreach ($group_msg_list as $value){
            Gateway::sendToUid($user->id,$value['message']);
        }
        $data = [];
        foreach ($group_msg_list as $key => $value){
            $noread_users = explode(',',trim($value['noread_members'],','));
            $keys = array_search($user->id,$noread_users);
            unset($noread_users[$keys]);
            $data[$key]['id'] = $value['id'];
            $data[$key]['noread_members'] = ','.implode(',',$noread_users).',';
        }
        model('chat_group_message')->isUpdate(true)->saveAll($data);
        $this->success('ok');
    }
}