<?php
namespace app\api\controller\three;

class Three extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];
    //帖子充值
    public function tiezichongzhi(){
        $pn_id = input('pn_id') ? input('pn_id') : $this->error('请选择充值项');
        $pn_data = db('post_num')->find($pn_id);
        if (!$pn_data || $pn_data['price'] <= 0) {
            $this->error("支付金额必须大于0");
        }
        $user = $this->auth->getUser();
        $pay_type = input('pay_type') ? input('pay_type') : $this->error('请指定付款方式');//1=微信,2=支付宝
        if ($pay_type == 1){
            //微信
            $type = 'wechat';
        }else{
            //支付宝
            $type = 'alipay';
        }
        $out_trade_no = 'Z'.date("YmdHis") . mt_rand(100000, 999999);
        $params = [
            'amount'=>$pn_data['price'],
            'orderid'=>$out_trade_no,
            'type'=>$type,
            'title'=>"帖子充值",
            'notifyurl'=> $this->request->root(true) ."/api/three/Back/tiezinotifyx/paytype/".$type.'/postnum/'.$pn_data['num'].'/userid/'.$user->id,
            'returnurl'=> $this->request->root(true) ."/api/three/Back/returnx/paytype/".$type.'/postnum/'.$pn_data['num'].'/userid/'.$user->id,
            'method'=>"app"
        ];
        $params['amount'] = 0.01;
        return \addons\epay\library\Service::submitOrder($params);
    }

    /**
     * 三方充值
     */
    public function recharge(){
        $money = input('money') ? input('money') : $this->error('请输入金额');
        $pay_type = input('pay_type') ? input('pay_type') : $this->error('请指定支付方式');
        if ($pay_type == 1){
            //微信
            $type = 'wechat';
        }else{
            //支付宝
            $type = 'alipay';
        }
        if (!$money || $money <= 0) {
            $this->error("支付金额必须大于0");
        }
        $user = $this->auth->getUser();
        $out_trade_no = 'R'.date("YmdHis") . mt_rand(100000, 999999);
        $params = [
            'amount'=>$money,
            'orderid'=>$out_trade_no,
            'type'=>$type,
            'title'=>"余额充值",
            'notifyurl'=> $this->request->root(true) ."/api/three/Back/rechargenotifyx/paytype/".$type.'/money/'.$money.'/userid/'.$user->id,
            'returnurl'=> $this->request->root(true) ."/api/three/Back/rechargereturnx/paytype/".$type.'/money/'.$money.'/userid/'.$user->id,
            'method'=>"app"
        ];
        $params['amount'] = 0.01;

        $recharge = [];
        $recharge['flow_sn'] = $out_trade_no;
        $recharge['user_id'] = $user->id;
        $recharge['remark'] = '';
        $recharge['type'] = $pay_type;
        $recharge['status'] = '1';
        $recharge['createtime'] = time();
        $recharge['money'] = $money;
        $res = db('recharge_flow')->insert($recharge);
        if ($res){
            return \addons\epay\library\Service::submitOrder($params);
        }else{
            return false;
        }
    }

    /**
     * 这里是订单支付,反正我也看不懂
     */
    public function orderpay(\app\common\model\Order $oModel){
        $id = input('order_id') ?? $this->error('缺少参数!');
        $type = input('pay_type') ?? 1;
        $user = $this->auth->getUser();
        if ($type == 1){
            //微信
            $type = 'wechat';
        }else{
            //支付宝
            $type = 'alipay';
        }
        $orderInfo = $oModel->where([
            'id' => ['IN', $id],
            'user_id' => $user['id']
        ])->select();
        $count = $oModel->where([
            'id' => ['IN', $id],
            'user_id' => $user['id']
        ])->select();
        $cnums = count(explode(',', $id));
        if (count($count) !== $cnums) $this->error('订单参数补全!');
        $total_price = 0;
        $saveData = [];
        foreach ($orderInfo as $k => $value) {
            $total_price += $value['actual_price'];
        }
        $out_trade_no = 'O'.date("YmdHis") . mt_rand(100000, 999999);
        $params = [
            'amount'=>$total_price,
            'orderid'=>$out_trade_no,
            'type'=>$type,
            'title'=>"订单支付",
            'notifyurl'=> $this->request->root(true) ."/api/three/Back/orderpaynotifyx/paytype/".$type.'/userid/'.$user->id.'/orderid/'.$id,
            'returnurl'=> $this->request->root(true) ."/api/three/Back/orderpayreturnx/paytype/".$type.'/userid/'.$user->id.'/orderid/'.$id,
            'method'=>"app"
        ];
        $params['amount'] = 0.01;
        return \addons\epay\library\Service::submitOrder($params);
    }
    //入驻支付
    public function ruzhu(){
        $apply_num = input('apply_num') ? input('apply_num') : $this->error('请输入入驻号码');
        $apply_name = input('apply_name') ? input('apply_name') : $this->error('请输入入驻名称');
        $real_name = input('real_name') ? input('real_name') : $this->error('请输入真实姓名');
        $mobile = input('mobile') ? input('mobile') : $this->error('请输入联系电话');
        $user = $this->auth->getUser();
        $pay_type = input('pay_type') ? input('pay_type') : $this->error('请指定支付方式');//1=微信,2=支付宝
        if ($pay_type == 1){
            //微信
            $type = 'wechat';
        }else{
            //支付宝
            $type = 'alipay';
        }
        $money = db('config')->where('name','apply_price')->value('value')??0.01;
        if (!$money || $money <= 0) {
            $this->error("支付金额必须大于0");
        }
        $out_trade_no = 'RZ'.date("YmdHis") . mt_rand(100000, 999999);
        $params = [
            'amount'=>$money,
            'orderid'=>$out_trade_no,
            'type'=>$type,
            'title'=>"申请入驻",
            'notifyurl'=> $this->request->root(true) ."/api/three/Back/ruzhunotifyx/paytype/".$type.'/userid/'
                .$user->id.'/applynum/'.$apply_num.'/applyname/'.$apply_name.'/realname/'.$real_name.'/mobile/'.$mobile,
            'returnurl'=> $this->request->root(true) ."/api/three/Back/ruzhureturnx/paytype/".$type.'/userid/'
                .$user->id.'/applynum/'.$apply_num.'/applyname/'.$apply_name.'/realname/'.$real_name.'/mobile/'.$mobile,
            'method'=>"app"
        ];
        $params['amount'] = 0.01;
        return \addons\epay\library\Service::submitOrder($params);
    }
}