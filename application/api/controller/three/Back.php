<?php


namespace app\api\controller\three;


use addons\epay\library\Service;
use app\common\model\Order;

class Back extends \app\common\controller\Api
{
    protected $noNeedLogin = ['*'];
    /**
     * 支付成功，仅供开发测试
     */
    public function tiezinotifyx()
    {
        $paytype = $this->request->param('paytype');
        $buy_post_num = $this->request->param('postnum');
        $userid = $this->request->param('userid');

        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];
            db('user')->where('id',$userid)->setInc('buy_post_num',$buy_post_num);

            //你可以在此编写订单逻辑
        } catch (Exception $e) {
            file_put_contents('tiezierr.txt',$e->getMessage());
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }
    }
    /**
     * 支付成功，仅供开发测试
     */
    public function rechargenotifyx()
    {
        $paytype = $this->request->param('paytype');
        $money = $this->request->param('money');
        $userid = $this->request->param('userid');
        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];
            db('user')->where('id',$userid)->setInc('money', $money);
            db('recharge_flow')->where('flow_sn',$out_trade_no)->update(['status'=>'2']);
            //你可以在此编写订单逻辑
        } catch (Exception $e) {
            file_put_contents('rechargeerr.txt',$e->getMessage());
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }
    }
    /**
     * 支付返回，仅供开发测试
     */
    public function returnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }
    /**
     * 支付返回，仅供开发测试
     */
    public function rechargereturnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }


    /**
     * 支付成功，仅供开发测试
     */
    public function orderpaynotifyx()
    {
        $paytype = $this->request->param('paytype');
        $userid = $this->request->param('userid');
        $orderid = $this->request->param('orderid');
        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];
            $oModel = new Order();
            $orderInfo = $oModel->where([
                'id' => ['IN', $orderid],
                'user_id' => $userid
            ])->select();
            $count = $oModel->where([
                'id' => ['IN', $orderid],
                'user_id' => $userid
            ])->select();
            $cnums = count(explode(',', $orderid));

            if (count($count) !== $cnums) $this->error('订单参数补全!');
            $total_price = 0;
            $saveData = [];
            foreach ($orderInfo as $k => $value) {
                $saveData[$k] = [
                    'id' => $value['id'],
                    'status' => '3',
                    'pay_time' => time(),
                    'updatetime' => time(),
                    'pay_type' => $paytype == 'alipay' ? '2' : '1',
                    'pay_status' => '2'
                ];
            }
            $res = $oModel->saveAll($saveData);
            //你可以在此编写订单逻辑
        } catch (Exception $e) {
            file_put_contents('orderpayerr.txt',$e->getMessage());
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }
    }
    /**
     * 支付返回，仅供开发测试
     */
    public function orderpayreturnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }
    /**
     * 支付成功，仅供开发测试
     */
    public function ruzhunotifyx()
    {
        $paytype = $this->request->param('paytype');
        $userid = $this->request->param('userid');
        $apply_num = $this->request->param('applynum');//入驻号码
        $apply_name = $this->request->param('applyname');//入驻昵称
        $real_name = $this->request->param('realname');//真实姓名
        $mobile = $this->request->param('mobile');//真实手机号
        file_put_contents('ruzhu1.txt',$apply_name);
        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $data = [];
            $data['user_id'] = $userid;
            $data['apply_num'] = $apply_num;
            $data['apply_name'] = $apply_name;
            $data['real_name'] = $real_name;
            $data['mobile'] = $mobile;
            $data['createtime'] = time();
            $data['status'] = '1';
            db('apply')->insert($data);
            //你可以在此编写订单逻辑
        } catch (Exception $e) {
            file_put_contents('orderpayerr.txt',$e->getMessage());
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }


    }
    /**
     * 支付返回，仅供开发测试
     */
    public function ruzhureturnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }
}