<?php

namespace app\api\controller;

use app\api\validate\PostValidate;
use app\common\model\Comment;
use app\common\model\Config;
use app\common\model\Post as PostModel;
use app\common\controller\Api;
use app\common\model\PostType;
use app\common\model\Report;
use app\common\model\UserCollect;
use app\common\model\UserLike;
use app\common\model\UserReport;
use think\Db;
use think\Exception;
use think\Request;

class Post extends Api
{
    protected $noNeedLogin = ['type_list', 'index', 'detail'];
    protected $noNeedRight = ['*'];

    /**
     * 获取帖子类型列表
     * @param Request $request
     * @param PostType $ptModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function type_list(Request $request, PostType $ptModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.type_list');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $field = 'id,name,image';
            if($pid == '0'){
                $where = ['pid' => 0];
            }elseif($pid == '1'){
                $where = ['pid' => 0, 'is_show' => 1];
            }elseif($pid == '2'){
                $where = ['pid' => ['<>', 0]];
            }
            $pt = $ptModel->where($where)
                ->order('weight desc')
                ->field($field)
                ->select();
            if($pt && $pid == '0'){
                foreach ($pt as &$item){
                    $item['second_list'] = $ptModel->where('pid', $item['id'])
                        ->order('weight desc')
                        ->field($field)
                        ->select();
                }
            }
            $this->success('获取成功', $pt);
        }
    }

    /**
     * 通过父级ID获取二级分类
     * @param Request $request
     * @param PostType $ptModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function get_list2(Request $request, PostType $ptModel)
    {
        $resData = $request->request();
        extract($resData);
        $field = 'id,name,image';
        $where = ['pid' => $pid];
        $pt = $ptModel->where($where)
            ->order('weight desc')
            ->field($field)
            ->select();
        $this->success('获取成功', $pt);
    }

    /**
     * 通过类型获取帖子列表
     * @param Request $request
     * @param PostModel $pModel
     * @throws \think\exception\DbException
     */
    public function index(Request $request, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.index');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            if(!$page) $page = 1;
            if(!$pageSize) $pageSize = 10;
            $field = 'id,user_id,name,content,images,createtime,updatetime,address';
            $data = $pModel->with('user')
                ->where('type_id', $type_id)
                ->field($field)
                ->order('id DESC')
                ->paginate($pageSize);
            if($data){
                $data = $data->toArray()['data'];
                foreach ($data as &$item){
                    $item['images'] = explode(',', $item['images']);
                    $item['images'] = $item['images'][0];
                }
            }
            $this->success('获取成功', $data);
        }
    }

    /**
     * 发布帖子
     * @param Request $request
     * @param PostModel $pModel
     */
    public function add(Request $request, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.add');
        if($result !== true){
            $this->error($result);
        }else{
            $max_post_num = Config::where('name', 'max_post_num')->value('value');  //用户每日最大发帖次数
            $user = $this->auth->getUser();
            $resData['user_id'] = $user->id;
            if($user->post_num >= $max_post_num){
                //次数多了
                if($user->buy_post_num > 0){
                    $res = $pModel->allowField(true)->save($resData);
                    $user->buy_post_num -= 1;
                    $user->save();
                }else{
                    return json(['code' => 201]);
                }
            }else{
                $res = $pModel->allowField(true)->save($resData);
                $user->post_num += 1;
                $user->save();
            }
            $res ? $this->success('发布成功') : $this->error('发布失败');
        }
    }
    //获取发帖区域列表
    public function get_post_area(){
        $list = db('topic_area')->select();

        foreach ($list as &$value){
            $value['address'] = $value['sheng'] . $value['shi'] . $value['qu'];
        }
        $this->success('ok',$list);
    }
    /**
     * 帖子详情
     * @param Request $request
     * @param PostModel $pModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail(Request $request, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.detail');
        if($result !== true){
            $this->error($result);
        }else{
            $data = $pModel->with(['user','comment' => function($query){
                    return $query->with('user');
                }])
                ->where('id',$resData['id'])
                ->find();
            $data->images = explode(',', $data->images);
            foreach ($data->comment as $item){
                $item->username = $item->user->username;
                $item->avatar = $item->user->avatar;
                unset($item->user_id);
                unset($item->post_id);
                unset($item->user);
                unset($item->updatetime);
            }
            $data->is_like = 0;
            $data->is_collect = 0;
            $token = input('token');
            if($token){
                $user = $this->auth->getUser();
                $is_like = UserLike::where('post_id', $resData['id'])->where('user_id', $user->id)->find();
                if($is_like)$data->is_like = 1;
                $is_collect = UserCollect::where('post_id', $resData['id'])->where('user_id', $user->id)->find();
                if($is_collect)$data->is_collect = 1;
            }
            $data->comment_num = Comment::where('post_id', $resData['id'])->count();
            $this->success('获取成功', $data);
        }
    }

    /**
     * 点赞
     * @param Request $request
     * @param UserLike $ulModel
     * @param PostModel $pModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function add_like(Request $request, UserLike $ulModel, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.operate');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $user = $this->auth->getUser();
            $judge = $ulModel->where(['user_id' => $user->id, 'post_id' => $id])->find();
            if($judge) $this->error('您已对此贴进行了点赞，请不要重复点赞');  //重复点赞判定
            $ulModel->save(['user_id' => $user->id, 'post_id' => $id]); //添加点赞记录
            $post = $pModel->find($id);
            $post->like_num += 1;
            $post->save();  //增加帖子的点赞数量
            $this->success('点赞成功');
        }
    }

    /**
     * 取消点赞
     * @param Request $request
     * @param UserLike $ulModel
     * @param PostModel $pModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function cancel_like(Request $request, UserLike $ulModel, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.operate');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $user = $this->auth->getUser();
            $judge = $ulModel->where(['user_id' => $user->id, 'post_id' => $id])->find();
            if(!$judge) $this->error('您还未对此贴进行点赞，不可取消点赞');  //重复取赞判定
            $judge->delete(); //删除点赞记录
            $post = $pModel->find($id);
            $post->like_num -= 1;
            $post->save();  //减少帖子的点赞数量
            $this->success('取消点赞成功');
        }
    }

    /**
     * 收藏
     * @param Request $request
     * @param UserCollect $ucModel
     * @param PostModel $pModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function add_collect(Request $request, UserCollect $ucModel, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.operate');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $user = $this->auth->getUser();
            $judge = $ucModel->where(['user_id' => $user->id, 'post_id' => $id])->find();
            if($judge) $this->error('您已对此贴进行了收藏，请不要重复收藏');  //重复收藏判定
            $ucModel->save(['user_id' => $user->id, 'post_id' => $id]); //添加收藏记录
            $post = $pModel->find($id);
            $post->collect_num += 1;
            $post->save();  //增加帖子的收藏数量
            $this->success('收藏成功');
        }
    }

    /**
     * 取消收藏
     * @param Request $request
     * @param UserCollect $ucModel
     * @param PostModel $pModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function cancel_collect(Request $request, UserCollect $ucModel, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.operate');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $user = $this->auth->getUser();
            $judge = $ucModel->where(['user_id' => $user->id, 'post_id' => $id])->find();
            if(!$judge) $this->error('您还未对此贴进行收藏，不可取消收藏');  //重复取消收藏判定
            $judge->delete(); //删除收藏记录
            $post = $pModel->find($id);
            $post->collect_num -= 1;
            $post->save();  //减少帖子的收藏数量
            $this->success('收藏成功');
        }
    }

    /**
     * 获取我收藏的帖子列表
     * @param Request $request
     * @param UserCollect $ucModel
     * @throws \think\exception\DbException
     */
    public function collect_list(Request $request, UserCollect $ucModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.collect_list');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            if(!$page) $page = 1;
            if(!$pageSize) $pageSize = 10;
            $user = $this->auth->getUser();
            $collect_list = $ucModel->where('user_id', $user->id)
                ->with(['post' => function($query){
                    return $query->with('user');
                }])
                ->paginate($pageSize);
            $data = [];
            if($collect_list){
                $collect_list = $collect_list->toArray()['data'];
                foreach ($collect_list as $item){
                    $data[] = $item['post'];
                }
                foreach ($data as &$item){
                    $item['images'] = explode(',', $item['images']);
                    $item['images'] = $item['images'][0];
                }
            }
            $this->success('获取成功', $data);
        }
    }

    /**
     * 批量删除收藏
     * @param Request $request
     * @param UserCollect $ucModel
     * @param PostModel $pModel
     */
    public function del_collect(Request $request, UserCollect $ucModel, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.del_collect');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            $user = $this->auth->getUser();
            Db::startTrans();
            try {
                $ids = explode(',', $ids);
                foreach ($ids as $id){
                    $collectData = $ucModel->where(['user_id' => $user->id, 'post_id' => $id])->find();
                    $post = $pModel->find($collectData->post_id);
                    $post->collect_num -= 1;
                    $post->save();  //减少帖子的收藏数量
                    $collectData->delete($id);   //删除收藏记录
                }
                Db::commit();
            }catch (Exception $e){
                Db::rollback();
                $this->error($e->getMessage());
            }
            $this->success('取消收藏成功');
        }
    }

    /**
     * 添加评论
     * @param Request $request
     * @param Comment $cModel
     */
    public function add_comment(Request $request, Comment $cModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.add_comment');
        if($result !== true){
            $this->error($result);
        }else{
            $user = $this->auth->getUser();
            $resData['user_id'] = $user->id;
            $cModel->allowField(true)->save($resData);
            $this->success('评论成功');
        }
    }

    /**
     * 获取举报类型列表
     * @param Request $request
     * @param Report $rModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function report_list(Request $request, Report $rModel)
    {
        $this->success('获取成功', $rModel->field('id,name')->order('weight desc')->select());
    }

    /**
     * 添加举报
     * @param Request $request
     * @param UserReport $urModel
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function add_report(Request $request, UserReport $urModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.add_report');
        if($result !== true){
            $this->error($result);
        }else{
            $user = $this->auth->getUser();
            $resData['user_id'] = $user->id;
            $judge = $urModel->where(['user_id' => $user->id, 'post_id' => $resData['post_id']])->find();
            if($judge) $this->error('您已对该帖子进行了一次举报，请勿重复举报');
            $res = $urModel->allowField(true)->save($resData);
            $res ? $this->success('举报成功') : $this->error('举报失败');
        }
    }

    public function edit(Request $request)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.edit');
        if($result !== true){
            $this->error($result);
        }else{

        }
    }

    /**
     * 删除帖子
     * @param Request $request
     * @param PostModel $pModel
     */
    public function delete(Request $request, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.delete');
        if($result !== true){
            $this->error($result);
        }else{
            $res = $pModel->where('id', $resData['id'])->delete();
            $res ? $this->success('删除成功') : $this->error('删除失败');
        }
    }

    /**
     * 获取我发布的帖子列表
     * @param Request $request
     * @param PostModel $pModel
     * @throws \think\exception\DbException
     */
    public function my_list(Request $request, PostModel $pModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, PostValidate::class . '.my_list');
        if($result !== true){
            $this->error($result);
        }else{
            extract($resData);
            if(!$page) $page = 1;
            if(!$pageSize) $pageSize = 10;
            $user = $this->auth->getUser();
            $data = $pModel->where('user_id', $user->id)->paginate($pageSize);
            if($data){
                $data = $data->toArray()['data'];
                foreach ($data as &$item){
                    $item['images'] = explode(',', $item['images']);
                    $item['images'] = $item['images'][0];
                }
            }
            $this->success('获取成功', $data);
        }
    }
}