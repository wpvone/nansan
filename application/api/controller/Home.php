<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Evaluate;
use app\common\model\RechargeFlow;
use app\common\model\CashFlow;
use app\common\model\UserBank;
use think\Db;
use think\Exception;
use think\Request;
use app\common\model\UserHistory;
use app\common\model\UserFavo;
use app\common\model\UserBusiness;


/**
 * Class Home
 * @package app\api\controller
 *
 * 个人中心, 充值提现, 浏览记录, 收藏列表等
 */
class Home extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     *
     * 用户充值
     *
     * @param int $type 充值方式:1=微信,2=支付宝,3=银行卡,4=其他
     * @param float $money 金额
     */
    public function reCharge(RechargeFlow $rcModel)
    {
        $money = input('money');
        $type = input('type');
        $user = $this->auth->getUserinfo();
        $res = $rcModel::addInfo($user['id'], $type, $money);
        return json($res);
    }

    /**
     * 获取充值列表
     * $page 1
     * $limit 8
     */
    public function getRcList(Request $request, RechargeFlow $rcModel)
    {
        $param = $request->param();
        $param['page'] = $param['page'] ?? 1;
        $param['limit'] = $param['limit'] ?? 8;
        $user = $this->auth->getUserinfo();
        $ret = $rcModel::getInfo($user['id'], $param);
        $this->success('ok', $ret);
    }

    /**
     * 提现申请提交
     *
     * @param int $user_bank_id 用户银行表id
     * @param float $money 金额
     * @param string $password 密码
     */
    public function cash(CashFlow $cModel)
    {
        $password = input('password')?input('password'):$this->error('请输入密码');
        $money = input('money')?input('money'):$this->error('请输入金额');
        $bankName = input('bank_name')?input('bank_name'):$this->error('请输入银行名');
        $bank_num = input('bank_num')?input('bank_num'):$this->error('请输入银行卡号');
        $bank_name_zhi = input('bank_name_zhi')?input('bank_name_zhi'):$this->error('请输入对应支行');
        $card_name = input('card_name')?input('card_name'):$this->error('请输入持卡人姓名');
        $user = $this->auth->getUser();
        if (!$user->pay_password) $this->error('请先设置支付密码', [], -1);
        $password = md5(md5($password) . $user->salt);
        if ($password !== $user->pay_password) $this->error('支付密码错误!');
        if ($user->money < $money) $this->error('提现金额不能大于自己余额!');
        $bankInfo = [
            'bank_name' => $bankName,
            'bank_num' => $bank_num,
            'bank_name_zhi' => $bank_name_zhi,
            'card_name' => $card_name,
        ];
        Db::startTrans();
        try {
            $ret = $cModel::addInfo($user->id, $money, $bankInfo, '申请提现');
            $oldMoney = $user->money;
            $newMoney  = $oldMoney - $money;
            number_format($newMoney, 2);
            $user->money = $newMoney;
            $user->updatetime = time();
            $user->save();
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            $this->error('提现失败!');
        }
       $this->success('等待后台审核!');
    }

    public function get_consp_flow()
    {
        $page = input('page') ? input('page') : 1;
        $pageSize = input('pageSize') ? input('pageSize') : 10;
        $user = $this->auth->getUser();
        $data = db('consp_flow')->where('user_id', $user->id)->paginate($pageSize);
        if($data){
            $data = $data->toArray()['data'];
            foreach ($data as $key => $item){
                $data[$key]['createtime'] = date('Y-m-d H:i:s', $item['createtime']);
            }
        }

        $this->success('获取成功', $data);
    }

    /**
     * 获取提现流水列表
     * $page 1
     * $limit 8
     */
    public function getCashList(Request $request, CashFlow $cModel)
    {
        $param = $request->param();
        $param['page'] = $param['page'] ?? 1;
        $param['limit'] = $param['limit'] ?? 8;
        $user = $this->auth->getUserinfo();
        $ret = $cModel::getInfo($user['id'], $param);
        $this->success('ok', $ret);
    }


    /**
     * 历史浏览
     *
     * page
     * limit
     */
    public function history(UserHistory $uhModel)
    {
        $user = $this->auth->getUserinfo();
        $page = input('page')?? 1;
        $limit = input('limit')?? 4;
        $data = $uhModel::getList($user['id'], $page, $limit);
        $this->success('ok', $data);
    }

    /**
     * 删除浏览记录
     * $ids
     */
    public function deleteHistory(UserHistory $uhModel)
    {
        $ids = input('ids') ?? $this->error('参数错误!');
        $ids = trim($ids, ',');
        $res = $uhModel::destroy($ids, true);
        $this->success('删除成功!', $res);
    }

    /**
     * 收藏列表
     * page
     * limit
     */
    public function favoList(UserFavo $ufModel)
    {
        $user = $this->auth->getUserinfo();
        $page = input('page')?? 1;
        $limit = input('limit')?? 4;
        $data = $ufModel::getList($user['id'], $page, $limit);
        $this->success('ok', $data);
    }

    /**
     * 收藏取消收藏
     *
     * @pram int $product_id 商品ID
     */
    public function favo(UserFavo $ufModel)
    {
        $user = $this->auth->getUserinfo();
        $product_id = $this->request->param('product_id') ?? $this->error('参数缺少!');
        $this->success('操作成功!', $ufModel::setFavo($user['id'], $product_id));
    }

    /**
     * 批量删除收藏
     * $ids
     */
    public function deleteFavo(UserFavo $ufModel)
    {
        $ids = input('ids') ?? $this->error('参数错误!');
        $ids = trim($ids, ',');
        $res = $ufModel::destroy($ids, true);
        $this->success('删除成功!', $res);
    }

    /**
     * 删除收藏商铺
     * @param str $ids
     */
    public function delBusinessFavo()
    {
        $ids = input('ids', '')?? $this->error('请选择列!');
        $ids = trim($ids, ',');

        $UBModel = new UserBusiness();
        $res = $UBModel::destroy($ids, true);

        $this->success('删除成功!', $res);


    }


    /**
     * 用户评价订单信息列表
     */
    public function myEvaluate()
    {
        $page = input('page') ?? 1;
        $limit = input('limit')??4;
        $user = $this->auth->getUserinfo();
        $eModel = new Evaluate();
        $data = $eModel->with([
            'order' =>function($q) {
                $q->with(['orderProduct']);
            },
        ])->where('user_id', $user['id'])
            ->order('id desc')
            ->page($page, $limit)
            ->select();

        if ($data) {
            foreach ($data as $k => $v)
            {
                if(isset($data[$k]['order']['address_json'])){
                     unset($data[$k]['order']['address_json']);
                }
            }
        }

        $this->success('ok', $data);
    }

    /**
     * 申请商家入驻
     * name
     * mobile
     * zheng
     * fan
     * license 商户营业执照
     */
    public function apply()
    {
        $name = input('name', '');
        $mobile = input('mobile', '');
        $zheng = input('zheng', '');
        $fan = input('fan', '');
        $license = input('license', '');
        if (!$name || !$mobile || !$zheng || !$fan || !$license) $this->error('请补全信息!');

        $user = $this->auth->getUser();
        if ($user->apply_status == 2) $this->error('后台正在审核,请勿重复提交!');
        if ($user->apply_status == 3) $this->error('您已的申请已经通过!');
        Db::startTrans();
        try {
            $res = Db::name('apply')->insert([
                'user_id' => $user['id'],
                'name' => $name,
                'mobile' => $mobile,
                'idcard_zheng_image' => $zheng,
                'idcard_fan_image' => $fan,
                'license_image' => $license,
                'status' => '1'
            ]);
            $user->apply_status = 2;
            $user->save();
            Db::commit();
        }catch (Exception $e){
            Db::rollback();
            $this->error('提交失败,请重试!');
        }
        if ($res) {
            $this->success('提交成功!');
        }else{
            $this->error('提交失败,请重试!');
        }
    }

}

