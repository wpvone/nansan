<?php

namespace app\api\controller;

use app\api\validate\ApplyValidate;
use app\common\controller\Api;
use app\common\model\Config;
use think\Request;
use app\common\model\Apply as ApplyModel;

class Apply extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];

    /**
     * 获取便民电话列表
     * @param Request $request
     * @param ApplyModel $aModel
     * @throws \think\exception\DbException
     */
    public function index(Request $request, ApplyModel $aModel)
    {
        $resData = $request->request();

        $result = $this->validate($resData, ApplyValidate::class . '.index');
        if($result !== true){
            $this->error($result);
        }else {
            extract($resData);
            if(!$page) $page = 1;
            if(!$pageSize) $pageSize = 10;
            $data = $aModel->where('status', 2)->paginate($pageSize);
            if($data) $data = $data->toArray()['data'];
            $this->success('获取成功', $data);
        }
    }

    /**
     * 获取入驻金额
     */
    public function get_price()
    {
        $data = Config::where('name', 'apply_price')->value('value');
        $this->success('获取成功', $data);
    }

    /**
     * 申请入驻
     * @param Request $request
     * @param ApplyModel $aModel
     */
    public function add(Request $request, ApplyModel $aModel)
    {
        $resData = $request->request();
        $result = $this->validate($resData, ApplyValidate::class . '.add');
        if($result !== true){
            $this->error($result);
        }else{
            $user = $this->auth->getUser();
            if(!$user->pay_password) $this->error('未设置支付密码');
            if(md5(md5($resData['pay_password']) . $user->salt) !== $user->pay_password) $this->error('支付密码错误');
            if($user->money < $resData['money']) $this->error('余额不足');
            $resData['user_id'] = $user->id;
            $aModel->allowField(true)->save($resData);
            $user->apply_status = 2;
            $user->money -= $resData['money'];
            $user->save();
            $this->success('申请成功');
        }
    }
}