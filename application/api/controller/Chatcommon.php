<?php


namespace app\api\controller;


class Chatcommon extends \app\common\controller\Api
{
    protected $noNeedRight = ['*'];

    /**
     * 新消息提醒/不提醒
     */
    public function news_notice(){
        $user = $this->auth->getUser();
        if ($user->news_notice == 1){
            $user->news_notice = '2';
        }else{
            $user->news_notice = '1';
        }
        $user->save();
        $this->success('设置成功');
    }

    /**
     * 获取聊天 投诉举报好友原因列表
     */
    public function get_chat_why(){
        $list = db('chat_complaint_why')
            ->select();
        $this->success('ok',$list);
    }

    /**
     * 投诉举报好友
     */
    public function complaint_friend(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请先指定好友');
        $why = input('why') ? input('why') : $this->error('请指定投诉原因');
        $data = [];
        $data['user_id'] = $user->id;
        $data['friend_id'] = $friend_id;
        $res = db('chat_complaint')->where($data)->whereTime('createtime','d')->find();
        if ($res){
            $this->error('今日已经投诉举报该用户,请等待管理员处理');
        }
        $data['why'] = $why;
        $data['createtime'] = time();
        $res = db('chat_complaint')->insert($data);
        if ($res){
            $this->success('投诉举报成功');
        }else{
            $this->error('投诉举报失败');
        }
    }

    /**
     * 改变朋友圈背景图
     */
    public function change_circle(){
        $image = input('image') ? input('image') : $this->error('请上传背景图');
        $user = $this->auth->getUser();
        $user->friend_image = $image;
        $user->save();
        $this->success('设置成功',$image);
    }
    /**
     * 改变朋友圈背景图
     */
    public function get_circle_image(){
        $user = $this->auth->getUser();
        $this->success('ok',$user->friend_image);
    }

    /**
     * 获取客服信息
     */
    public function get_service_info(){
        $service = db('user')->find(1);
        $data = [];
        $data['id'] = 1;
        $data['nickname'] = $service['nickname'];
        $data['avatar'] = $service['avatar'];
        $this->success('ok',$data);
    }

    /**
     * 用户是否为自己的好友
     */
    public function is_friend(){
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定用户');
        $is_friend = db('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();
        if (!$is_friend){
            if($friend_id == $user->id){
                $this->success('ok');
            }
            $this->error('no');
        }else{
            $this->success('ok');
        }
    }

    /**
     * 用户是否已经是群成员
     */
    public function is_member(){
        $user = $this->auth->getUser();
        $group_id = input('group_id') ? input('group_id'): $this->error('请指定群组');
        $is_member = db('chat_groupuser')
            ->where('user_id',$user->id)
            ->where('chat_group_id',$group_id)
            ->find();
        if (!$is_member){
            $this->success('ok',false);
        }else{
            $this->success('ok',true);
        }
    }
}