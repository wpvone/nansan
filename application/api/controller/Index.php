<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Config;
use app\common\model\Question;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function index()
    {
        $this->success('请求成功');
    }

    public function get_question()
    {
        $this->success('获取成功', Question::field('id,question')->select());
    }

    public function get_user_agreement()
    {
        $data = Config::where('name', 'user_agreement')->value('value');
        $this->success('获取成功', $data);
    }

    public function get_privacy_policy()
    {
        $data = Config::where('name', 'privacy_policy')->value('value');
        $this->success('获取成功', $data);
    }

    public function get_post_num()
    {
        $this->success('获取成功', db('post_num')->select());
    }
    public function get_banner(){
        $list = db('banner')
            ->select();
        $this->success('ok',$list);
    }
}
