<?php

namespace addons\epay\controller;

use addons\epay\library\Service;
use fast\Random;
use think\addons\Controller;
use Exception;
use think\Db;
use GatewayClient\Gateway;
use app\api\controller\Chatmoney;

/**
 * 微信支付宝插件首页
 *
 * 此控制器仅用于开发展示说明和体验，建议自行添加一个新的控制器进行处理返回和回调事件，同时删除此控制器文件
 *
 * Class Index
 * @package addons\epay\controller
 */
class Index extends Controller
{

    protected $layout = 'default';

    protected $config = [];

    public function _initialize()
    {
        parent::_initialize();
        if (!config("app_debug")) {
            $this->error("仅在开发环境下查看");
        }
    }

    public function index()
    {
        $this->view->assign("title", "微信支付宝整合插件");
        return $this->view->fetch();
    }

    /**
     * 体验，仅供开发测试
     */
    public function experience()
    {
        $amount = $this->request->request('amount');
        $type = $this->request->request('type');
        $method = $this->request->request('method');

        if (!$amount || $amount < 0) {
            $this->error("支付金额必须大于0");
        }

        if (!$type || !in_array($type, ['alipay', 'wechat'])) {
            $this->error("支付类型不能为空");
        }

        //订单号
        $out_trade_no = date("YmdHis") . mt_rand(100000, 999999);

        //订单标题
        $title = '测试订单';

        //回调链接
        $notifyurl = $this->request->root(true) . '/addons/epay/index/notifyx/paytype/' . $type;
        $returnurl = $this->request->root(true) . '/addons/epay/index/returnx/paytype/' . $type . '/out_trade_no/' . $out_trade_no;

        $response = Service::submitOrder($amount, $out_trade_no, $type, $title, $notifyurl, $returnurl, $method);

        return $response;
    }

    /**
     * 支付成功，仅供开发测试
     */
    public function notifyx()
    {
        $paytype = $this->request->param('paytype');
        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];

            //你可以在此编写订单逻辑
        } catch (Exception $e) {
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }
    }

    /**
     * 支付返回，仅供开发测试
     */
    public function returnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }
    /**
     * 发红包
     */
    public function parkey_experience()
    {
        $num = input('num') ? input('num') : $this->error('请输入数量');
        $money = input('money') ? input('money') : $this->error('请输入金额');
        $type = input('type') ? input('type') : $this->error('请指定红包类型');//1=普通红包,2=拼手气红包
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请指定好友或者群ID');
        $chat_type = input('chat_type') ? input('chat_type') : $this->error('聊天类型');//1=好友,2=群聊
        $pay_type = input('pay_type') ? input('pay_type') : $this->error('支付类型');//1=微信,2=支付宝
        $note = input('note') ? input('note'):'恭喜发财,大吉大利';
        if (!is_numeric($num) || $num < 1){
            $this->error('请指定正确的数量');
        }
        if (!is_numeric($money) || $money < 0.01 * $num){
            $this->error('请输入正确的金额,每包最低0.01元');
        }
        if (!in_array($chat_type,['1','2'])){
            $this->error('聊天类型指定错误');
        }
        $user = $this->auth->getUser();
        if ($chat_type == 1){
            //好友
            $is_friend = db('chat_friend')//好友列表
            ->where('user_id',$user->id)//自己
            ->where('friend_id',$friend_id)//好友
            ->find();
            if (!$is_friend){
                $this->error('好友ID指定错误');
            }
        }else{
            //群
            $is_member = db('chat_groupuser')//群用户
            ->where('user_id',$user->id)//自己
            ->where('chat_group_id',$friend_id)//群
            ->find();
            if (!$is_member){
                $this->error('群ID指定错误');
            }
        }
        $parkeyModel = new Chatmoney();
        if ($type == 1){
            //普通红包
            $amount = $money * $num;
        }elseif ($type == 2){
            //拼手气红包
            //比较难
            $this->error('先发普通红包哈');
            //
            //补
            //
        }else{
            $this->error('请指定正确的红包类型');
        }

        if ($pay_type == 1){
            //微信
            $type = 'wechat';
        }elseif ($pay_type==2){
            //支付宝
            $type = 'alipay';
        }else{
            $this->error('付款方式指定错误');
        }
        $method = 'app';
        if (!$amount || $amount < 0) {
            $this->error("支付金额必须大于0");
        }

        //订单号
        $out_trade_no = 'P'.date("YmdHis") . mt_rand(100000, 999999);

        $three_parket = [];
        $three_parket['order_num'] = $out_trade_no;
        $three_parket['user_id'] = $user->id;
        $three_parket['num'] = $num;
        $three_parket['money'] = $money;
        $three_parket['type'] = $type;
        $three_parket['friend_id'] = $friend_id;
        $three_parket['chat_type'] = $chat_type;
        $three_parket['pay_type'] = $pay_type;
        $three_parket['note'] = $note;
        $three_parket['createtime'] = time();
        $resss = db('chat_three_parket')->insert($three_parket);
        if (!$resss){
            $this->error('红包发起失败');
        }

        //订单标题
        $title = '红包';

        //回调链接
        $notifyurl = $this->request->root(true) . '/addons/epay/index/parket_notifyx/paytype/' . $type;
        $returnurl = $this->request->root(true) . '/addons/epay/index/parket_returnx/paytype/' . $type . '/out_trade_no/' . $out_trade_no;
        $amount = 0.01;
        $response = Service::submitOrder($amount, $out_trade_no, $type, $title, $notifyurl, $returnurl, $method);
        return $response;
    }

    /**
     * 支付成功，仅供开发测试
     */
    public function parket_notifyx()
    {
        file_put_contents('redin.txt',date('Y-m-d H:i:s',time()));
        $paytype = $this->request->param('paytype');
        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];
            $three_parket = db('chat_three_parket')->where('order_num',$out_trade_no)->find();
            if ($three_parket){
                db('chat_three_parket')->where('id',$three_parket['id'])->update(['status'=>'2','updatetime'=>time()]);
                $send_parket = [];
                $send_parket['user_id'] = $three_parket['user_id'];//发包人
                $send_parket['friend_id'] = $three_parket['friend_id'];//好友/群ID
                $send_parket['packet_type'] = '1';//普通红包
                $send_parket['chat_type'] = $three_parket['chat_type'];//聊天类型
                $send_parket['money'] = $three_parket['num'] * $three_parket['money'];//红包总金额
                $send_parket['packet_num'] = $three_parket['num'];//红包总个数
                $send_parket['wait_num'] = $three_parket['num'];//带领取个数
                $send_parket['createtime'] = time();//发包时间
                $send_parket['note'] = $three_parket['note'];//备注说明
                $parket_id = db('chat_packet')->insertGetId($send_parket);//红包ID
                $recive = [];
                for ($i = 0; $i < $three_parket['num']; $i++){
                    $recive[$i]['chat_parket_id'] = $parket_id;//红包id
                    $recive[$i]['money'] = $three_parket['money'];//金额
                    $recive[$i]['createtime'] = time();//发包时间
                }
                $res = db('chat_rparket')->insertAll($recive);//插入带领取的分配包

                if ($three_parket['chat_type'] == 1){
//                    //好友
                    $message_arr = [];
                    $message_arr['type'] = 'user';
                    $message_arr['categorytype'] = 'static';
                    $message_arr['from_id'] = $three_parket['user_id'];
                    $message_arr['to_id'] = $three_parket['friend_id'];
                    $message_arr['msg'] = [];
                    $message_arr['msg']['id'] = 222222222;
                    $message_arr['msg']['time'] = time();
                    $message_arr['msg']['type'] = 'redEnvelope';
                    $send_userinfo = db('user')->find($three_parket['user_id']);//发红包人的信息
                    $message_arr['msg']['userinfo'] = [];
                    $message_arr['msg']['userinfo']['uid'] = $three_parket['user_id'];
                    $message_arr['msg']['userinfo']['username'] = $send_userinfo['nickname'];
                    $message_arr['msg']['userinfo']['face'] = $send_userinfo['avatar'];
                    $message_arr['msg']['noselfinfo'] = [];
                    $get_userinfo = db('user')->find($three_parket['friend_id']);//收红包人的信息
                    $message_arr['msg']['noselfinfo']['uid'] = $three_parket['friend_id'];
                    $message_arr['msg']['noselfinfo']['username'] = $get_userinfo['nickname'];
                    $message_arr['msg']['noselfinfo']['face'] = $get_userinfo['avatar'];
                    $message_arr['msg']['content'] = [];
                    $message_arr['msg']['content']['blessing'] = $three_parket['note'];
                    $message_arr['msg']['content']['rid'] = $parket_id;
                    $message_arr['msg']['content']['isReceived'] = false;
                    $message_arr['msg']['content']['money'] = $send_parket['money'];
                    $message_arr['msg']['content']['redenvelopnumber'] = $three_parket['num'];
                    $content = json_encode($message_arr,JSON_UNESCAPED_UNICODE);

                    Gateway::sendToUid($three_parket['user_id'],$content);
                    Gateway::sendToUid($three_parket['friend_id'],$content);

                }else{
                    //群
                    $message_arr = [];
                    $message_arr['type'] = 'user';
                    $message_arr['categorytype'] = 'group';
                    $message_arr['from_id'] = $three_parket['user_id'];
                    $message_arr['group_id'] = $three_parket['friend_id'];
                    $message_arr['msg'] = [];
                    $message_arr['msg']['id'] = 222222222;
                    $message_arr['msg']['time'] = time();
                    $message_arr['msg']['type'] = 'redEnvelope';
                    $send_userinfo = db('user')->find($three_parket['user_id']);//发红包人的信息
                    $message_arr['msg']['userinfo'] = [];
                    $message_arr['msg']['userinfo']['uid'] = $three_parket['user_id'];
                    $message_arr['msg']['userinfo']['username'] = $send_userinfo['nickname'];
                    $message_arr['msg']['userinfo']['face'] = $send_userinfo['avatar'];
                    $message_arr['msg']['to_group_info'] = [];
                    $get_userinfo = db('chat_group')->find($three_parket['friend_id']);//收红包人的信息
                    $message_arr['msg']['to_group_info']['uid'] = $three_parket['friend_id'];
                    $message_arr['msg']['to_group_info']['groupname'] = $get_userinfo['name'];
                    $message_arr['msg']['to_group_info']['face'] = $get_userinfo['avatar'];
                    $message_arr['msg']['content'] = [];
                    $message_arr['msg']['content']['blessing'] = $three_parket['note'];
                    $message_arr['msg']['content']['rid'] = $parket_id;
                    $message_arr['msg']['content']['isReceived'] = false;
                    $message_arr['msg']['content']['money'] = $send_parket['money'];
                    $message_arr['msg']['content']['redenvelopnumber'] = $three_parket['num'];
                    $content = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
                    Gateway::sendToGroup($three_parket['friend_id'],$content);
                }
            }

            //你可以在此编写订单逻辑

        } catch (Exception $e) {
            file_put_contents('rederror.txt',$e->getMessage());
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }
    }

    /**
     * 支付返回，仅供开发测试
     */
    public function parket_returnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }
        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }
    /**
     * 转账
     */
    public function transfer_experience()
    {
        $user = $this->auth->getUser();
        $friend_id = input('friend_id') ? input('friend_id') : $this->error('请先指定好友');
        $money = input('money') ? input('money') : $this->error('请输入转账金额');
        $note = input('note') ? input('note') : '';
        $pay_type = input('pay_type') ? input('pay_type') : $this->error('请指定付款方式');//1=微信,2=支付宝
        $is_friend = db('chat_friend')
            ->where('user_id',$user->id)
            ->where('friend_id',$friend_id)
            ->find();
        if (!$is_friend){
            $this->error('好友指定错误');
        }
        $amount = $money;
        if (!$amount || $amount < 0) {
            $this->error("支付金额必须大于0");
        }
        if ($pay_type == 1){
            //微信
            $type = 'wechat';
        }else{
            //支付宝
            $type = 'alipay';
        }
        //订单号
        $out_trade_no = 'T'.date("YmdHis") . mt_rand(100000, 999999);
        $tranfer = [];
        $tranfer['order_num'] = $out_trade_no;
        $tranfer['user_id'] = $user->id;
        $tranfer['friend_id'] = $friend_id;
        $tranfer['money'] = $money;
        $tranfer['note'] = $note;
        $tranfer['createtime'] = time();
        $res = db('chat_three_transfer')->insert($tranfer);
        if (!$res){
            $this->error('发起转账失败');
        }
        //订单标题
        $title = '转账';
        $method = 'app';
        //回调链接
        $notifyurl = $this->request->root(true) . '/addons/epay/index/transfer_notifyx/paytype/' . $type;
        $returnurl = $this->request->root(true) . '/addons/epay/index/transfer_returnx/paytype/' . $type . '/out_trade_no/' . $out_trade_no;
        $amount = 0.01;
        $response = Service::submitOrder($amount, $out_trade_no, $type, $title, $notifyurl, $returnurl, $method);

        return $response;
    }

    /**
     * 支付成功，仅供开发测试
     */
    public function transfer_notifyx()
    {
        $paytype = $this->request->param('paytype');
        $pay = Service::checkNotify($paytype);
        if (!$pay) {
            echo '签名错误';
            return;
        }
        $data = $pay->verify();
        try {
            $payamount = $paytype == 'alipay' ? $data['total_amount'] : $data['total_fee'] / 100;
            $out_trade_no = $data['out_trade_no'];
            $three_transfer = db('chat_three_transfer')->where('order_num',$out_trade_no)->find();
            if ($three_transfer){
                $tranfer = [];
                $tranfer['user_id'] = $three_transfer['user_id'];
                $tranfer['friend_id'] = $three_transfer['friend_id'];
                $tranfer['money'] = $three_transfer['money'];
                $tranfer['note'] = $three_transfer['note'];
                $tranfer['createtime'] = time();
                $tranfer['status'] = '1';
                $tranfer['overtime'] = time() + 86400;//一天
                $res = db('chat_transfer_money')->insertGetId($tranfer);
                $message_arr = [];
                $message_arr['type'] = 'user';
                $message_arr['categorytype'] = 'static';
                $message_arr['from_id'] = $three_transfer['user_id'];
                $message_arr['to_id'] = $three_transfer['friend_id'];
                $message_arr['msg'] = [];
                $message_arr['msg']['id'] = 222222222;
                $message_arr['msg']['time'] = time();
                $message_arr['msg']['type'] = 'zhuanEnvelope';
                $send_userinfo = db('user')->find($three_transfer['user_id']);//发红包人的信息
                $message_arr['msg']['userinfo'] = [];
                $message_arr['msg']['userinfo']['uid'] = $three_transfer['user_id'];
                $message_arr['msg']['userinfo']['username'] = $send_userinfo['nickname'];
                $message_arr['msg']['userinfo']['face'] = $send_userinfo['avatar'];
                $message_arr['msg']['noselfinfo'] = [];
                $get_userinfo = db('user')->find($three_transfer['friend_id']);//收红包人的信息
                $message_arr['msg']['noselfinfo']['uid'] = $three_transfer['friend_id'];
                $message_arr['msg']['noselfinfo']['username'] = $get_userinfo['nickname'];
                $message_arr['msg']['noselfinfo']['face'] = $get_userinfo['avatar'];
                $message_arr['msg']['content'] = [];
                $message_arr['msg']['content']['zhuan_id'] = $res;
                $message_arr['msg']['content']['money'] = $three_transfer['money'];
                $content = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
                Gateway::sendToUid($three_transfer['user_id'],$content);
                Gateway::sendToUid($three_transfer['friend_id'],$content);
            }
            //你可以在此编写订单逻辑
        } catch (Exception $e) {
        }
        if($paytype == 'alipay'){
            echo 'success';
        }else{
            echo $pay->success();
        }
    }

    /**
     * 支付返回，仅供开发测试
     */
    public function transfer_returnx()
    {
        $paytype = $this->request->param('paytype');
        $out_trade_no = $this->request->param('out_trade_no');
        $pay = Service::checkReturn($paytype);
        if (!$pay) {
            $this->error('签名错误', '');
        }

        //你可以在这里通过out_trade_no去验证订单状态
        //但是不可以在此编写订单逻辑！！！

        $this->success("请返回网站查看支付结果", addon_url("epay/index/index"));
    }
}
