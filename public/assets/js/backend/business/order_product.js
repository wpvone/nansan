define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'business/order_product/index' + location.search,
                    add_url: 'business/order_product/add',
                    edit_url: 'business/order_product/edit',
                    del_url: 'business/order_product/del',
                    multi_url: 'business/order_product/multi',
                    table: 'order_product',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'order.order_num', title: __('Order.order_num')},
                        // {field: 'product_title', title: __('商品信息')},

                        {field: 'product_title', title: __('商品信息'),
                            cellStyle: function() {
                                return {
                                    css: {
                                        'max-width': '240px',
                                        display: '',
                                        overflow: 'hidden',
                                        'text-overflow': 'ellipsis'
                                    }
                                }
                            },
                            formatter: function(val ,row) {
                                return '<div>' + val + '</div><div>' + row.description + '</div>'
                            }},

                        {field: 'order_id', title: __('订单ID'), visible: false},
                        {field: 'attr_sku_name', title: __('Attr_sku_name')},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'one_price', title: __('One_price'), operate:'BETWEEN'},
                        {field: 'buy_num', title: __('购买数量')},
                        // {field: 'discount_price', title: __('Discount_price'), operate:'BETWEEN'},
                        {field: 'product_price', title: __('Product_price'), operate:'BETWEEN'},
                        {field: 'delivery_price', title: __('Delivery_price'), operate:'BETWEEN'},
                        // {field: 'buyer_review', title: __('Buyer_review')},
                        {field: 'order_price', title: __('Order_price'), operate:'BETWEEN'},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'createtime', title: __('创建时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'product.title', title: __('Product.title')},
                        // {field: 'attrsku.price', title: __('Attrsku.price'), operate:'BETWEEN'},
                        // {field: 'attrsku.stock', title: __('Attrsku.stock')},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'business/order_product/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'business/order_product/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'business/order_product/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});