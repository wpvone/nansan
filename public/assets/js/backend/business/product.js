requirejs.config({
    paths: {
        vue: 'backend/business/libs/vue'
    }
})

define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'vue', 'backend/business/libs/CustomFormatter',
    'backend/business/libs/Hook', 'backend/business/libs/image'],
    function ($, undefined, Backend, Table, Form, Vue, Formatter, Hook, Image) {


    var Controller = {
        index: function () {
            // 初始化表格参数配置
            var bid = $('#bid').val();
            Table.api.init({
                extend: {
                    index_url: 'business/product/index' + location.search,
                    add_url: 'business/product/add?bid=' + bid,
                    edit_url: 'business/product/edit?bid=' + bid,
                    del_url: 'business/product/del',
                    multi_url: 'business/product/multi',
                    table: 'product',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url + '&bid=' + bid,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        // {field: 'categories_id', title: __('Categories_id')},
                        {field: 'title', title: __('Title'),
                            cellStyle: function() {
                            return {
                                css: {
                                    'max-width': '240px',
                                    display: '',
                                    overflow: 'hidden',
                                    'text-overflow': 'ellipsis'
                                }
                            }
                            }, operate: "LIKE",
                            formatter: function(val ,row) {
                                return '<div>' + val + '</div><div>' + row.description + '</div>'
                            }},
                        {field: 'description', title: __('Description'),
                            cellStyle: function() {return {css: {'max-width': '240px', display: '',
                                    overflow: 'hidden', 'text-overflow': 'ellipsis'}}},
                            visible: false, operate: false},
                        {field: 'head_image', title: __('Head_image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'images', title: __('Images'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'read_num', title: __('Read_num'), operate: "BETWEEN"},
                        // {field: 'favo_num', title: __('Favo_num')},
                        {field: 'sell_num', title: __('Sell_num'), operate: "BETWEEN"},
                        // {field: 'favo_coms', title: __('Favo_coms')},
                        // {field: 'mid_coms', title: __('Mid_coms')},
                        // {field: 'bad_coms', title: __('Bad_coms')},
                        // {field: 'unit_id', title: __('Unit_id')},
                        // {field: 'serve_ids', title: __('Serve_ids')},
                        // {field: 'delivery_tpl_id', title: __('Delivery_tpl_id')},
                        {field: 'default_exp_price', title: __('Default_exp_price'), operate:'BETWEEN'},
                        {field: 'siteswitch', title: __('是否上架'), searchList: {"0":__('Siteswitch 0'),"1":__('Siteswitch 1')}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'hotswitch', title: __('Hotswitch'), searchList: {"0":__('Hotswitch 0'),"1":__('Hotswitch 1')}, table: table, formatter: Table.api.formatter.toggle},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'from_address', title: __('From_address'), operate: "LIKE"},
                        {field: 'low_price', title: __('Low_price'), operate:'BETWEEN'},
                        {field: 'categories.title', title: __('Categories.title')},
                        // {field: 'deliverytpl.title', title: __('Deliverytpl.title')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table,
                        //     events: Table.api.events.operate, formatter: Table.api.formatter.operate},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate, buttons: [
                                {
                                    name: 'edit',
                                    icon: 'fa fa-pencil',
                                    title: __('Edit'),
                                    extend: 'data-toggle="tooltip" data-area=\'["100%", "100%"]\'',
                                    classname: 'btn btn-xs btn-success btn-editone',
                                    url: $.fn.bootstrapTable.defaults.extend.edit_url
                                }
                            ]}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'business/product/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title'), align: 'left'},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'business/product/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'business/product/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            var vm = new Vue({
                el: '#app',
                data() {
                    return {
                        attrGroups: [],
                        attrItems: [],
                        productSkus: [],
                        skus: []
                    }
                },
                computed: {
                    productSkusStr() {
                        return JSON.stringify(this.productSkus)
                    }
                },
                mounted() {
                    Image.init()
                },
                methods: {
                    setAttrGroupName(i, e) {
                        this.$set(this.attrGroups, i, e.target.value)
                    },
                    addRow() {
                        var val = this.$refs['attr-group-name'].value
                        if ($.trim(val) == '') {
                            Toastr.error("不能为空")
                            return
                        }
                        if (val.indexOf(',') > -1) {
                            Toastr.error("规格子项 " + val + " 不能包含符号‘,’")
                            return
                        }
                        this.attrGroups.push(val)
                        this.$refs['attr-group-name'].value = ''
                    },
                    addChild(index) {
                        var val = this.$refs['attr-item-name-' + index][0].value
                        if ($.trim(val) == '') {
                            Toastr.error("不能为空")
                            return
                        }
                        if (val.indexOf(',') > -1) {
                            Toastr.error("规格 " + val + " 不能包含符号‘,’")
                            return
                        }
                        if (!val) return false
                        if (this.attrItems[index]) {
                            this.attrItems[index].push(val)
                        } else {
                            this.attrItems.push([val])
                        }
                        this.$refs['attr-item-name-' + index][0].value = ""
                        this.generateProducts()
                    },
                    upimage(i) {
                        Image.open(function(data) {
                            $('#c-attr-image-' + i).val(data.url)
                            if (vm.skus[i]) {
                                vm.$set(vm.skus[i], 'image', data.url)
                            } else {
                                vm.$set(vm.skus, i, {image: data.url})
                            }
                            Image.close()
                        })
                    },
                    removeChild(i, j) {
                        this.attrItems[i].pop(j)
                        this.generateProducts()
                    },
                    removeGroup(i) {
                        this.attrGroups.pop(i)
                        if (this.attrItems[i])
                            this.attrItems.pop(i)
                        this.generateProducts()
                    },
                    generateProducts() {
                        this.productSkus = this.descartes(this.attrItems)
                    },
                    descartes(array){
                        if (array.length == 0) return []
                        if( array.length < 2 ) {
                            var res = []
                            array[0].forEach(function(v) {
                                res.push([v])
                            })
                            return res
                        }
                        return [].reduce.call(array, function(col, set) {
                            var res = [];
                            col.forEach(function(c) {
                                set.forEach(function(s) {
                                    var t = [].concat( Array.isArray(c) ? c : [c] );
                                    t.push(s);
                                    res.push(t);
                                })});
                            return res;
                        });
                    }
                }
            })
            Controller.api.bindevent();
            window.batchSetAttrs = function(field) {
                $('.attr-' + field).val($('#batch-' + field).val())
            }
            Hook.listen('document')
        },
        edit: function () {
            Controller.api.bindevent();
            var vm = new Vue({
                el: '#app',
                data() {
                    return {
                        attrGroups: [],
                        attrItems: [],
                        productSkus: [],
                        skus: []
                    }
                },
                computed: {
                    productSkusStr() {
                        return JSON.stringify(this.productSkus)
                    }
                },
                mounted() {
                    skus = JSON.parse(skus)
                    if (skus.length) {
                        this.skus = skus
                        this.attrGroups = this.skus[0].keys.split(',')
                        this.attrItems = JSON.parse(attrItems);
                        this.generateProducts()
                    }
                    Image.init()
                },
                methods: {
                    setAttrGroupName(i, e) {
                        this.$set(this.attrGroups, i, e.target.value)
                    },
                    addRow() {
                        var val = this.$refs['attr-group-name'].value
                        if ($.trim(val) == '') {
                            Toastr.error("不能为空")
                            return
                        }
                        if (val.indexOf(',') > -1) {
                            Toastr.error("规格 " + val + " 不能包含符号‘,’")
                            return
                        }
                        this.attrGroups.push(val)
                        this.$refs['attr-group-name'].value = ''
                    },
                    addChild(index) {
                        var val = this.$refs['attr-item-name-' + index][0].value
                        if ($.trim(val) == '') {
                            Toastr.error("不能为空")
                            return
                        }
                        if (val.indexOf(',') > -1) {
                            Toastr.error("规格子项 " + val + " 不能包含符号‘,’")
                            return
                        }
                        if (!val) return false
                        if (this.attrItems[index]) {
                            this.attrItems[index].push(val)
                        } else {
                            this.attrItems.push([val])
                        }
                        this.$refs['attr-item-name-' + index][0].value = ""
                        this.generateProducts()
                    },
                    upimage(i) {
                        Image.open(function(data) {
                            $('#c-attr-image-' + i).val(data.url)
                            if (vm.skus[i]) {
                                vm.$set(vm.skus[i], 'image', data.url)
                            } else {
                                vm.$set(vm.skus, i, {image: data.url})
                            }
                            Image.close()
                        })
                    },
                    removeChild(i, j) {
                        this.attrItems[i].pop(j)
                        this.generateProducts()
                    },
                    removeGroup(i) {
                        this.attrGroups.pop(i)
                        this.attrItems.pop(i)
                        this.generateProducts()
                    },
                    batchSetAttr($type) {
                        for(var i = 0; i < this.skus.length; i ++) {
                            this.$set(this.skus[i], $type, this.$refs['input-' + $type].value)
                        }
                    },
                    generateProducts() {
                        this.productSkus = this.descartes(this.attrItems)
                    },
                    descartes(array){
                        if (array.length == 0) return []
                        if( array.length < 2 ) {
                            var res = []
                            array[0].forEach(function(v) {
                                res.push([v])
                            })
                            return res
                        }
                        return [].reduce.call(array, function(col, set) {
                            var res = [];
                            col.forEach(function(c) {
                                set.forEach(function(s) {
                                    var t = [].concat( Array.isArray(c) ? c : [c] );
                                    t.push(s);
                                    res.push(t);
                                })});
                            return res;
                        });
                    }
                }
            })
            window.batchSetAttr = function(field) {
                $('.attr-' + field).val($('#batch-' + field).val())
            }
            Hook.listen('form')
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});