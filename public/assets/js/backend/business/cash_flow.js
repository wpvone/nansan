define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'business/cash_flow/index' + location.search,
                    add_url: 'business/cash_flow/add',
                    edit_url: 'business/cash_flow/edit',
                    del_url: 'business/cash_flow/del',
                    multi_url: 'business/cash_flow/multi',
                    table: 'cash_flow',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        // {field: 'type', title: __('Type'), searchList: {"1":__('Type 1'),"2":__('Type 2')}, formatter: Table.api.formatter.normal},
                        {field: 'createtime', title: __('提现时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3')}, formatter: Table.api.formatter.status},
                        // {field: 'remark', title: __('Remark')},
                        {field: 'bankName', title: __('Bankname')},
                        {field: 'bank_name_zhi', title: __('Bank_name_zhi')},
                        {field: 'bank_num', title: __('Bank_num')},
                        {field: 'card_name', title: __('Card_name')},
                        {field: 'user.username', title: __('申请者'),
                            formatter: function(val, row, index) {

                                return  __('用户名')+':'+ row.user.username + '<br>'
                                        + '昵称:' + row.user.nickname + '<br>'
                            }},
                        // {field: 'user.nickname', title: __('User.nickname')},
                        {field: 'updatetime', title: __('审核时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                        // 下拉按钮  ajax 刷新删除回调的success

                        {field: 'operate', title: __('Operate'),
                            table: table, events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate, buttons: [
                                {
                                    name: 'ajax',
                                    text: '通过',
                                    title: __('审核通过'),
                                    icon: 'fa fa-magic',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    dropdown: '操作',
                                    visible: function(row) {
                                        if (row.status == 1) return true
                                        return false;
                                    },
                                    url: function(row) {
                                        return  'business/cash_flow/pass/ids/' + row.id;
                                    },

                                    confirm: '确认通过?',
                                    refresh :true,

                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },

                                {
                                    name: 'ajax',
                                    text: '拒绝提现',
                                    title: __('审核驳回'),
                                    icon: 'fa fa-magic',
                                    classname: 'btn btn-xs btn-info btn-magic btn-ajax',
                                    dropdown: '操作',
                                    visible: function(row) {
                                        if (row.status == 1) return true
                                        return false;
                                    },
                                    url: function(row) {
                                        return  'business/cash_flow/notPass/ids/' + row.id;
                                    },
                                    confirm: '确认驳回?',
                                    refresh :true,

                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                }
                            ]}

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});