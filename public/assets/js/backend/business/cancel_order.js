define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'business/cancel_order/index' + location.search,
                    add_url: 'business/cancel_order/add',
                    edit_url: 'business/cancel_order/edit',
                    del_url: 'business/cancel_order/del',
                    multi_url: 'business/cancel_order/multi',
                    table: 'cancel_order',
                }
            });

            var table = $("#table");
            var order = $("#ids").val();

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        // {field: 'user_id', title: __('User_id')},
                        {field: 'order_id', title: __('Order_id'), visible: false},
                        {field: 'order.order_num', title: __('订单号')},
                        {field: 'user.username', title: __('申请者')},
                        {field: 'type', title: __('Type'), searchList: {"1":__('Type 1'),"2":__('Type 2')},
                            custom: { "1": 'success', "2": 'danger'},
                            formatter: Table.api.formatter.normal},
                        // {field: 'remark', title: __('Remark')},
                        // {field: 'order_cancel_id', title: __('Order_cancel_id')},
                        // {field: 'status', title: __('Status'),
                        //     searchList: {"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3')},
                        //     formatter: Table.api.formatter.status, visible: false},

                        {field: 'status', title: __('当前进度'),
                            formatter: function(val, row, index) {
                            if (row.type == 1) {
                                if (row.status == 1) {
                                    return '待审核';
                                }
                                if(row.status == 2) {
                                    return '通过';
                                }
                                if (row.status == 3) {
                                    return '驳回';
                                }
                            } else {
                                return row.Progress;

                            }
                        }, operate: false},


                        {field: 'money', title: __('需退款金额'), operate:'BETWEEN'},
                        // {field: 'order.attr_name', title: __('Order.attr_name')},
                        // {field: 'order.sku_name', title: __('Order.sku_name')},
                        // {field: 'order.product_id', title: __('Order.product_id')},
                        // {field: 'order.one_price', title: __('Order.one_price'), operate:'BETWEEN'},
                        // {field: 'order.buy_num', title: __('Order.buy_num')},
                        // {field: 'order.total_price', title: __('Order.total_price'), operate:'BETWEEN'},
                        {field: 'ordercancel.name', title: __('Ordercancel.name'), operate: "LIKE"},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('审核时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},

                        // 下拉按钮
                        {field: 'operate', title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [
                                {
                                    name: 'detail', text: '查看订单', icon: '', classname: 'btn btn-xs btn-info btn-dialog',
                                    dropdown: '操作',
                                    extend: 'data-area=\'["80%", "60%"]\'',
                                    url: function(row) {
                                        return 'business/order/index?id=' + row.order_id;
                                    }
                                },

                                {
                                    name: 'ajax',
                                    text: '通过',
                                    title: __('审核通过'),
                                    icon: 'fa fa-magic',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    dropdown: '操作',
                                    // extend: 'data-refund="1"',
                                    visible: function(row) {
                                        if (row.status == 1) return true // 取消订单
                                        return false;
                                    },
                                    url: function(row) {
                                        return  'business/cancel_order/pass/ids/' + row.id;
                                    },

                                    confirm: '确认通过?',
                                    refresh :true,
                                    // success: function (data, ret) {
                                    //     Layer.alert(ret.msg);
                                    //     setTimeout(res =>{
                                    //         $('.btn-refresh').click();
                                    //     },1000);
                                    //
                                    //     //如果需要阻止成功提示，则必须使用return false;
                                    //     //return false;
                                    // },
                                    error: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        window.location.reload();
                                        return false;
                                    }
                                },
                                {
                                    name: 'ajax',
                                    text: '驳回',
                                    title: __('审核驳回'),
                                    icon: 'fa fa-magic',
                                    classname: 'btn btn-xs btn-warning btn-magic btn-ajax',
                                    dropdown: '操作',
                                    // extend: 'data-refund="1"',
                                    visible: function(row) {
                                        if (row.status == 1) return true
                                        return false;
                                    },
                                    url: function(row) {
                                        return  'business/cancel_order/reject/ids/' + row.id;
                                    },

                                    confirm: '确认驳回?',
                                    refresh :true,
                                    // success: function (data, ret) {
                                    //     Layer.alert(ret.msg);
                                    //     setTimeout(res =>{
                                    //         $('.btn-refresh').click();
                                    //     },1000);
                                    //     //如果需要阻止成功提示，则必须使用return false;
                                    //     //return false;
                                    // },
                                    error: function (data, ret) {
                                        window.location.reload();
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                },

                                {
                                    name: 'detail',
                                    text: '更新退款进度',
                                    icon: '',
                                    classname: 'btn btn-xs btn-success btn-dialog',
                                    dropdown: '操作',
                                    visible: function(row) {
                                        if (row.type == 2 && row.status == 1) return true
                                        return false
                                    },
                                    url: function(row) {
                                        return 'business/cancel_order/update/ids/' + row.id;
                                    }
                                },

                            ]}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        update: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});