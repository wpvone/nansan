define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'business/order/index' + location.search,
                    add_url: 'business/order/add',
                    edit_url: 'business/order/edit',
                    del_url: 'business/order/del',
                    multi_url: 'business/order/multi',
                    table: 'order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        // {field: 'order_num', title: __('Order_num')},
                        {field: 'order_num', title: __('Order_num'), formatter: function(val, row, index) {
                                return  val + '<br>'
                                    + Table.api.formatter.datetime.call(this, row.createtime, row, index)

                            }},
                        // {field: 'pay_type', title: __('Pay_type'), searchList: {"1":__('Pay_type 1'),"2":__('Pay_type 2'),"3":__('Pay_type 3'),"4":__('Pay_type 4'),"5":__('Pay_type 5')}, formatter: Table.api.formatter.normal},
                        // {field: 'pay_status', title: __('Pay_status'), searchList: {"1":__('Pay_status 1'),"2":__('Pay_status 2'),"3":__('Pay_status 3')}, formatter: Table.api.formatter.status},
                        {field: 'pay_status', title: __('Pay_status'), formatter: function(val, row, index) {
                                var str = '';
                                if (row.pay_status == 2) {
                                    // `pay_type` enum('5','4','3','2','1')支付类型:1=微信,2=支付宝,3=银行卡,4=余额,5=其他',
                                    if (row.pay_type == 1) {
                                        str = '微信';
                                    }
                                    if (row.pay_type == 2) {
                                        str = '支付宝';
                                    }
                                    if (row.pay_type == 3) {
                                        str = '银行卡';
                                    }
                                    if (row.pay_type == 4) {
                                        str = '余额';
                                    }
                                    if (row.pay_type == 5) {
                                        str = '其他';
                                    }

                                    return  __('支付时间')+':'+ row.pay_time + '<br>'
                                        + '状态:' + '用户已支付' + '<br>'
                                        + '支付方式:' + str + '<br>'
                                }
                                if(row.pay_status == 1) {
                                    return '用户未支付'
                                }

                                if(row.pay_status == 3) {
                                    return '超时未支付'
                                }

                            },
                            searchList: {"1": '待支付', "2": '支付成功', "3": '超时未支付'},
                            },


                        {field: 'actual_price', title: __('Actual_price'), operate:'BETWEEN'},
                        {field: 'address_json.id', title: __('收货地址'), formatter: function(val, row, index) {
                                return '收件人' + ':' + row.address_json.name + '<br>'
                                    + __('联系方式') + ':' + row.address_json.mobile + '<br>'
                                    + __('地址') + ':' + row.address_json.address + '<br>'
                                    + '详细地址:' + row.address_json.detail_address
                            }, operate:false},
                        // {field: 'delivery_time', title: __('Delivery_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'pay_time', title: __('Pay_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},

                        // {field: 'received_time', title: __('21'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'is_delivery', title: __('快递是否发货'),
                            searchList: {"1":__('Is_delivery 1'),"2":__('Is_delivery 2')}, visible: false,
                            formatter: Table.api.formatter.normal},
                        {field: 'express_sn', title: __('快递信息'), formatter: function(val, row, index) {
                                if(row.is_delivery == 1) {
                                    // return  __('Express_name')+':'+ row.express_name + '<br>'
                                    //     + __('Express_sn') + ':<br>' + row.express_sn + '<br>'
                                    //     + '发货时间:' + row.delivery_time + '<br>'
                                    return  '已发货'
                                }else {
                                    return '未发货'
                                }
                            }, operate: false},


                        // {field: 'express_name', title: __('Express_name')},
                        // {field: 'express_sn', title: __('Express_sn')},
                        {field: 'is_received', title: __('是否收货'),
                            searchList: {"1":__('Is_received 1'),"2":__('Is_received 2')}, visible: false,
                            formatter: Table.api.formatter.normal},
                        {field: 'received_time', title: __('是否收货'), operate: false, formatter: function(val, row, index) {
                                if (row.is_received == 1) {
                                    return  __('收货时间')+':'+ row.received_time + '<br>'
                                             + '状态:' + '用户已收货' + '<br>'
                                } else {
                                    return '暂无信息'
                                }

                            }},

                        {field: 'is_evaluate', title: __('Is_evaluate'), searchList: {"1":__('Is_evaluate 1'),"2":__('Is_evaluate 2')}, formatter: Table.api.formatter.normal},
                        {field: 'status', title: __('Status'), searchList: {"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3'),"4":__('Status 4'),"5":__('Status 5'),"6":__('Status 6'),"7":__('Status 7'),"8":__('Status 8')}, formatter: Table.api.formatter.status},

                        {field: 'user.username', title: __('下单者信息'), operate: "LIKE", formatter: function(val, row, index) {
                                return '用户名' + ':' + row.user.username + '<br>'
                                    + __('昵称') + ':' + row.user.nickname + '<br>'
                            }, operate: false},
                        {field: 'user.username', title: __('下单者'), operate: "LIKE", visible: false},
                        // {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'user.username', title: __('User.username')},

                        {field: 'saler_remark', title: __('Saler_remark'), operate: "LIKE"},

                        // {field: 'user.nickname', title: __('User.nickname')},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate, buttons: [
                                {
                                    name: 'detail', text: '查看', icon: '', classname: 'btn btn-xs btn-info btn-dialog',
                                    dropdown: '操作',
                                    extend: 'data-area=\'["80%", "60%"]\'',
                                    url: function(row) {
                                        return 'business/order_product/index?order_id=' + row.id;
                                    }
                                },
                                {
                                    name: 'detail', text: '审核取消订单', icon: '', classname: 'btn btn-xs btn-success btn-dialog',
                                    dropdown: '操作',
                                    extend: 'data-area=\'["80%", "60%"]\'',
                                    visible: function(row) {
                                        if (row.status == 7) return true
                                        return false
                                    },
                                    url: function(row) {
                                        return 'business/cancel_order/index?order_id=' + row.id;
                                    }
                                },
                                {
                                    name: 'detail', text: '退款', icon: '', classname: 'btn btn-xs btn-success btn-dialog',
                                    dropdown: '操作',
                                    extend: 'data-area=\'["80%", "60%"]\'',
                                    visible: function(row) {
                                        if (row.status == 2) return true
                                        return false
                                    },
                                    url: function(row) {
                                        return 'business/cancel_order/index?order_id=' + row.id;
                                    }
                                },


                                {
                                    name: 'detail',
                                    text: '发货',
                                    title: '发货',
                                    icon: '',
                                    classname: 'btn btn-xs btn-success btn-ajax',
                                    dropdown: '操作',
                                    visible: function(row) {
                                        if (row.status == 3 ) return true
                                        return false
                                    },
                                    url: function(row) {
                                        return 'business/order/send/ids/' + row.id;
                                    }
                                },

                                // {
                                //     name: 'detail',
                                //     text: '修改物流信息',
                                //     icon: '',
                                //     classname: 'btn btn-xs btn-success btn-dialog',
                                //     dropdown: '操作',
                                //     visible: function(row) {
                                //         if (row.is_delivery == 1 && row.is_received == 2 && row.status == 4) return true
                                //         return false
                                //     },
                                //     url: function(row) {
                                //         return 'business/order/sedit/ids/' + row.id;
                                //     }
                                // },

                                {
                                    name: 'ajax',
                                    text: '确认付款',
                                    title: __('确认付款'),
                                    icon: '',
                                    classname: 'btn btn-xs btn-success btn-dialog btn-ajax',
                                    dropdown: '操作',
                                    visible: function(row) {
                                        if (row.status == 1) return true
                                        return false
                                    },
                                    url: function(row) {
                                        return 'business/order/pay/ids/' + row.id;
                                    },
                                    confirm: '确认操作?',
                                    refresh :true,
                                    error: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        return false;
                                    }
                                }
                            ]}


                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'business/order/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'business/order/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'business/order/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        send: function () {
            Controller.api.bindevent();
        },
        sedit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});