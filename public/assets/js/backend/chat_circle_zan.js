define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chat_circle_zan/index' + location.search,
                    add_url: 'chat_circle_zan/add',
                    edit_url: 'chat_circle_zan/edit',
                    del_url: 'chat_circle_zan/del',
                    multi_url: 'chat_circle_zan/multi',
                    import_url: 'chat_circle_zan/import',
                    table: 'chat_circle_zan',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'to_uid', title: __('To_uid')},
                        {field: 'circle_id', title: __('Circle_id')},
                        {field: 'comment_id', title: __('Comment_id')},
                        {field: 'status', title: __('Status')},
                        {field: 'count', title: __('Count')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});