define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chat_friend/index' + location.search,
                    add_url: 'chat_friend/add',
                    edit_url: 'chat_friend/edit',
                    del_url: 'chat_friend/del',
                    multi_url: 'chat_friend/multi',
                    import_url: 'chat_friend/import',
                    table: 'chat_friend',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user.nickname', title: __('User.nickname'), operate: 'LIKE'},
                        // {field: 'user.mobile', title: __('User.mobile'), operate: 'LIKE'},
                        {field: 'friend.nickname', title: __('Friend.nickname'), operate: 'LIKE'},
                        // {field: 'friend.mobile', title: __('Friend.mobile'), operate: 'LIKE'},
                        // {field: 'user_id', title: __('User_id')},
                        // {field: 'friend_id', title: __('Friend_id')},
                        {field: 'note', title: __('Note'), operate: 'LIKE'},

                        {field: 'is_black', title: __('Is_black'), searchList: {"1":__('Is_black 1'),"2":__('Is_black 2')}, formatter: Table.api.formatter.normal},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});