define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'easypeople_order/index' + location.search,
                    add_url: 'easypeople_order/add',
                    edit_url: 'easypeople_order/edit',
                    del_url: 'easypeople_order/del',
                    multi_url: 'easypeople_order/multi',
                    import_url: 'easypeople_order/import',
                    table: 'easypeople_order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'easypeople_id', title: __('Easypeople_id')},
                        {field: 'dateprice_id', title: __('Dateprice_id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'pay_fee', title: __('Pay_fee'), operate:'BETWEEN'},
                        {field: 'status', title: __('Status'), searchList: {"-1":__('Status -1'),"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.status},
                        {field: 'easypeople.id', title: __('Easypeople.id')},
                        {field: 'easypeople.category_id', title: __('Easypeople.category_id')},
                        {field: 'easypeople.dateprice_id', title: __('Easypeople.dateprice_id')},
                        {field: 'easypeople.user_id', title: __('Easypeople.user_id')},
                        {field: 'easypeople.title', title: __('Easypeople.title'), operate: 'LIKE'},
                        {field: 'easypeople.brief', title: __('Easypeople.brief'), operate: 'LIKE'},
                        {field: 'easypeople.images', title: __('Easypeople.images'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'easypeople.mobile', title: __('Easypeople.mobile')},
                        {field: 'easypeople.province', title: __('Easypeople.province')},
                        {field: 'easypeople.city', title: __('Easypeople.city')},
                        {field: 'easypeople.area', title: __('Easypeople.area')},
                        {field: 'easypeople.area_detail', title: __('Easypeople.area_detail'), operate: 'LIKE'},
                        {field: 'easypeople.createtime', title: __('Easypeople.createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'easypeople.status', title: __('Easypeople.status'), formatter: Table.api.formatter.status},
                        {field: 'easypeople.runtime', title: __('Easypeople.runtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'easypeople.endtime', title: __('Easypeople.endtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'easypeopledateprice.id', title: __('Easypeopledateprice.id')},
                        {field: 'easypeopledateprice.name', title: __('Easypeopledateprice.name'), operate: 'LIKE'},
                        {field: 'easypeopledateprice.day', title: __('Easypeopledateprice.day')},
                        {field: 'easypeopledateprice.price', title: __('Easypeopledateprice.price'), operate:'BETWEEN'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});